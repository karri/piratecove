include Common.mk

objects= \
	treasure.o \

target = piratecove.a78

all: $(target)

$(target) : $(objects)
	$(CL) -t atari7800 -o $@ --force-import __EXEHDR__ -m piratecove.map $(objects) -l atari7800
	sign7800 -w $(target)

clean:
	$(RM) $(objects) $(target) wizzy.map *.c~

