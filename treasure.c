/*
 *	ScottFree Revision 1.14
 *
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version
 *	2 of the License, or (at your option) any later version.
 */

#include <lynx.h>
#include <6502.h>
#include <conio.h>
#include <joystick.h>
#include <tgi.h>
#include "../resident/resident.h"
#include "../abcmusic/abcmusic.h"
#define tgi_flip() tgi_ioctl(1, 0)
#define tgi_setbgcolor(val) tgi_ioctl(2, (unsigned)(val))
void __fastcall__ fade_to_pal(char *pal, unsigned char interval);
void __fastcall__ fade_out(unsigned char interval);
void __fastcall__ fade_in(char *pal, unsigned char interval);
extern char lynxjoy[];
#define USE_GRAPHICS
extern char drawPending;
extern char interruptOccurred;

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#include <signal.h>
#include "treasure.h"
 
static int strncasecmp(char *a,char *b, int n)
{
	while(*a&&n>0)
	{
		char c=*a;
		char d=*b;
		if(islower(c))
			c=toupper(c);
		if(islower(d))
			d=toupper(d);
		if(c<d)
			return(-1);
		if(c>d)
			return(1);
		a++;
		b++;
		n--;
		if(n==0)
			return(0);
	}
	if(*b)
		return(1);
	return(0);
}

static Header GameHeader = {
	66,
	177,
	79,
	26,
	7,
	1,
	2,
	3,
	150,
	88,
	1
};

#define ITEMS_TOTAL 67
#define SAVED_ITEMS (ITEMS_TOTAL/2)

static Item Items[ITEMS_TOTAL] = {
{"Flight of stairs",1,1,""}, 
{"Open window",2,2,""}, 
{"Books in a bookcase",2,2,""},
{"Large blood soaked book",0,0,"book"}, 
{"Bookcase with secret passage beyond",0,0,""}, 
{"Pirate's duffel bag",4,4,"bag"}, 
{"Sign says:\n'Bring *TREASURES* here\nsay: SCORE'",1,1}, 
{"Empty bottle",0,0,"bottle"}, 
{"Unlit torch",4,4,"torch"}, 
{"Lit torch",0,0,"torch"}, 
{"Matches",0,0,"matches"}, 
{"Small ship's keel and mast",6,6,""}, 
{"Wicked looking pirate",9,9,""}, 
{"Treasure chest",9,9,"chest"}, 
{"Mongoose",8,8,"mongoose"}, 
{"Rusty anchor",24,24,"anchor"}, 
{"Grass shack",8,8,""}, 
{"Mean and hungry looking crocodiles",11,11,""}, 
{"Locked door",11,11,""}, 
{"Open door with hall beyond",0,0,""}, 
{"Pile of sails",17,17,"sails"}, 
{"Fish",10,10,"FIS"}, 
{"*COMPASS* embroded with jewels.",25,25,"compass"}, 
{"Deadly mamba snakes",25,25,""}, 
{"Parrot",9,9,"parrot"}, 
{"Bottle of rum",1,1,"bottle"}, 
{"Rug",0,0,"rug"}, 
{"Ring of keys",0,0,"keys"}, 
{"Open treasure chest",0,0,"chest"}, 
{"Set of plans",0,0,"plans"}, 
{"Rug",1,1,""}, 
{"Claw hammer",15,15,"hammer"}, 
{"Nails",0,0,"nails"}, 
{"Pile of precut lumber",17,17,"lumber"}, 
{"Tool shed",17,17,""}, 
{"Locked door",16,16,""}, 
{"Open door with pit beyond",0,0,""}, 
{"Pirate ship",0,0,""}, 
{"Rock wall with narrow crack in it",18,18,""}, 
{"Narrow crack in the rock",17,17,""}, 
{"Salt water",10,10,""}, 
{"Sleeping pirate",0,0,""}, 
{"Bottle of salt water",0,0,"bottle"}, 
{"Rum bottle smashed into pieces. Sign 'Opposite of LIGHT is Unlight'",4,4,""}, 
{"Safety sneakers",1,1,"sneakers"}, 
{"Map",0,0,"map"}, 
{"Shovel",15,15,"shovel"}, 
{"Mouldy old bones",0,0,"bones"}, 
{"Sand",6,6,"sand"}, 
{"Bottles of rum",0,0,"bottles"}, 
{"*STAMPS* of great value.",0,0,"stamps"}, 
{"Lagoon",6,6,""}, 
{"The tide is out",24,24,""}, 
{"The tide is coming in",0,0,""}, 
{"Water wings",15,15,"wings"}, 
{"Flotsam and jetsam",0,0,""}, 
{"Monastary",23,23,""}, 
{"Wooden box",0,0,"box"}, 
{"Dead squirrel",0,0,""}, 
{"Sign in the sand says:\n'Private property! Watch out for the tide! Have a nice day!'",6,6,""}, 
{"Sack of crackers",1,1,"crackers"}, 
{"Note",0,0,"NOT"}, 
{"Small advertising flyer",0,0,"flyer"}, 
{"Burnt out torch",0,0,"torch"}, 
{"",0,0,""}, 
{"",0,0,""},
{"",0,0,""} 
};

static Room Rooms[27] = {
{{0,0,0,0,0,0},""},
{{0,0,0,0,0,0},"flat in London"},
{{0,0,0,0,0,1},"alcove"},
{{0,0,4,2,0,0},"secret passageway"},
{{0,0,0,3,0,0},"musty attic"},
{{0,0,0,0,0,0},"*I'm outside an open window on the ledge of a very tall building"},
{{0,0,8,0,0,0},"sandy beach on a tropical isle"},
{{0,12,13,14,0,11},"maze of caves"},
{{0,0,14,6,0,0},"meadow"},
{{0,0,0,8,0,0},"grass shack"},
{{10,24,10,10,0,0},"*I'm in the ocean"},
{{0,0,0,0,7,0},"pit"},
{{7,0,14,13,0,0},"maze of caves"},
{{7,14,12,19,0,0},"maze of caves"},
{{0,0,0,8,0,0},"*I'm at the foot of a cave ridden hill, a pathway leads on up to the top"},
{{17,0,0,0,0,0},"tool shed"},
{{0,0,17,0,0,0},"long hallway"},
{{0,0,0,16,0,0},"large cavern"},
{{0,0,0,0,0,14},"*I'm on top of a hill. Below is Tortuga. Across the sea way off in the distance I see the horizon."},
{{0,14,14,13,0,0},"maze of caves"},
{{0,0,0,0,0,0},"*I'm aboard a Pirate ship anchored off shore"},
{{0,22,0,0,0,0},"*I'm on the beach at Isla de Muerta"},
{{21,0,23,0,0,0},"spooky old graveyard filled with piles of empty and broken rum bottles"},
{{0,0,0,22,0,0},"large barren field"},
{{10,6,6,6,0,0},"shallow lagoon. To the north is the ocean"},
{{0,0,0,23,0,0},"sacked and deserted monastary"},
{{0,0,0,0,0,0},"*Welcome to Never Never Land"}
};

#define WORDS 80
#define NOUN_NULL 0
#define VERB_GO 1
#define VERB_CLIMB 2
#define VERB_WALK 3
#define VERB_RUN 4
#define VERB_ENTER 5
#define VERB_PACE 6
#define VERB_FOLLOW 7
#define VERB_SAY 8
#define VERB_SAIL 9
#define VERB_GET 10
#define VERB_TAKE 11
#define VERB_CATCH 12
#define VERB_PICK 13
#define VERB_REMOVE 14
#define VERB_WEAR 15
#define VERB_PULL 16
#define VERB_FLY 17
#define VERB_DROP 18
#define VERB_RELEASE 19
#define VERB_THROW 20
#define VERB_LEAVE 21
#define VERB_GIVE 22
#define VERB_DRINK 23
#define VERB_EAT 24
#define VERB_INVENTORY 25

#define VERB_LOOK 27
#define VERB_EXAMINE 28
#define VERB_WATCH 29
#define VERB_READ 30
#define VERB_LISTEN 31

#define VERB_SCORE 33
#define VERB_SAVE 34
#define VERB_KILL 35
#define VERB_ATTACK 36
#define VERB_LIGHT 37

#define VERB_OPEN 39
#define VERB_SHAKE 40
#define VERB_UNLIGHT 41
#define VERB_HELP 42


#define VERB_SWING 45
#define VERB_QUIT 46
#define VERB_BUILD 47
#define VERB_MAKE 48
#define VERB_WAKE 49
#define VERB_SET 50
#define VERB_CAST 51
#define VERB_DIG 52
#define VERB_BURN 53
#define VERB_FIND 54
#define VERB_JUMP 55
#define VERB_EMPTY 56
#define VERB_WEIGH 57

#define VERB_BREAK 59
#define VERB_SMASH 60


#define VERB_WAIT 63
#define VERB_FEED 64


#define VERB_CLOSE 67
#define VERB_SHUT 68
#define VERB_UNLOCK 80
#define VERB_FEEL 81
#define VERB_LOAD 82

static char *Verbs[WORDS+3] = {
"auto",
"go",
"*climb",
"*walk",
"*run",
"*enter",
"*pace",
"*follow",
"say",
"sail",
"get",
"*take",
"*catch",
"*pick",
"*remove",
"*wear",
"*pull",
"fly",
"drop",
"*release",
"*throw",
"*leave",
"*give",
"drink",
"*eat",
"inventory",
"sail",
"look",
"*examine",
"*watch",
"read",
"listen",
".",
"score",
"save",
"kill",
"*attack",
"light",
".",
"open",
"*shake",
"unlight",
"help",
".",
".",
"swing",
"quit",
"build",
"*make",
"wake",
"set",
"cast",
"dig",
"burn",
"find",
"jump",
"empty",
"weigh",
"",
"break",
"*smash",
".",
"",
"wait",
"feed",
"",
"",
"close",
"*shut",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"unlock",
"feel",
"load"
};

static char VerbsSorted[62] = {
VERB_ATTACK,
VERB_BREAK,
VERB_BUILD,
VERB_BURN,
VERB_CAST,
VERB_CATCH,
VERB_CLIMB,
VERB_CLOSE,
VERB_DIG,
VERB_DRINK,
VERB_DROP,
VERB_EAT,
VERB_EMPTY,
VERB_ENTER,
VERB_EXAMINE,
VERB_FEED,
VERB_FEEL,
VERB_FIND,
VERB_FLY,
VERB_FOLLOW,
VERB_GET,
VERB_GIVE,
VERB_GO,
VERB_HELP,
VERB_INVENTORY,
55,
35,
21,
37,
31,
VERB_LOAD,
27,
48,
39,
6,
13,
16,
46,
30,
19,
14,
4,
26,
9,
34,
8,
33,
50,
40,
68,
60,
45,
11,
20,
41,
80,
63,
49,
3,
29,
15,
57
};

#define NOUN_NORTH 1
#define NOUN_SOUTH 2
#define NOUN_EAST 3
#define NOUN_WEST 4
#define NOUN_UP 5
#define NOUN_DOWN 6

#define NOUN_PASSAGE 8
#define NOUN_HALLWAY 9
#define NOUN_BOOK 10
#define NOUN_BOTTLE 11
#define NOUN_RUM 12
#define NOUN_WINDOW 13
#define NOUN_GAME 14
#define NOUN_MONGOOSE 15
#define NOUN_PIRATE 16
#define NOUN_AROUND 17
#define NOUN_BAG 18
#define NOUN_DUFFEL 19
#define NOUN_TORCH 20
#define NOUN_OFF 21
#define NOUN_MATCHES 22
#define NOUN_YOHO 23
#define NOUN_30 24
#define NOUN_LUMBER 25
#define NOUN_RUG 26
#define NOUN_KEY 27
#define NOUN_INVENTORY 28
#define NOUN_COMPASS 29
#define NOUN_SAIL 30
#define NOUN_FISH 31
#define NOUN_ANCHOR 32
#define NOUN_SHACK 33
#define NOUN_PLANS 34
#define NOUN_CAVE 35
#define NOUN_SIGN 36
#define NOUN_DOOR 37
#define NOUN_CHEST 38
#define NOUN_PARROT 39
#define NOUN_HAMMER 40
#define NOUN_NAILS 41
#define NOUN_BOAT 42
#define NOUN_SHIP 43
#define NOUN_SHED 44
#define NOUN_CRACK 45
#define NOUN_WATER 46
#define NOUN_SALT 47
#define NOUN_LAGOON 48
#define NOUN_TIDE 49
#define NOUN_PIT 50
#define NOUN_SHOVEL 51
#define NOUN_BEACH 52
#define NOUN_MAP 53
#define NOUN_PAC 54
#define NOUN_BONES 55
#define NOUN_HOLE 56
#define NOUN_SAND 57
#define NOUN_BOX 58
#define NOUN_SNEAKERS 59
#define NOUN_CRACKERS 60
#define NOUN_SACK 61
#define NOUN_PIECES 62
#define NOUN_KEEL 63
#define NOUN_FLOTSAM 64
#define NOUN_JETSAM 65
#define NOUN_STAIRS 66
#define NOUN_UPSTAIRS 67
#define NOUN_PATH 68
#define NOUN_HILL 69

#define NOUN_AWAKE 71
#define NOUN_BUN 72

#define NOUN_NOTE 74
#define NOUN_FLYER 75
#define NOUN_DESTRUCTIVE 76
#define NOUN_CROCODILES 77
#define NOUN_SNAKES 78
#define NOUN_TREASURE 79
#define NOUN_WINGS 80
#define NOUN_MONASTARY 81
#define NOUN_SHORE 82
#define NOUN_STAMPS 83
#define NOUN_CART 84

static char *Nouns[WORDS+5] = {
"any",
"north",
"south",
"east",
"west",
"up",
"down",
".",
"passage",
"hallway",
"book",
"bottle",
"*rum",
"window",
"game",
"mongoose",
"pirate",
"around",
"bag",
"*duffel bag",
"torch",
"off",
"matches",
"yoho",
"30",
"lumber",
"rug",
"key",
"inventory",
"compass",
"sails",
"fish",
"anchor",
"shack",
"plans",
"cave",
"sign",
"door",
"chest",
"parrot",
"hammer",
"nails",
"boat",
"*ship",
"shed",
"crack",
"water",
"*salt water",
"lagoon",
"*tide",
"pit",
"shovel",
"*beach",
"map",
"pace",
"bones",
"hole",
"sand",
"box",
"sneakers",
"crackers",
"*sack",
"pieces",
"keel",
"flotsam",
"*jetsam",
"stairs",
"*upstairs",
"path",
"*hill",
"yoho",
"awake",
"*bun",
"pieces",
"note",
"flyer",
"destructive",
"crocodiles",
"snakes",
"treasure",
"wings",
"monastary",
"shore",
"stamps",
"cart"
};

static char NounsSorted[83] = {
24,
32,
17,
71,
18,
52,
42,
55,
10,
11,
58,
72,
84,
35,
38,
29,
45,
60,
77,
76,
37,
6,
19,
3,
31,
64,
75,
14,
9,
40,
69,
56,
28,
65,
63,
27,
48,
25,
53,
22,
81,
15,
41,
1,
74,
21,
54,
39,
8,
68,
62,
73,
16,
50,
34,
26,
12,
61,
30,
47,
57,
33,
44,
43,
82,
51,
36,
78,
59,
2,
66,
83,
49,
20,
79,
5,
67,
46,
4,
13,
80,
23,
70
};

static char *Messages[89] = {
"",
"There's a strange sound",
"The book is in bad condition but I can make out the title:\n'Isla de Muerta'.\nThere's a word written in blood in the flyleaf: 'YOHO' and a message:'What you need is a compass that does not point North'",
"Nothing happens",
"There's something there all right. Maybe I should",
"That's not very smart",
"I may need to say a MAGIC word here!",
"Everything spins around and suddenly I'm elsewhere...",
"Torch is lit",
"I was wrong, I guess its not a mongoose cause the snakes bit it!",
"I'm snake bit",
"Parrot attacks snakes and drives them off",
"Pirate won't let me",
"Its locked",
"Its open",
"There are a set of plans in it",
"Not while I'm carrying it",
"Crocs stop me",
"Sorry I can't",
"Wrong game you silly goose!",
"I don't have it",
"Pirate grabs rum and scuttles off chortling",
"...I think its me, Hee Hee.",
"Its nailed to the floor!",
"Yoho ho and a ...",
"No, something is missing!",
"It was a tight squeeze!",
"Something won't fit",
"Since nothing is happening",
"I slipped and fell...",
"Something falls out",
"They're plans to build the Jolly Roger (a Pirate ship!) You'll need: hammer, nails, lumber, anchor, sails, and a keel.",
"I've no container",
"It soaks into the ground",
"Too dry, fish vanish.",
"Pirate awakens and says 'Aye matey we be casting off soon' He then VANISHES!",
"What a waste...",
"I've no crew",
"Pirate says: 'Aye matey we be needing a map first'.",
"After a day at sea we set anchor off of a sandy beach. All Ashore who's going Ashore...",
"Try: 'WEIGH ANCHOR'",
"There's a map in it",
"Its a map to Isla de Muerta. At the bottom it says: '30 paces then dig!'",
"\nBased on the Pirate Adventure  by Alexis & Scott Adams.\nSAVE GAME is supported.\nRemember you can always ask for 'help'.\nPress 'B' and up/down to scroll text.",
"Its empty",
"I've no plans!",
"open it?",
"go there?",
"I found something!",
"I didn't find anything",
"I don't see it here",
"OK I walked off 30 paces.",
"CONGRATULATIONS !!! But your Adventure is not over yet...",
"Reading expands the mind",
"The Parrot crys:",
"'Check the bag matey'",
"'Check the chest matey'",
"from the other side!",
"Open the book!",
"There's multiple exits here!",
"Crocs eat fish and leave",
"I'm underwater, I guess I don't swim well. Blub Blub...",
"'Pieces of eight'",
"Its stuck in the sand",
"OK",
"Pirate says: 'Aye me Buckeroo, we be waiting for the tide to come in!'",
"The tide is out",
"The tide is coming in",
"About 20 pounds. Try: 'SET SAIL'",
"'Tides be a changing matey'",
"Note here says: 'I be liking parrots, they be smart matey!'",
"Pirate follows me ashore as if expecting something",
"Climb stairs...",
"Got anything to eat matey?",
"Parrot attacks crocs but is beaten off",
"Bird flys off looking very unhappy",
"Parrot ate a cracker.",
"Yummy",
"I hear nothing now",
"Pirate says:'First Yee be getting that ACCURSED thing off me ship!'",
"read it?",
"The Black Pearl is a new pirates cart for the Atari Lynx. Keep all the games your heart desires in one cart!",
"I'm not feeling destructive!",
"'Check the book, matey!'",
"All right, POOF the GAME is destroyed!",
"I see nothing special",
"I don't know where to look!",
"Its stuck",
""
};

static Action Actions[178] = {
{80,{422,342,420,340,0},{16559,8850}},
{80,{462,482,460,0,0},{15712,1705}},
{100,{521,552,540,229,220},{203,8700}},
{3,{483,0,0,0,0},{15712,0}},
{100,{284,308,0,0,0},{8626,0}},
{100,{28,663,403,40,0},{8700,0}},
{100,{48,20,660,740,220},{9055,10902}},
{100,{28,20,0,0,0},{3810,0}},
{100,{8,700,720,0,0},{10868,0}},
{100,{48,40,640,400,300},{9055,8305}},
{19,{524,0,0,0,0},{9778,9450}},
{40,{104,886,0,0,0},{4411,0}},
{80,{242,502,820,80,240},{9321,10109}},
{100,{8,140,80,500,0},{10262,8850}},
{25,{421,846,420,200,0},{5162,0}},
{100,{129,120,0,0,0},{6508,0}},
{50,{242,982,820,440,240},{9321,8850}},
{35,{483,69,0,0,0},{15705,0}},
{7,{483,249,0,0,0},{15706,0}},
{50,{484,1073,1086,0,0},{17661,9150}},
{50,{204,1086,0,0,0},{16711,0}},
{10,{209,1040,1060,300,1100},{10872,10050}},
{10,{208,1040,1060,89,0},{10867,0}},
{100,{483,8,0,0,0},{15719,10200}},
{100,{8,0,0,0,0},{10200,0}},
{100,{104,308,0,0,0},{8626,0}},
{80,{462,282,280,1160,0},{1422,0}},
{80,{342,482,480,260,0},{18725,9300}},
{30,{483,1212,252,480,1200},{18825,0}},
{20,{483,1203,0,0,0},{18900,0}},
{25,{1234,483,63,0,0},{15733,0}},
{100,{328,1260,180,320,0},{10860,11400}},
{1223,{107,100,61,0,0},{10507,8164}},
{7530,{404,242,63,903,0},{2829,0}},
{5570,{163,203,160,180,0},{10870,1264}},
{6170,{183,180,160,0,0},{10914,11400}},
{6300,{104,0,0,0,0},{900,0}},
{1529,{442,465,440,0,0},{7914,0}},
{1529,{442,462,0,0,0},{760,9150}},
{183,{322,180,0,0,0},{8170,9600}},
{1538,{262,242,0,0,0},{1800,0}},
{1538,{262,245,260,0,0},{7914,0}},
{5888,{262,242,0,0,0},{1800,0}},
{5888,{262,245,0,0,0},{1950,0}},
{6188,{262,245,541,260,560},{2155,7950}},
{5888,{261,0,0,0,0},{2400,0}},
{4088,{561,0,0,0,0},{2400,0}},
{4088,{263,0,0,0,0},{2713,0}},
{4088,{562,580,109,100,249},{2303,8700}},
{4088,{249,562,108,900,240},{6203,8700}},
{4088,{248,562,0,0,0},{6600,0}},
{4068,{103,69,0,0,0},{646,0}},
{4068,{103,68,0,0,0},{6600,0}},
{5887,{342,0,0,0,0},{2550,0}},
{5887,{362,0,0,0,0},{2713,0}},
{5887,{382,0,0,0,0},{2100,0}},
{159,{382,320,0,0,0},{8170,9600}},
{6187,{342,362,0,0,0},{2550,0}},
{6187,{345,362,541,360,380},{8303,10164}},
{3461,{503,0,0,0,0},{19051,3300}},
{3750,{0,0,0,0,0},{9900,0}},
{1528,{0,0,0,0,0},{9900,0}},
{4108,{1143,1012,0,0,0},{646,0}},
{1271,{0,0,0,0,0},{2853,0}},
{4510,{66,0,0,0,0},{2720,0}},
{4950,{0,0,0,0,0},{9750,0}},
{5114,{0,0,0,0,0},{10650,0}},
{7092,{592,0,0,0,0},{2745,0}},
{185,{284,140,0,0,0},{8156,10564}},
{4098,{1054,0,0,0,0},{647,17550}},
{4098,{1053,0,0,0,0},{647,17400}},
{4083,{322,0,0,0,0},{647,0}},
{4095,{762,0,0,0,0},{647,0}},
{195,{782,921,0,0,0},{2727,0}},
{195,{762,261,0,0,0},{2727,0}},
{6900,{0,0,0,0,0},{9450,0}},
{1526,{602,0,0,0,0},{2723,0}},
{1541,{621,602,640,520,600},{7853,8364}},
{195,{782,661,0,0,0},{2727,0}},
{7092,{623,583,303,643,20},{8700,0}},
{7092,{0,0,0,0,0},{3750,0}},
{200,{722,220,0,0,0},{10554,9600}},
{195,{762,61,0,0,0},{2727,0}},
{1223,{104,120,61,0,0},{10507,8164}},
{1526,{523,520,0,0,0},{7914,0}},
{195,{762,340,0,0,0},{8126,8464}},
{195,{782,360,0,0,0},{8157,10564}},
{7530,{404,242,1053,89,0},{17250,0}},
{0,{0,0,0,0,0},{0,0}},
{5868,{103,200,69,60,0},{4553,8700}},
{5868,{68,0,0,0,0},{494,0}},
{1546,{146,0,0,0,0},{4800,0}},
{1546,{802,141,140,840,0},{8302,17100}},
{2746,{841,840,140,0,0},{8302,4950}},
{3496,{802,0,0,0,0},{811,0}},
{3496,{841,840,140,0,0},{811,8302}},
{7366,{822,820,240,400,0},{5305,9300}},
{5861,{503,0,0,0,0},{2100,0}},
{8411,{503,500,140,0,0},{5433,10800}},
{192,{742,400,0,0,0},{8170,9600}},
{201,{404,88,420,240,242},{8170,8071}},
{201,{404,89,120,0,0},{8170,9600}},
{7530,{404,245,0,0,0},{2737,0}},
{7530,{404,912,0,0,0},{2738,0}},
{7530,{404,89,80,740,420},{10539,8762}},
{7530,{404,88,80,740,120},{10539,9062}},
{7671,{0,0,0,0,0},{6000,0}},
{4553,{903,0,0,0,0},{6300,0}},
{1350,{0,0,0,0,0},{6000,0}},
{1510,{62,60,0,0,0},{7914,0}},
{5860,{63,1254,1240,0,0},{8064,4500}},
{201,{404,88,420,0,0},{8170,9600}},
{218,{284,360,0,0,0},{8170,9600}},
{1539,{482,242,0,0,0},{1800,0}},
{1539,{482,480,0,0,0},{7904,16800}},
{194,{682,300,0,0,0},{8170,9600}},
{174,{140,464,0,0,0},{8751,0}},
{174,{140,0,0,0,0},{9051,0}},
{7800,{444,940,921,954,0},{10548,8014}},
{3495,{1203,0,0,0,0},{19050,0}},
{7800,{424,994,980,921,0},{10553,7264}},
{8250,{104,0,0,0,0},{10505,9150}},
{7800,{464,148,1140,921,1154},{10553,7264}},
{1541,{643,640,0,0,0},{7914,0}},
{163,{104,40,0,0,0},{8170,9600}},
{6300,{44,0,0,0,0},{15456,0}},
{4534,{583,0,0,0,0},{4650,0}},
{6187,{702,541,0,0,0},{2713,16050}},
{5887,{702,0,0,0,0},{2713,0}},
{5887,{0,722,0,0,0},{2100,0}},
{198,{1022,480,0,0,0},{8170,9600}},
{216,{2,24,40,0,0},{8170,9600}},
{1510,{44,60,40,80,85},{7801,10800}},
{1532,{302,208,300,0,0},{7914,0}},
{1532,{302,209,0,0,0},{2813,0}},
{1532,{305,0,0,0,0},{10518,7564}},
{8411,{843,840,140,0,0},{10914,0}},
{165,{1122,500,0,0,0},{8170,9600}},
{1392,{0,0,0,0,0},{6000,0}},
{6300,{284,0,0,0,0},{16350,0}},
{8582,{0,0,0,0,0},{17700,0}},
{7800,{921,209,302,200,0},{8814,0}},
{7950,{0,0,0,0,0},{2700,0}},
{5908,{621,1143,1000,0,0},{4553,0}},
{5266,{0,0,0,0,0},{1800,0}},
{6300,{342,0,0,0,0},{18450,0}},
{1200,{0,0,0,0,0},{17185,450}},
{6300,{124,0,0,0,0},{16350,0}},
{9450,{208,1040,1060,0,0},{10919,0}},
{6300,{184,242,0,0,0},{3600,0}},
{7800,{921,140,0,0,0},{7410,0}},
{6300,{24,0,0,0,0},{18300,0}},
{158,{82,60,0,0,0},{8170,9600}},
{4510,{63,0,0,0,0},{300,0}},
{3450,{0,0,0,0,0},{2700,0}},
{6300,{0,0,0,0,0},{450,0}},
{163,{22,100,0,0,0},{8170,9600}},
{8100,{0,0,0,0,0},{2836,0}},
{4650,{0,0,0,0,0},{128,0}},
{1538,{563,560,0,0,0},{7914,0}},
{5860,{1253,63,1220,1234,0},{17153,4500}},
{4060,{63,0,0,0,0},{730,6900}},
{4574,{1223,0,0,0,0},{18000,0}},
{4103,{903,0,0,0,0},{730,0}},
{4575,{1243,0,0,0,0},{10631,0}},
{5860,{1233,63,0,0,0},{494,0}},
{4125,{1243,0,0,0,0},{730,0}},
{2550,{0,0,0,0,0},{2700,0}},
{8850,{0,0,0,0,0},{2832,0}},
{4063,{22,0,0,0,0},{647,0}},
{4092,{742,0,0,0,0},{647,0}},
{9676,{0,0,0,0,0},{20163,0}},
{5250,{0,0,0,0,0},{2832,0}},
{1578,{462,0,0,0,0},{760,9150}},
{10063,{0,0,0,0,0},{2837,0}},
{8250,{0,0,0,0,0},{17103,0}},
{9450,{209,1040,1060,300,1100},{10872,17100}},
{4050,{0,0,0,0,0},{11514,20250}}
};

static char NounText[16];
#ifdef FULL_ADV
static int LightRefill = 150;
static int Counters[16];	/* Range unknown */
static int CurrentCounter;
static int SavedRoom;
static int RoomSaved[16];	/* Range unknown */
#endif
static char Redraw;		/* Update item window */

#define MyLoc	(GameHeader.PlayerRoom)

static int BitFlags=0;

#define LINE_LEN 21
#define BOTTOM_HEIGHT 30
#define SCROLL_DELAY 0
#define LINES_PER_SCREEN 8

static char bottom_screen[BOTTOM_HEIGHT * LINE_LEN];
static char paletteOk = 0;
static char autoscroll = 1;
static char scrolldelay = 0;
static int scroll_y = 0;
typedef enum {
	LAGOON,
	NO_LIGHT,
	TIDE,
	INSIDE
} background_t;
static background_t background;
static char command_line[LINE_LEN];
extern char treaback[];

typedef struct {
    unsigned char b0;
    unsigned char b1;
    unsigned char b2;
    void *next;
    void *bitmap;
    int posx, posy, sizex, sizey;
    char palette[8];
} sprite_t;

#ifdef USE_GRAPHICS
static sprite_t Sbackgr = {
    0xc0, 0x10, 0x20,
    0, &treaback,
    0, 0, 0x100, 0x100,
    {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef}
};
#endif
#define tgi_sprite(spr) tgi_ioctl(0, (unsigned)(spr))

static char pal[] = {
	0x000 >> 8,
	0x030 >> 8,
	0x090 >> 8,
	0x0D1 >> 8,
	0x330 >> 8,
	0x282 >> 8,
	0x363 >> 8,
	0x651 >> 8,
	0x666 >> 8,
	0x696 >> 8,
	0x999 >> 8,
	0x9C9 >> 8,
	0xB8B >> 8,
	0xBAB >> 8,
	0xC6C >> 8,
	0xFFF >> 8,

	0x000 & 0xff,
	0x030 & 0xff,
	0x090 & 0xff,
	0x0D1 & 0xff,
	0x330 & 0xff,
	0x282 & 0xff,
	0x363 & 0xff,
	0x651 & 0xff,
	0x666 & 0xff,
	0x696 & 0xff,
	0x999 & 0xff,
	0x9C9 & 0xff,
	0xB8B & 0xff,
	0xBAB & 0xff,
	0xC6C & 0xff,
	0xFFF & 0xff,
};

static char normal_pal[] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef};
static char tide_pal[] = {0x01, 0x22, 0x45, 0x67, 0x89, 0xab, 0xcd, 0x3f};

static unsigned char lastInterrupt;

static void check_music()
{
    if (lastInterrupt != interruptOccurred) {
	abc_metronome();
	abc_sound();
	//check_next_tune();
	lastInterrupt = interruptOccurred;
    }
}

static void scroll()
{
	check_music();
	if (autoscroll & scroll_y > 0) {
		scroll_y--;
	}
	memmove(bottom_screen, bottom_screen+LINE_LEN, sizeof(bottom_screen-LINE_LEN));
	memset(bottom_screen+(BOTTOM_HEIGHT - 1) * LINE_LEN, 0, LINE_LEN);
}

static int bottomcurx, bottomcury;

static void wmove(int y, int x)
{
	bottomcurx = x;
	bottomcury = y;
}

static void wclrtoeol()
{
	memset(bottom_screen + bottomcury * LINE_LEN + bottomcurx, ' ', LINE_LEN - bottomcurx);
}

static void wprintw(char* text)
{
	memcpy(bottom_screen + bottomcury * LINE_LEN + bottomcurx, text, strlen(text));
	bottomcurx += strlen(text);
	bottom_screen[bottomcury * LINE_LEN + bottomcurx] = 0;
}

static void werase()
{
	memset(bottom_screen, ' ', sizeof(bottom_screen));
	memset(command_line, ' ', sizeof(command_line));
	strcpy(command_line, ">");
}

static char next_time_slot;
#define GAME_SPEED 10
#define LINE_HEIGHT 10

static void wrefresh()
{
	int i;
	
	while (drawPending)
		;
	if (
		(MyLoc == 6) ||
		(MyLoc == 8) ||
		(MyLoc == 9) ||
		(MyLoc == 10) ||
		(MyLoc == 14) ||
		(MyLoc == 18) ||
		(MyLoc == 20) ||
		(MyLoc == 21) ||
		(MyLoc == 22) ||
		(MyLoc == 23) ||
		(MyLoc == 24) ||
		(MyLoc == 25))
	{
		background = LAGOON;
	} else {
		background = INSIDE;
	}
	if((BitFlags&(1<<DARKBIT)) && Items[LIGHT_SOURCE].Location!= CARRIED
	            && Items[LIGHT_SOURCE].Location!= MyLoc)
	{
		background = NO_LIGHT;
	} else {
		if (background == LAGOON) {
			if (Items[53].Location == 24)
				background = TIDE;
		}
	}
	check_music();
	switch (background) {
#ifdef USE_GRAPHICS
	case LAGOON:
		memcpy(Sbackgr.palette, normal_pal, 8);
		tgi_sprite(&Sbackgr);
		break;
	case TIDE:
		memcpy(Sbackgr.palette, tide_pal, 8);
		tgi_sprite(&Sbackgr);
		break;
#else
	case LAGOON:
		tgi_setcolor(7);
		tgi_bar(0, 0, 159, 101);
		break;
	case TIDE:
		tgi_setcolor(2);
		tgi_bar(0, 0, 159, 101);
		break;
#endif
	case NO_LIGHT:
		tgi_setcolor(0);
		tgi_bar(0, 0, 159, 101);
		break;
	case INSIDE:
		tgi_setcolor(4);
		tgi_bar(0, 0, 159, 101);
		break;
	}
	tgi_setcolor(COLOR_WHITE);
	for (i = 0; i < LINES_PER_SCREEN; i++) {
		check_music();
		bottom_screen[(scroll_y + i + 1) * LINE_LEN - 1] = 0;
		tgi_outtextxy(0, 8 + i * LINE_HEIGHT, (char *) bottom_screen + (scroll_y + i) * LINE_LEN);
	}
	command_line[LINE_LEN - 1] = 0;
	tgi_outtextxy(0, 8 + LINES_PER_SCREEN * LINE_HEIGHT, (char *) command_line);
	Redraw=0;
	drawPending = 1;
	if (paletteOk == 0) {
		//char *pal = (char *)tgi_getdefpalette();
		fade_in(pal, 100);
		tgi_setpalette(pal);
	}
	next_time_slot = interruptOccurred + GAME_SPEED;
}

static void ClearScreen(void)
{
	werase();
	wrefresh();
}

static int RandomPercent(int n)
{
	unsigned int rv=rand();
	rv%=100;
	if(rv<n)
		return(1);
	return(0);
}

static int CountCarried()
{
	int ct=0;
	int n=0;
	while(ct<=GameHeader.NumItems)
	{
		if(Items[ct].Location==CARRIED)
			n++;
		ct++;
	}
	return(n);
}

static char *MapSynonym(char *word)
{
	int n=1;
	char *tp;
	static char lastword[16];	/* Last non synonym */
	if(*word=='*')
		word++;
	while(n<=GameHeader.NumWords)
	{
		tp=Nouns[n];
		if(*tp=='*')
			tp++;
		else
			strcpy(lastword,tp);
		if(strncasecmp(word,tp,GameHeader.WordLength)==0)
			return(lastword);
		n++;
	}
	return(NULL);
}

static int MatchUpItem(char *text, int loc)
{
	char *word=MapSynonym(text);
	int ct=0;
	
	if(word==NULL)
		word=text;
	
	while(ct<=GameHeader.NumItems)
	{
		if(Items[ct].AutoGet && Items[ct].Location==loc &&
			strncasecmp(Items[ct].AutoGet,word,GameHeader.WordLength)==0)
			return(ct);
		ct++;
	}
	return(-1);
}

static int OutputPos=0;

static void OutReset()
{
	OutputPos=0;
	wmove(BOTTOM_HEIGHT-1,0);
	wclrtoeol();
}

static void OutBuf(char *buffer)
{
	char word[80];
	int wp;
	while(*buffer)
	{
		if(OutputPos==0)
		{
			while(*buffer && isspace(*buffer))
			{
				if(*buffer=='\n')
				{
					scroll();
					wmove(BOTTOM_HEIGHT-1,0);
					wclrtoeol();
					OutputPos=0;
				}
				buffer++;
			}
		}
		if(*buffer==0)
			return;
		wp=0;
		while(*buffer && !isspace(*buffer))
		{
			word[wp++]=*buffer++;
		}
		word[wp]=0;
		if(OutputPos+strlen(word)>(LINE_LEN-2))
		{
			scroll();
			wmove(BOTTOM_HEIGHT-1,0);
			wclrtoeol();
			OutputPos=0;
		}
		wprintw(word);
		OutputPos+=strlen(word);
		
		if(*buffer==0)
			return;
		
		if(*buffer=='\n')
		{
			scroll();
			wmove(BOTTOM_HEIGHT-1,0);
			wclrtoeol();
			OutputPos=0;
		}
		else
		{
			OutputPos++;
			if(OutputPos<(LINE_LEN-1)) {
				wprintw(" ");
			}
		}
		buffer++;
	}
}

#ifdef FULL_ADV
static void OutputNumber(int a)
{
	char buf[16];
	//sprintf(buf,"%d ",a);
	OutBuf(buf);
}
#endif
		
static void Look()
{
	static char *ExitNames[6]=
	{
		"North","South","East","West","Up","Down"
	};
	Room *r;
	int ct,f;
	int pos;
	
	werase();
	if((BitFlags&(1<<DARKBIT)) && Items[LIGHT_SOURCE].Location!= CARRIED
	            && Items[LIGHT_SOURCE].Location!= MyLoc)
	{
		OutBuf("I can't see. It is too dark!\n");
		wrefresh();
		return;
	}
	r=&Rooms[MyLoc];
	if(*r->Text=='*') {
		OutBuf(r->Text+1);
		OutBuf("\n");
	} else
	{
		OutBuf("I'm in a ");
		OutBuf(r->Text);
		OutBuf("\n");
	}
	ct=0;
	f=0;
	OutBuf("Obvious exits: ");
	while(ct<6)
	{
		if(r->Exits[ct]!=0)
		{
			if(f==0) {
				f=1;
			} else {
				OutBuf(" ");
			}
			OutBuf(ExitNames[ct]);
		}
		ct++;
	}
	if(f==0) {
		OutBuf("none");
	}
	OutBuf("\n");
	ct=0;
	f=0;
	pos=0;
	while(ct<=GameHeader.NumItems)
	{
		if(Items[ct].Location==MyLoc)
		{
			if(f==0)
			{
				OutBuf("\nI can also see: ");
				pos=16;
				f++;
			}
			else
			{
				OutBuf(" - ");
				pos+=3;
			}
			if(pos+strlen(Items[ct].Text)>(LINE_LEN-10))
			{
				pos=0;
				OutBuf("\n");
			}
			OutBuf(Items[ct].Text);
			pos += strlen(Items[ct].Text);
		}
		ct++;
	}
	OutBuf("\n");
	wrefresh();
}

static int WhichWord(char *word, char **list)
{
	int n=1;
	int ne=1;
	char *tp;
	if (*word=='*')
		word++;
	while(ne<=GameHeader.NumWords)
	{
		tp=list[ne];
		if(*tp=='*')
			tp++;
		else
			n=ne;
		if(strncasecmp(word,tp,GameHeader.WordLength)==0)
			return(n);
		ne++;
	}
	return(-1);
}

extern int read_eeprom(unsigned char pos);

extern char write_eeprom(unsigned char pos, int val);

static void hex(int i)
{
	char tmp[32];
	itoa(i, tmp, 16);
	OutBuf(" 0x");
	OutBuf(tmp);
	OutBuf("\n");
}

typedef union {
	int words;
	char bytes[2];
} save_t;

static char SaveGame()
{
	int ct;
	unsigned char next;
	save_t val;
	char fail = 0;
	next = 0;
	
	fail |= write_eeprom(next++, 0x0001); // Magic
#ifdef FULL_ADV
 	for(ct=0;ct<16;ct++)
		{
			write_eeprom(next++, Counters[ct] * 256 + RoomSaved[ct]);
		}

	write_eeprom(next++, (short)SavedRoom * 256 + CurrentCounter);
#endif
	fail |= write_eeprom(next++, BitFlags);
	val.bytes[0] = MyLoc;
	val.bytes[1] = GameHeader.LightTime;
	fail |= write_eeprom(next++, val.words);
	ct = 0;
	for(;next<SAVED_ITEMS+3;next++) {
		val.bytes[0] = Items[ct++].Location;
		val.bytes[1] = Items[ct++].Location;
		fail |= write_eeprom(next, val.words);
	}
	return fail;
}

static char LoadGame()
{
	unsigned char next;
	int ct;
	char lfail;
	save_t val;

	next = 0;	
	ct = read_eeprom(next);
	if (ct != 0x0001) {
		OutBuf("Wrong magic");
		hex(ct);
		return 1;
	}

	// Sanity check eeprom
	next = 0;
	next++; // Magic
	next++; // BitFlag
	val.words = read_eeprom(next++);
	lfail = (val.bytes[0] >= GameHeader.NumRooms);
	if (lfail) {
		OutBuf("Impossible MyLoc");
		hex(val.bytes[0]);
		return 1;
	}
	lfail = (val.bytes[1] > GameHeader.LightTime);
	if (lfail) {
		OutBuf("Impossible LightTime");
		hex(val.bytes[1]);
		return 1;
	}
	for(;next<SAVED_ITEMS+3;next++) {
		val.words = read_eeprom(next);
		lfail = (val.bytes[0] > GameHeader.NumRooms) &&
			(val.bytes[0] != 0xff);
		if (lfail) {
			OutBuf("Impossible Location");
			hex(val.bytes[0]);
			return 1;
		}
		lfail = (val.bytes[1] > GameHeader.NumRooms) &&
			(val.bytes[1] != 0xff);
		if (lfail) {
			OutBuf("Impossible Location");
			hex(val.bytes[0]);
			return 1;
		}
	}
	
	next = 0;
	next++;
#ifdef FULL_ADV
	for(ct=0;ct<16;ct++)
	{
		Counters[ct] = read_eeprom(next) / 256;
		RoomSaved[ct] = read_eeprom(next++) % 256;
	}
	SavedRoom = read_eeprom(next) / 256;
	CurrentCounter = read_eeprom(next++) % 256;
#endif
	BitFlags = read_eeprom(next++);
	val.words = read_eeprom(next++);
	MyLoc = val.bytes[0];
	GameHeader.LightTime = val.bytes[1];
	ct = 0;
	for(;next<SAVED_ITEMS+3;next++) {
		val.words = read_eeprom(next);
		Items[ct++].Location = val.bytes[0];
		Items[ct++].Location = val.bytes[1];
	}
	return 0;
}

static int PerformLine(int ct)
{
	int continuation=0;
	int param[5],pptr=0;
	int act[4];
	int cc=0;
	while(cc<5)
	{
		int cv,dv;
		cv=Actions[ct].Condition[cc];
		dv=cv/20;
		cv%=20;
		switch(cv)
		{
			case 0:
				param[pptr++]=dv;
				break;
			case 1:
				if(Items[dv].Location!=CARRIED)
					return(0);
				break;
			case 2:
				if(Items[dv].Location!=MyLoc)
					return(0);
				break;
			case 3:
				if(Items[dv].Location!=CARRIED&&
					Items[dv].Location!=MyLoc)
					return(0);
				break;
			case 4:
				if(MyLoc!=dv)
					return(0);
				break;
			case 5:
				if(Items[dv].Location==MyLoc)
					return(0);
				break;
			case 6:
				if(Items[dv].Location==CARRIED)
					return(0);
				break;
			case 7:
				if(MyLoc==dv)
					return(0);
				break;
			case 8:
				if((BitFlags&(1<<dv))==0)
					return(0);
				break;
			case 9:
				if(BitFlags&(1<<dv))
					return(0);
				break;
			case 10:
				if(CountCarried()==0)
					return(0);
				break;
			case 11:
				if(CountCarried())
					return(0);
				break;
			case 12:
				if(Items[dv].Location==CARRIED||Items[dv].Location==MyLoc)
					return(0);
				break;
			case 13:
				if(Items[dv].Location==0)
					return(0);
				break;
			case 14:
				if(Items[dv].Location)
					return(0);
				break;
#ifdef FULL_ADV
			case 15:
				if(CurrentCounter>dv)
					return(0);
				break;
			case 16:
				if(CurrentCounter<=dv)
					return(0);
				break;
#endif
			case 17:
				if(Items[dv].Location!=Items[dv].InitialLoc)
					return(0);
				break;
			case 18:
				if(Items[dv].Location==Items[dv].InitialLoc)
					return(0);
				break;
#ifdef FULL_ADV
			case 19:/* Only seen in Brian Howarth games so far */
				if(CurrentCounter!=dv)
					return(0);
				break;
#endif
		}
		cc++;
	}
	/* Actions */
	act[0]=Actions[ct].Action[0];
	act[2]=Actions[ct].Action[1];
	act[1]=act[0]%150;
	act[3]=act[2]%150;
	act[0]/=150;
	act[2]/=150;
	cc=0;
	pptr=0;
	while(cc<4)
	{
		if(act[cc]>=1 && act[cc]<52)
		{
			OutBuf(Messages[act[cc]]);
			OutBuf("\n");
		}
		else if(act[cc]>101)
		{
			OutBuf(Messages[act[cc]-50]);
			OutBuf("\n");
		}
		else switch(act[cc])
		{
			case 0:/* NOP */
				break;
			case 52: //GET
				if(CountCarried()==GameHeader.MaxCarry)
				{
					OutBuf("I've too much to carry! ");
					break;
				}
				if(Items[param[pptr]].Location==MyLoc)
					Redraw=1;
				Items[param[pptr++]].Location= CARRIED;
				break;
			case 53: // MOVE_INTO_AR
				Redraw=1;
				Items[param[pptr++]].Location=MyLoc;
				break;
			case 54: // GOTO
				Redraw=1;
				MyLoc=param[pptr++];
				break;
			case 55: //REMOVE
				if(Items[param[pptr]].Location==MyLoc)
					Redraw=1;
				Items[param[pptr++]].Location=0;
				break;
			case 56: // SET_NIGHT
				BitFlags|=1<<DARKBIT;
				break;
			case 57: // SET_DAY
				BitFlags&=~(1<<DARKBIT);
				break;
			case 58: // SET_BIT
				BitFlags|=(1<<param[pptr++]);
				break;
			case 59: // REMOVE
				if(Items[param[pptr]].Location==MyLoc)
					Redraw=1;
				Items[param[pptr++]].Location=0;
				break;
			case 60: // CLEAR_BIT
				BitFlags&=~(1<<param[pptr++]);
				break;
			case 61: // KILL_PLAYER
				OutBuf("I am dead.\n");
				BitFlags&=~(1<<DARKBIT);
				MyLoc=GameHeader.NumRooms;/* It seems to be what the code says! */
				Look();
				break;
			case 62: // MOVE_X_INTO_Y
			{
				/* Bug fix for some systems - before it could get parameters wrong */
				int i=param[pptr++];
				Items[i].Location=param[pptr++];
				Redraw=1;
				break;
			}
			case 63: // QUIT
doneit:				OutBuf("The game is now over.\n");
				wrefresh();
				return -3;
			case 64: // LOOK
				Look();
				break;
			case 65: // SCORE
			{
				int ct=0;
				int n=0;
				while(ct<=GameHeader.NumItems)
				{
					if(Items[ct].Location==GameHeader.TreasureRoom &&
					  *Items[ct].Text=='*')
					  	n++;
					ct++;
				}
				if(n==GameHeader.Treasures)
				{
					OutBuf("Your adventure is over. But the Black Pearl is still to be found.\n");
					OutBuf("The compass points to 83 143 57 233\n");
					OutBuf("The stamp shows an image of Port 1234.\n");
					goto doneit;
				} else {
					OutBuf("You need to bring back two treasures to win the game.\n");
					if (n == 1) {
						OutBuf("Now you have only one.\n");
					} else {
						OutBuf("Now you have none.\n");
					}
				}
				break;
			}
			case 66: // INVENTORY
			{
				int ct=0;
				int f=0;
				OutBuf("I'm carrying:\n");
				while(ct<=GameHeader.NumItems)
				{
					if(Items[ct].Location==CARRIED)
					{
						if(f==1)
						{
							OutBuf(" - ");
						}
						f=1;
						OutBuf(Items[ct].Text);
					}
					ct++;
				}
				if(f==0)
					OutBuf("Nothing");
				OutBuf(".\n");
				break;
			}
			case 67: // SET_BIT
				BitFlags|=(1<<0);
				break;
			case 68: // CLEAR_BIT
				BitFlags&=~(1<<0);
				break;
#ifdef FULL_ADV
			case 69:
				GameHeader.LightTime=LightRefill;
				if(Items[LIGHT_SOURCE].Location==MyLoc)
					Redraw=1;
				Items[LIGHT_SOURCE].Location=CARRIED;
				//BitFlags&=~(1<<LIGHTOUTBIT);
				break;
#endif
			case 70: // CLS
				ClearScreen(); /* pdd. */
				OutReset();
				break;
			case 71: // SAVE
				if (SaveGame()) {
					OutBuf("Cannot save game to eeprom\n");
				} else {
					OutBuf("Game saved\n");
				}
				break;
			case 72: // SWAP_ITEMS
			{
				int i1=param[pptr++];
				int i2=param[pptr++];
				int t=Items[i1].Location;
				if(t==MyLoc || Items[i2].Location==MyLoc)
					Redraw=1;
				Items[i1].Location=Items[i2].Location;
				Items[i2].Location=t;
				break;
			}
#ifdef FULL_ADV
			case 73:
				continuation=1;
				break;
			case 74:
				if(Items[param[pptr]].Location==MyLoc)
					Redraw=1;
				Items[param[pptr++]].Location= CARRIED;
				break;
#endif
			case 75: // PUT_X_WITH_Y
			{
				int i1,i2;
				i1=param[pptr++];
				i2=param[pptr++];
				if(Items[i1].Location==MyLoc)
					Redraw=1;
				Items[i1].Location=Items[i2].Location;
				if(Items[i2].Location==MyLoc)
					Redraw=1;
				break;
			}
			case 76:	// LOOK
				Look();
				break;
#ifdef FULL_ADV
			case 77:
				if(CurrentCounter>=0)
					CurrentCounter--;
				break;
			case 78:
				OutputNumber(CurrentCounter);
				break;
			case 79:
				CurrentCounter=param[pptr++];
				break;
			case 80:
			{
				int t=MyLoc;
				MyLoc=SavedRoom;
				SavedRoom=t;
				Redraw=1;
				break;
			}
			case 81:
			{
				/* This is somewhat guessed. Claymorgue always
				   seems to do select counter n, thing, select counter n,
				   but uses one value that always seems to exist. Trying
				   a few options I found this gave sane results on ageing */
				int t=param[pptr++];
				int c1=CurrentCounter;
				CurrentCounter=Counters[t];
				Counters[t]=c1;
				break;
			}
			case 82:
				CurrentCounter+=param[pptr++];
				break;
			case 83:
				CurrentCounter-=param[pptr++];
				if(CurrentCounter< -1)
					CurrentCounter= -1;
				/* Note: This seems to be needed. I don't yet
				   know if there is a maximum value to limit too */
				break;
			case 84:
				OutBuf(NounText);
				break;
#endif
			case 85: // ECHO_NOUN_CR
				OutBuf(NounText);
				OutBuf("\n");
				break;
#ifdef FULL_ADV
			case 86:
				OutBuf("\n");
				break;
			case 87:
			{
				/* Changed this to swap location<->roomflag[x]
				   not roomflag 0 and x */
				int p=param[pptr++];
				int sr=MyLoc;
				MyLoc=RoomSaved[p];
				RoomSaved[p]=sr;
				Redraw=1;
				break;
			}
			case 88:
				wrefresh();
				wrefresh();
				//sleep(2);	/* DOC's say 2 seconds. Spectrum times at 1.5 */
				break;
			case 89:
				pptr++;
				/* SAGA draw picture n */
				/* Spectrum Seas of Blood - start combat ? */
				/* Poking this into older spectrum games causes a crash */
				break;
#endif
			default:
				//fprintf(stderr,"Unknown action %d [Param begins %d %d]\n",
				//	act[cc],param[pptr],param[pptr+1]);
				break;
		}
		cc++;
	}
	return(1+continuation);		
}

static int PerformActions(int vb,int no)
{
	static int disable_sysfunc=0;	/* Recursion lock */
	int d=BitFlags&(1<<DARKBIT);
	
	int ct=0;
	int fl;
	int doagain=0;
	
	if(vb==1 && no == -1 )
	{
		OutBuf("Give me a direction too.\n");
		wrefresh();
		return(0);
	}
	if (vb==VERB_LOAD && no == NOUN_GAME) {
		if (LoadGame()) {
			OutBuf("No saved game found.\n");
		} else {
			OutBuf("Game loaded.\n");
		}
		wrefresh();
		return(0);
	}
	if(vb==1 && no>=1 && no<=6)
	{
		int nl;
		if(Items[LIGHT_SOURCE].Location==MyLoc ||
		   Items[LIGHT_SOURCE].Location==CARRIED)
		   	d=0;
		if(d)
			OutBuf("Dangerous to move in the dark!\n");
		nl=Rooms[MyLoc].Exits[no-1];
		if(nl!=0)
		{
			MyLoc=nl;
			Look();
			return(0);
		}
		if(d)
		{
			OutBuf("I fell down and broke my neck.\n");
			wrefresh();
			//sleep(5);
			//endwin();
			return -3;
		}
		OutBuf("I can't go in that direction.\n");
		wrefresh();
		return(0);
	}
	fl= -1;
	while(ct<=GameHeader.NumActions)
	{
		int vv,nv;
		vv=Actions[ct].Vocab;
		/* Think this is now right. If a line we run has an action73
		   run all following lines with vocab of 0,0 */
		if(vb!=0 && (doagain&&vv!=0))
			break;
		/* Oops.. added this minor cockup fix 1.11 */
		if(vb!=0 && !doagain && fl== 0)
			break;
		nv=vv%150;
		vv/=150;
		if((vv==vb)||(doagain&&Actions[ct].Vocab==0))
		{
			if((vv==0 && RandomPercent(nv))||doagain||
				(vv!=0 && (nv==no||nv==0)))
			{
				int f2;
				if(fl== -1)
					fl= -2;
				f2=PerformLine(ct);
				if(f2>0)
				{
					/* ahah finally figured it out ! */
					fl=0;
					if(f2==2)
						doagain=1;
					if(vb!=0 && doagain==0)
						return 0;
				}
				if (f2 == -3)
					fl = -3;
			}
		}
		ct++;
		if(Actions[ct].Vocab!=0)
			doagain=0;
	}
	if(fl!=0 && disable_sysfunc==0)
	{
		int i;
		if(Items[LIGHT_SOURCE].Location==MyLoc ||
		   Items[LIGHT_SOURCE].Location==CARRIED)
		   	d=0;
		if(vb==10 || vb==18)
		{
			/* Yes they really _are_ hardcoded values */
			if(vb==10)
			{
				if(strcasecmp(NounText,"ALL")==0)
				{
					int ct=0;
					int f=0;
					
					if(d)
					{
						OutBuf("It is dark.\n");
						return 0;
					}
					while(ct<=GameHeader.NumItems)
					{
						if(Items[ct].Location==MyLoc && Items[ct].AutoGet!=NULL && Items[ct].AutoGet[0]!='*')
						{
							no=WhichWord(Items[ct].AutoGet,Nouns);
							disable_sysfunc=1;	/* Don't recurse into auto get ! */
							PerformActions(vb,no);	/* Recursively check each items table code */
							disable_sysfunc=0;
							if(CountCarried()==GameHeader.MaxCarry)
							{
								OutBuf("I've too much to carry.\n");
								return(0);
							}
						 	Items[ct].Location= CARRIED;
						 	Redraw=1;
						 	OutBuf(Items[ct].Text);
						 	OutBuf(": O.K.\n");
						 	f=1;
						 }
						 ct++;
					}
					if(f==0)
						OutBuf("Nothing taken.\n");
					return(0);
				}
				if(no==-1)
				{
					OutBuf("What ?\n");
					return(0);
				}
				if(CountCarried()==GameHeader.MaxCarry)
				{
					OutBuf("I've too much to carry.\n");
					return(0);
				}
				i=MatchUpItem(NounText,MyLoc);
				if(i==-1)
				{
					OutBuf("It's beyond my power to do that.\n");
					return(0);
				}
				Items[i].Location= CARRIED;
				OutBuf("O.K.\n");
				Redraw=1;
				return(0);
			}
			if(vb==18)
			{
				if(strcasecmp(NounText,"ALL")==0)
				{
					int ct=0;
					int f=0;
					while(ct<=GameHeader.NumItems)
					{
						if(Items[ct].Location==CARRIED && Items[ct].AutoGet && Items[ct].AutoGet[0]!='*')
						{
							no=WhichWord(Items[ct].AutoGet,Nouns);
							disable_sysfunc=1;
							PerformActions(vb,no);
							disable_sysfunc=0;
							Items[ct].Location=MyLoc;
							OutBuf(Items[ct].Text);
							OutBuf(": O.K.\n");
							Redraw=1;
							f=1;
						}
						ct++;
					}
					if(f==0)
						OutBuf("Nothing dropped.\n");
					return(0);
				}
				if(no==-1)
				{
					OutBuf("What ?\n");
					return(0);
				}
				i=MatchUpItem(NounText,CARRIED);
				if(i==-1)
				{
					OutBuf("It's beyond my power to do that.\n");
					return(0);
				}
				Items[i].Location=MyLoc;
				OutBuf("O.K.\n");
				Redraw=1;
				return(0);
			}
		}
	}
	return(fl);
}

#define TRUE 1
#define FALSE 0
typedef enum {SCROLL_MODE, VERB_MODE, NOUN_MODE, END_GAME} mode_t;

static mode_t gamemode = SCROLL_MODE;

typedef enum {
	FIRST_LOOK,
	TIME_ADVANCE,
	SECOND_LOOK,
	USER_INPUT,
	USER_ACTION,
	LIGHT_FADE} state_t;

static state_t gamestate = FIRST_LOOK;

void display_input(int verb_index, int noun_index, char *verb, char *noun)
{
	int len;

	strcpy(command_line, ">");
	if (gamemode == NOUN_MODE) {
		strcpy(noun, Nouns[noun_index]);
		strcpy(verb, Verbs[verb_index]);
		len = strlen(verb);
		verb[len++] = ' ';
		verb[len] = 0;
		if (verb[0] == '*') {
			strcpy(command_line+1, verb+1);
			len--;
		} else {
			strcpy(command_line+1, verb);
		}
		if (noun[0] == '*') {
			strcpy(command_line+len+1, noun+1);
		} else {
			strcpy(command_line+len+1, noun);
		}
	} else {
		strcpy(verb, Verbs[verb_index]);
		if (verb[0] == '*') {
			strcpy(command_line+1, verb+1);
		} else {
			strcpy(command_line+1, verb);
		}
	}
	wrefresh();
}

#ifdef SOLUTION
static int solution_index = -1;
extern char solution[];
#endif

int treasure(void)
{
	int vb,no;
	char verb[16],noun[16];
	int verb_index;
	int sorted_verb_index = 20;
	int sorted_noun_index = 56;
	int noun_index;

#ifdef USE_GRAPHICS
        asm("lda #<_TREASUREBG_FILENR");
        asm("ldx #0");
        asm("jsr _FileLoadFile");
#endif
#ifdef SOLUTION
        asm("lda #<_SOLUTION_FILENR");
        asm("ldx #0");
        asm("jsr _FileLoadFile");
#endif
	werase();
	wmove(BOTTOM_HEIGHT-1,0);
	OutReset();
	srand(1234);
	Look();
	verb_index = VerbsSorted[sorted_verb_index];
	noun_index = NounsSorted[sorted_noun_index];
	while(1)
	{
		unsigned char joy;

		check_music();
		switch (gamestate) {
		case FIRST_LOOK:
			if(Redraw!=0)
			{
				Look();
			}
			gamestate = TIME_ADVANCE;
			break;
		case TIME_ADVANCE:
			PerformActions(0,0);
			wrefresh();
			gamestate = SECOND_LOOK;
			break;
		case SECOND_LOOK:
			if(Redraw!=0)
			{
				Look();
			}
			gamestate = USER_INPUT;
			break;
		case USER_INPUT:
			if (interruptOccurred == next_time_slot) {
				joy = joy_read(JOY_1);
				next_time_slot = interruptOccurred + GAME_SPEED;
				if ((gamemode == SCROLL_MODE || gamemode == END_GAME) & autoscroll && scroll_y < BOTTOM_HEIGHT - LINES_PER_SCREEN) {
					if (scrolldelay == 0) {
						scroll_y ++;
						wrefresh();
						scrolldelay = SCROLL_DELAY;
					} else {
						scrolldelay--;
					}
				}
			} else {
				joy = 0;
			}
			if (JOY_BTN_UP(joy)) {
				if (JOY_BTN_FIRE2(joy)) {
					if (scroll_y > 0) {
						scroll_y--;
						wrefresh();
					}
					autoscroll = 0;
				} else {
					switch (gamemode) {
					case END_GAME:
						break;
					case SCROLL_MODE:
						scroll_y = BOTTOM_HEIGHT - LINES_PER_SCREEN;
						gamemode = VERB_MODE;
						display_input(verb_index, noun_index, verb, noun);
						break;
					case VERB_MODE:
						if (sorted_verb_index > 0)
							sorted_verb_index--;
						else
							sorted_verb_index = 61;
						verb_index = VerbsSorted[sorted_verb_index];
						display_input(verb_index, noun_index, verb, noun);
						break;
					case NOUN_MODE:
						if (sorted_noun_index > 0)
							sorted_noun_index--;
						else
							sorted_noun_index = 82;
						noun_index = NounsSorted[sorted_noun_index];
						display_input(verb_index, noun_index, verb, noun);
						break;
					}
				}
			}
			if (JOY_BTN_DOWN(joy)) {
				if (JOY_BTN_FIRE2(joy)) {
					if (scroll_y < BOTTOM_HEIGHT - LINES_PER_SCREEN) {
						scroll_y++;
						wrefresh();
					}
					autoscroll = 0;
				} else {
					switch (gamemode) {
					case END_GAME:
						break;
					case SCROLL_MODE:
						scroll_y = BOTTOM_HEIGHT - LINES_PER_SCREEN;
						gamemode = VERB_MODE;
						display_input(verb_index, noun_index, verb, noun);
						break;
					case VERB_MODE:
						if (sorted_verb_index < 61)
							sorted_verb_index++;
						else
							sorted_verb_index = 0;
						verb_index = VerbsSorted[sorted_verb_index];
						display_input(verb_index, noun_index, verb, noun);
						break;
					case NOUN_MODE:
						if (sorted_noun_index < 82)
							sorted_noun_index++;
						else
							sorted_noun_index = 0;
						noun_index = NounsSorted[sorted_noun_index];
						display_input(verb_index, noun_index, verb, noun);
						break;
					}
				}
			}
			if (JOY_BTN_LEFT(joy)) {
				switch (gamemode) {
				case END_GAME:
				case SCROLL_MODE:
					break;
				case VERB_MODE:
					strcpy(command_line, ">");
					gamemode = SCROLL_MODE;
					wrefresh();
					break;
				case NOUN_MODE:
					gamemode = VERB_MODE;
					display_input(verb_index, noun_index, verb, noun);
					break;
				}
			}
			if (JOY_BTN_RIGHT(joy)) {
				switch (gamemode) {
				case SCROLL_MODE:
					scroll_y = BOTTOM_HEIGHT - LINES_PER_SCREEN;
					gamemode = VERB_MODE;
					display_input(verb_index, noun_index, verb, noun);
					break;
				case VERB_MODE:
					if (strlen(verb) > 1) {
						gamemode = NOUN_MODE;
						display_input(verb_index, noun_index, verb, noun);
					}
					break;
				case END_GAME:
				case NOUN_MODE:
					break;
				}
			}
			if (JOY_BTN_FIRE(joy)) {
				switch (gamemode) {
				case END_GAME:
					break;
				case SCROLL_MODE:
					OutReset();
					OutBuf("Use joypad first");
					wrefresh();
					break;
				case VERB_MODE:
					vb=WhichWord(Verbs[verb_index],Verbs);
					no=-1;
					NounText[0] = 0;
					gamestate = USER_ACTION;
					OutBuf(command_line);
					OutBuf("\n");
					strcpy(command_line, ">"); 
					autoscroll = 1;
					break;
				case NOUN_MODE:
					if (!strcmp(Verbs[verb_index], "load")) {
						vb = VERB_LOAD;
					} else {
						vb=WhichWord(Verbs[verb_index],Verbs);
					}
					no=WhichWord(Nouns[noun_index],Nouns);
					strcpy(NounText,Nouns[noun_index]);	/* Needed by GET/DROP hack */
					gamestate = USER_ACTION;
					OutBuf(command_line);
					OutBuf("\n");
					strcpy(command_line, ">"); 
					autoscroll = 1;
					break;
				}
			}
			if (kbhit()) {
			    switch (cgetc()) {
			    case 'F':
				tgi_flip();
				break;
			    case 'R':
				fade_out(100);
				return RESTART_LYNX;
#ifdef SOLUTION
			    case '3':
				if (solution_index == -1)
					solution_index = 0;
				else
					solution_index = -1;
				break;
#endif
			    case '2':
				play_next_tune();
				break;
			    case '1':
				if (gamemode == END_GAME)
					break;
#ifdef SOLUTION
				if (solution_index >= 0) {
					gamemode = NOUN_MODE;
					verb_index = solution[solution_index++];
					noun_index = solution[solution_index++];
					if (noun_index > 0)
						gamemode = NOUN_MODE;
					else
						gamemode = VERB_MODE;
					display_input(verb_index, noun_index, verb, noun);
					OutBuf(command_line);
					OutBuf("\n");
					strcpy(command_line, ">"); 
					wrefresh();
					vb=WhichWord(Verbs[verb_index],Verbs);
					if (noun_index > 0) {
						no=WhichWord(Nouns[noun_index],Nouns);
						strcpy(NounText,Nouns[noun_index]);	/* Needed by GET/DROP hack */
					} else {
						no = -1;
						NounText[0] = 0;
					}
					gamestate = USER_ACTION;
					autoscroll = 1;
				} else {
#endif
					OutBuf("look\n");
					strcpy(command_line, ">"); 
					wrefresh();
					vb=WhichWord(Verbs[VERB_LOOK],Verbs);
					no=-1;
					NounText[0] = 0;
					gamestate = USER_ACTION;
					autoscroll = 1;
#ifdef SOLUTION
				}
#endif
				break;
			    default:
				break;
			    }
			}
			break;
		case USER_ACTION:
			switch(PerformActions(vb,no))
			{
				case -1:
					OutBuf("I don't understand your command.\n");
					wrefresh();
					gamemode = SCROLL_MODE;
					break;
				case -2:
					OutBuf("I can't do that yet.\n");
					wrefresh();
					gamemode = SCROLL_MODE;
					break;
				case -3: // End of game
					autoscroll = 1;
					gamemode = END_GAME;
					break;
				default:
					gamemode = SCROLL_MODE;
					break;
			}
			wrefresh();
			gamestate = LIGHT_FADE;
			break;
		case LIGHT_FADE:
			/* Brian Howarth games seem to use -1 for forever */
			if(Items[LIGHT_SOURCE].Location/*==-1*/!=DESTROYED && GameHeader.LightTime!= -1)
			{
				GameHeader.LightTime--;
				if(GameHeader.LightTime<1)
				{
					//BitFlags|=(1<<LIGHTOUTBIT);
					if(Items[LIGHT_SOURCE].Location==CARRIED ||
						Items[LIGHT_SOURCE].Location==MyLoc)
					{
						OutBuf("Your light has run out. ");
						wrefresh();
					}
				}
				else if(GameHeader.LightTime<25)
				{
					if(Items[LIGHT_SOURCE].Location==CARRIED ||
						Items[LIGHT_SOURCE].Location==MyLoc)
					{
						if(GameHeader.LightTime%5==0) {
							OutBuf("Your light is growing dim. ");
							wrefresh();
						}
					}
				}
			}
			gamestate = FIRST_LOOK;
			break;
		}
	}
}
