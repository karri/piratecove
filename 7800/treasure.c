/*
 *	ScottFree Revision 1.14
 *
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version
 *	2 of the License, or (at your option) any later version.
 */

//#define MODE320
//#define USESFX
#include <atari7800.h>
#include <6502.h>
#include <conio.h>
#include <joystick.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#include <signal.h>
#include <time.h>
#include "treasure.h"
#include "outbuf.c"
#ifdef MODE320
#include "alcove.c"
#include "attic.c"
#include "crack.c"
#include "island.c"
#include "london.c"
#include "tide.c"
#include "maze.c"
#include "shack.c"
#include "meadow.c"
#include "ship.c"
#include "monastery.c"
#endif
#include "rooms.c"
#include "verbs.c"
#include "nouns.c"
#include "items.c"
#include "actions.c"

extern unsigned char sfx_advpickup[];
extern unsigned char sfx_advdrop[];
extern unsigned char sfx_teleported[];
extern unsigned char sfx_hahaha[];
extern unsigned char sfx_plonk[];
extern unsigned char sfx_simplebuzz[];
extern unsigned char sfx_falling[];
extern unsigned char sfx_yahoo[];
extern void __fastcall__ playsfx0(unsigned char *);

#define TREASURE_ROOM 1
#define TREASURES 2

static int NounToken;
static char Redraw;		/* Update item window */

static unsigned char MyLoc = 1;
static signed int LightTime = 150;

static int BitFlags = 0;

static unsigned char readselect() {
	if ((RIOT.swchb & 0x02) == 0) {
		return 1;
	}
	return 0;
}

static void refreshbottom(unsigned char area)
{
	switch (area) {
	case ROOM_FLAT:
		MARIA.bkgrnd = 0x0c;
		MARIA.p0c1 = MARIA.p0c3 = 0x03;
		MARIA.p0c2 = 0x33;
#ifdef MODE320
		memcpy(screen + (25 * LINE_LEN), londonbot, sizeof(londonbot));
#else
		gotoxy(0, 27);
		cputs("*------------------*");
#endif
		break;
	case ROOM_ALCOVE:
		MARIA.bkgrnd = 0x1a;
		MARIA.p0c1 = MARIA.p0c3 = 0x05;
		MARIA.p0c2 = 0xb2;
#ifdef MODE320
		memcpy(screen + (25 * LINE_LEN), londonbot, sizeof(londonbot));
#else
		gotoxy(0, 27);
		cputs("*------------------*");
#endif
		break;
	case ROOM_PASSAGEWAY:
		MARIA.bkgrnd = 0x00;
		MARIA.p0c1 = MARIA.p0c3 = 0x08;
		MARIA.p0c2 = 0xb8;
#ifdef MODE320
		memcpy(screen + (25 * LINE_LEN), londonbot, sizeof(londonbot));
#else
		gotoxy(0, 27);
		cputs("*------------------*");
#endif
		break;
	case ROOM_CAVERN:
		MARIA.bkgrnd = 0x80;
		MARIA.p0c1 = MARIA.p0c3 = 0x0a;
		MARIA.p0c2 = 0x0f;
#ifdef MODE320
		memcpy(screen + (25 * LINE_LEN), tidebot, sizeof(tidebot));
#else
		gotoxy(0, 27);
		cputs("*------------------*");
#endif
		break;
	case ROOM_NOLIGHT:
		MARIA.bkgrnd = 0x00;
		MARIA.p0c1 = MARIA.p0c3 = 0x05;
		MARIA.p0c2 = 0x07;
#ifdef MODE320
		memset(screen + (25 * LINE_LEN), 0, sizeof(londonbot));
#else
		gotoxy(0, 27);
		cputs("                    ");
#endif
		break;
	case ROOM_TIDE:
		MARIA.bkgrnd = 0x74;
		MARIA.p0c1 = MARIA.p0c3 = 0xed;
		MARIA.p0c2 = 0x2a;
#ifdef MODE320
		memcpy(screen + (25 * LINE_LEN), tidebot, sizeof(tidebot));
#else
		gotoxy(0, 27);
		cputs("*------------------*");
#endif
		break;
	case ROOM_LAGOON:
		MARIA.bkgrnd = 0x74;
		MARIA.p0c1 = MARIA.p0c3 = 0xed;
		MARIA.p0c2 = 0x2a;
#ifdef MODE320
		memcpy(screen + (25 * LINE_LEN), tidebot, sizeof(tidebot));
#else
		gotoxy(0, 27);
		cputs("*------------------*");
#endif
		break;
	case ROOM_MEADOW:
		MARIA.bkgrnd = 0x7f;
		MARIA.p0c1 = MARIA.p0c3 = 0xc5;
		MARIA.p0c2 = 0xc2;
#ifdef MODE320
		memcpy(screen + (25 * LINE_LEN), tidebot, sizeof(tidebot));
#else
		gotoxy(0, 27);
		cputs("*------------------*");
#endif
		break;
	}
}

static void wrefresh()
{
	unsigned char location = MyLoc;

	if ((BitFlags & (1 << DARKBIT))
	    && ItemsLocation[LIGHT_SOURCE] != CARRIED
	    && ItemsLocation[LIGHT_SOURCE] != MyLoc) {
		location = ROOM_NOLIGHT;
	}
	textcolor(1);
	gotoxy(0, 0);
#ifdef MODE320
	switch (location) {
	default:
		break;
	case ROOM_GRAVEYARD:
	case ROOM_FIELD:
	case ROOM_MONASTERY:
		memcpy(screen, monasterytop, sizeof(monasterytop));
		refreshbottom(ROOM_TIDE);
		break;
	case ROOM_NONE:
	case ROOM_NEVERLAND:
	case ROOM_FLAT:
	case ROOM_LEDGE:
		memcpy(screen, londontop, sizeof(londontop));
		refreshbottom(ROOM_FLAT);
		break;
	case ROOM_ALCOVE:
		memcpy(screen, alcovetop, sizeof(alcovetop));
		refreshbottom(ROOM_ALCOVE);
		break;
	case ROOM_PASSAGEWAY:
	case ROOM_HALLWAY:
		memcpy(screen, cracktop, sizeof(cracktop));
		refreshbottom(ROOM_PASSAGEWAY);
		break;
	case ROOM_ATTIC:
	case ROOM_SHED:
		memcpy(screen, attictop, sizeof(attictop));
		refreshbottom(ROOM_ALCOVE);
		break;
	case ROOM_SHACK:
		memcpy(screen, shacktop, sizeof(shacktop));
		refreshbottom(ROOM_ALCOVE);
		break;
	case ROOM_MEADOW:
		memcpy(screen, meadowtop, sizeof(meadowtop));
		refreshbottom(ROOM_MEADOW);
		break;
	case ROOM_MAZE1:
	case ROOM_MAZE2:
	case ROOM_MAZE3:
	case ROOM_MAZE4:
	case ROOM_CAVERN:
	case ROOM_PIT:
		memcpy(screen, mazetop, sizeof(mazetop));
		refreshbottom(ROOM_CAVERN);
		break;
	case ROOM_BEACH:
	case ROOM_OCEAN:
	case ROOM_LAGOON:
		memcpy(screen, tidetop, sizeof(tidetop));
		refreshbottom(ROOM_TIDE);
		break;
	case ROOM_HILL:
	case ROOM_HILLTOP:
	case ROOM_MUERTA:
		memcpy(screen, islandtop, sizeof(islandtop));
		refreshbottom(ROOM_LAGOON);
		break;
		break;
	case ROOM_SHIP:
		memcpy(screen, shiptop, sizeof(shiptop));
		refreshbottom(ROOM_TIDE);
		break;
	case ROOM_NOLIGHT:
		memset(screen, 0, sizeof(islandtop));
		refreshbottom(ROOM_NOLIGHT);
		break;
	}
#else
	switch (location) {
	case ROOM_HALLWAY:
	case ROOM_GRAVEYARD:
	case ROOM_FIELD:
	case ROOM_MUERTA:
	case ROOM_MONASTERY:
	case ROOM_NEVERLAND:
		break;
	default:
		break;
	case ROOM_NONE:
	case ROOM_FLAT:
	case ROOM_LEDGE:
		cputs("   ||| ------ |||   ");
		gotoxy(0, 1);
		cputs("|||    LONDON    |||");
		refreshbottom(ROOM_FLAT);
		break;
	case ROOM_ALCOVE:
		cputs("*------------------*");
		gotoxy(0, 1);
		cputs("|||    ALCOVE    |||");
		refreshbottom(ROOM_ALCOVE);
		break;
	case ROOM_PASSAGEWAY:
		cputs("*------------------*");
		gotoxy(0, 1);
		cputs("|||  PASSAGEWAY  |||");
		refreshbottom(ROOM_PASSAGEWAY);
		break;
	case ROOM_ATTIC:
		cputs("*------------------*");
		gotoxy(0, 1);
		cputs("|||    ATTIC     |||");
		refreshbottom(ROOM_ALCOVE);
		break;
	case ROOM_SHACK:
		cputs("*------------------*");
		gotoxy(0, 1);
		cputs("|||    SHACK     |||");
		refreshbottom(ROOM_ALCOVE);
		break;
	case ROOM_MEADOW:
		cputs("   ||| ------ |||   ");
		gotoxy(0, 1);
		cputs("|||    MEADOW    |||");
		refreshbottom(ROOM_MEADOW);
		break;
	case ROOM_MAZE1:
	case ROOM_MAZE2:
	case ROOM_MAZE3:
	case ROOM_MAZE4:
		cputs("*------------------*");
		gotoxy(0, 1);
		cputs("|||     MAZE     |||");
		refreshbottom(ROOM_CAVERN);
		break;
	case ROOM_CAVERN:
		cputs("*------------------*");
		gotoxy(0, 1);
		cputs("|||    CAVERN    |||");
		refreshbottom(ROOM_CAVERN);
		break;
	case ROOM_PIT:
		cputs("*------------------*");
		gotoxy(0, 1);
		cputs("|||     PIT      |||");
		refreshbottom(ROOM_CAVERN);
		break;
	case ROOM_SHED:
		cputs("*------------------*");
		gotoxy(0, 1);
		cputs("|||    SHED      |||");
		refreshbottom(ROOM_ALCOVE);
		break;
	case ROOM_BEACH:
		cputs("*------------------*");
		gotoxy(0, 1);
		cputs("|||    BEACH     |||");
		refreshbottom(ROOM_TIDE);
		break;
	case ROOM_HILL:
	case ROOM_HILLTOP:
		cputs("*------------------*");
		gotoxy(0, 1);
		cputs("|||    HILL      |||");
		refreshbottom(ROOM_LAGOON);
		break;
	case ROOM_LAGOON:
		cputs("*------------------*");
		gotoxy(0, 1);
		cputs("|||    LAGOON    |||");
		refreshbottom(ROOM_LAGOON);
		break;
	case ROOM_OCEAN:
		cputs("*------------------*");
		gotoxy(0, 1);
		cputs("|||     OCEAN    |||");
		refreshbottom(ROOM_TIDE);
		break;
	case ROOM_SHIP:
		cputs("*------------------*");
		gotoxy(0, 1);
		cputs("|||     SHIP     |||");
		refreshbottom(ROOM_TIDE);
		break;
	case ROOM_NOLIGHT:
		cputs("                    ");
		gotoxy(0, 1);
		cputs("                    ");
		refreshbottom(ROOM_NOLIGHT);
		break;
	}
#endif
	textcolor(2);
	gotoxy(0, TOP_HEIGHT + STORY_LINES - 1);
	Redraw = 0;
}

static void ClearScreen(void)
{
	memset(screen, 0, 24 * LINE_LEN);
	wrefresh();
}

static int RandomPercent(int n)
{
	unsigned int rv = rand();
	rv %= 100;
	if (rv < n)
		return (1);
	return (0);
}

static int CountCarried()
{
	int ct = 0;
	int n = 0;
	while (ct < ITEMS_TOTAL) {
		if (ItemsLocation[ct] == CARRIED)
			n++;
		ct++;
	}
	return (n);
}

static void Look()
{
	static const char *const ExitNames[6] = {
		"North", "South", "East", "West", "Up", "Down"
	};
	const Room *r;
	int ct, f;

	textcolor(2);
	wrefresh();
	if ((BitFlags & (1 << DARKBIT))
	    && ItemsLocation[LIGHT_SOURCE] != CARRIED
	    && ItemsLocation[LIGHT_SOURCE] != MyLoc) {
		OutBuf("I can't see. It is too dark!\n");
		wrefresh();
		return;
	}
	r = &Rooms[MyLoc];
	if (*r->Text == '*') {
		OutBuf(r->Text + 1);
		OutBuf("\n");
	} else {
		OutBuf("I'm in a ");
		OutBuf(r->Text);
		OutBuf("\n");
	}
	ct = 0;
	f = 0;
	OutBuf("Obvious exits: ");
	while (ct < 6) {
		if (r->Exits[ct] != 0) {
			if (f == 0) {
				f = 1;
			} else {
				OutBuf(" ");
			}
			OutBuf(ExitNames[ct]);
		}
		ct++;
	}
	if (f == 0) {
		OutBuf("none");
	}
	OutBuf("\n");
	LookItems(MyLoc);
}

#define WORDS_TOTAL 80

static int WhichWord(const char *word, const char *const *const list)
{
	int n = 1;
	int ne = 1;
	const char *tp;
	if (*word == '*')
		word++;
	while (ne < WORDS_TOTAL) {
		tp = list[ne];
		if (*tp == '*')
			tp++;
		else
			n = ne;
		if (strncasecmp(word, tp, WordLen) == 0)
			return (n);
		ne++;
	}
	return (-1);
}

static int PerformLine(int ct)
{
	int continuation = 0;
	int param[5], pptr = 0;
	int act[4];
	int cc = 0;
	while (cc < 5) {
		int cv, dv;
		cv = Actions[ct].Condition[cc];
		dv = cv / 20;
		cv %= 20;
		switch (cv) {
		case 0:
			param[pptr++] = dv;
			break;
		case 1:
			if (ItemsLocation[dv] != CARRIED)
				return (0);
			break;
		case 2:
			if (ItemsLocation[dv] != MyLoc)
				return (0);
			break;
		case 3:
			if (ItemsLocation[dv] != CARRIED &&
			    ItemsLocation[dv] != MyLoc)
				return (0);
			break;
		case 4:
			if (MyLoc != dv)
				return (0);
			break;
		case 5:
			if (ItemsLocation[dv] == MyLoc)
				return (0);
			break;
		case 6:
			if (ItemsLocation[dv] == CARRIED)
				return (0);
			break;
		case 7:
			if (MyLoc == dv)
				return (0);
			break;
		case 8:
			if ((BitFlags & (1 << dv)) == 0)
				return (0);
			break;
		case 9:
			if (BitFlags & (1 << dv))
				return (0);
			break;
		case 10:
			if (CountCarried() == 0)
				return (0);
			break;
		case 11:
			if (CountCarried())
				return (0);
			break;
		case 12:
			if (ItemsLocation[dv] == CARRIED
			    || ItemsLocation[dv] == MyLoc)
				return (0);
			break;
		case 13:
			if (ItemsLocation[dv] == 0)
				return (0);
			break;
		case 14:
			if (ItemsLocation[dv])
				return (0);
			break;
		case 17:
			if (ItemsLocation[dv] != ItemsInitialLoc[dv])
				return (0);
			break;
		case 18:
			if (ItemsLocation[dv] == ItemsInitialLoc[dv])
				return (0);
			break;
		}
		cc++;
	}
	/* Actions */
	act[0] = Actions[ct].Action[0];
	act[2] = Actions[ct].Action[1];
	act[1] = act[0] % 150;
	act[3] = act[2] % 150;
	act[0] /= 150;
	act[2] /= 150;
	cc = 0;
	pptr = 0;
	while (cc < 4) {
		switch (act[cc]) {
		case 0:	/* NOP */
			break;
		case 1:
#ifdef USESFX
			playsfx0(sfx_plonk);
#endif
			OutBuf("There's a strange sound\n");
			break;
		case 2:
			OutBuf("The book is in bad condition but I can make out the title:\n'Isla de Muerta'.\nThere's a word written in blood in the flyleaf: 'YOHO' and a message:'What you need is a compass that does not point North'\n");
			break;
		case 3:
			OutBuf("Nothing happens\n");
			break;
		case 4:
			OutBuf("There's something there all right. Maybe I should\n");
			break;
		case 5:
			OutBuf("That's not very smart\n");
			break;
		case 6:
			OutBuf("I may need to say a MAGIC word here!\n");
			break;
		case 7:
#ifdef USESFX
			playsfx0(sfx_teleported);
#endif
			OutBuf("Everything spins around and suddenly I'm elsewhere...\n");
			break;
		case 8:
			OutBuf("Torch is lit\n");
			break;
		case 9:
			OutBuf("I was wrong, I guess its not a mongoose cause the snakes bit it!\n");
			break;
		case 10:
			OutBuf("I'm snake bit\n");
			break;
		case 11:
			OutBuf("Parrot attacks snakes and drives them off\n");
			break;
		case 12:
			OutBuf("Pirate won't let me\n");
			break;
		case 13:
			OutBuf("Its locked\n");
			break;
		case 14:
			OutBuf("Its open\n");
			break;
		case 15:
			OutBuf("There are a set of plans in it\n");
			break;
		case 16:
			OutBuf("Not while I'm carrying it\n");
			break;
		case 17:
			OutBuf("Crocs stop me\n");
			break;
		case 18:
			OutBuf("Sorry I can't\n");
			break;
		case 19:
			OutBuf("Wrong game you silly goose!\n");
			break;
		case 20:
			OutBuf("I don't have it\n");
			break;
		case 21:
#ifdef USESFX
			playsfx0(sfx_hahaha);
#endif
			OutBuf("Pirate grabs rum and scuttles off chortling\n");
			break;
		case 22:
#ifdef USESFX
			playsfx0(sfx_hahaha);
#endif
			OutBuf("...I think its me, Hee Hee.\n");
			break;
		case 23:
			OutBuf("Its nailed to the floor!\n");
			break;
		case 24:
			OutBuf("Yoho ho and a ...\n");
			break;
		case 25:
			OutBuf("No, something is missing!\n");
			break;
		case 26:
			OutBuf("It was a tight squeeze!\n");
			break;
		case 27:
			OutBuf("Something won't fit\n");
			break;
		case 28:
			OutBuf("Since nothing is happening\n");
			break;
		case 29:
#ifdef USESFX
			playsfx0(sfx_falling);
#endif
			OutBuf("I slipped and fell...\n");
			break;
		case 30:
#ifdef USESFX
			playsfx0(sfx_plonk);
#endif
			OutBuf("Something falls out\n");
			break;
		case 31:
			OutBuf("They're plans to build the Jolly Roger (a Pirate ship!) You'll need: hammer, nails, lumber, anchor, sails, and a keel.\n");
			break;
		case 32:
			OutBuf("I've no container\n");
			break;
		case 33:
			OutBuf("It soaks into the ground\n");
			break;
		case 34:
			OutBuf("Too dry, fish vanish.\n");
			break;
		case 35:
#ifdef USESFX
			playsfx0(sfx_yahoo);
#endif
			OutBuf("Pirate awakens and says 'Aye matey we be casting off soon' He then VANISHES!\n");
			break;
		case 36:
			OutBuf("What a waste...\n");
			break;
		case 37:
			OutBuf("I've no crew\n");
			break;
		case 38:
			OutBuf("Pirate says: 'Aye matey we be needing a map first'.\n");
			break;
		case 39:
			OutBuf("After a day at sea we set anchor off of a sandy beach. All Ashore who's going Ashore...\n");
			break;
		case 40:
			OutBuf("Try: 'WEIGH ANCHOR'\n");
			break;
		case 41:
			OutBuf("There's a map in it\n");
			break;
		case 42:
			OutBuf("Its a map to Isla de Muerta. At the bottom it says: '30 paces then dig!'\n");
			break;
		case 43:
			OutBuf("\nBased on the Pirate Adventure by Alexis & Scott Adams.\nRemember you can always ask for 'help'.\n");
			break;
		case 44:
			OutBuf("Its empty\n");
			break;
		case 45:
			OutBuf("I've no plans!\n");
			break;
		case 46:
			OutBuf("open it?\n");
			break;
		case 47:
			OutBuf("go there?\n");
			break;
		case 48:
			OutBuf("I found something!\n");
			break;
		case 49:
			OutBuf("I didn't find anything\n");
			break;
		case 50:
			OutBuf("I don't see it here\n");
			break;
		case 51:
			OutBuf("OK I walked off 30 paces.\n");
			break;
		case 52:	//GET
			if (CountCarried() == MaxCarry) {
				OutBuf("I've too much to carry! ");
				break;
			}
			if (ItemsLocation[param[pptr]] == MyLoc) {
				Redraw = 1;
#ifdef USESFX
				playsfx0(sfx_advpickup);
#endif
			}
			ItemsLocation[param[pptr++]] = CARRIED;
			break;
		case 53:	// MOVE_INTO_AR
			Redraw = 1;
			ItemsLocation[param[pptr++]] = MyLoc;
			break;
		case 54:	// GOTO
			Redraw = 1;
			MyLoc = param[pptr++];
			break;
		case 55:	//REMOVE
			if (ItemsLocation[param[pptr]] == MyLoc) {
				Redraw = 1;
#ifdef USESFX
				playsfx0(sfx_advdrop);
#endif
			}
			ItemsLocation[param[pptr++]] = 0;
			break;
		case 56:	// SET_NIGHT
			BitFlags |= 1 << DARKBIT;
			break;
		case 57:	// SET_DAY
			BitFlags &= ~(1 << DARKBIT);
			break;
		case 58:	// SET_BIT
			BitFlags |= (1 << param[pptr++]);
			break;
		case 59:	// REMOVE
			if (ItemsLocation[param[pptr]] == MyLoc) {
				Redraw = 1;
#ifdef USESFX
				playsfx0(sfx_advdrop);
#endif
			}
			ItemsLocation[param[pptr++]] = 0;
			break;
		case 60:	// CLEAR_BIT
			BitFlags &= ~(1 << param[pptr++]);
			break;
		case 61:	// KILL_PLAYER
			OutBuf("I am dead.\n");
			BitFlags &= ~(1 << DARKBIT);
			MyLoc = ROOMS_TOTAL - 1;	/* It seems to be what the code says! */
			Look();
			break;
		case 62:	// MOVE_X_INTO_Y
			{
				/* Bug fix for some systems - before it could get parameters wrong */
				int i = param[pptr++];
				ItemsLocation[i] = param[pptr++];
				Redraw = 1;
				break;
			}
		case 63:	// QUIT
doneit:			OutBuf("The game is now over.\n");
			wrefresh();
			return -3;
		case 64:	// LOOK
			Look();
			break;
		case 65:	// SCORE
			{
				int ct = 0;
				int n = 0;
				while (ct < ITEMS_TOTAL) {
					if (ItemsLocation[ct] ==
					    TREASURE_ROOM
					    && *ItemsText[ct] == '*')
						n++;
					ct++;
				}
				if (n == TREASURES) {
					OutBuf
					    ("Your adventure is over.");
					goto doneit;
				} else {
					OutBuf
					    ("You need to bring back two treasures to win the game.\n");
					if (n == 1) {
						OutBuf
						    ("Now you have only one.\n");
					} else {
						OutBuf
						    ("Now you have none.\n");
					}
				}
				break;
			}
		case 66:	// INVENTORY
			{
				int ct = 0;
				int f = 0;
				OutBuf("I'm carrying:\n");
				while (ct < ITEMS_TOTAL) {
					if (ItemsLocation[ct] ==
					    CARRIED) {
						if (f == 1) {
							OutBuf(" - ");
						}
						f = 1;
						OutBuf(ItemsText[ct]);
					}
					ct++;
				}
				if (f == 0)
					OutBuf("Nothing");
				OutBuf(".\n");
				break;
			}
		case 67:	// SET_BIT
			BitFlags |= (1 << 0);
			break;
		case 68:	// CLEAR_BIT
			BitFlags &= ~(1 << 0);
			break;
		case 70:	// CLS
			ClearScreen();	/* pdd. */
			OutReset();
			break;
		case 71:	// SAVE
			OutBuf("Cannot save game to eeprom\n");
			break;
		case 72:	// SWAP_ITEMS
			{
				int i1 = param[pptr++];
				int i2 = param[pptr++];
				int t = ItemsLocation[i1];
				if (t == MyLoc
				    || ItemsLocation[i2] == MyLoc)
					Redraw = 1;
				ItemsLocation[i1] = ItemsLocation[i2];
				ItemsLocation[i2] = t;
				break;
			}
		case 75:	// PUT_X_WITH_Y
			{
				int i1, i2;
				i1 = param[pptr++];
				i2 = param[pptr++];
				if (ItemsLocation[i1] == MyLoc)
					Redraw = 1;
				ItemsLocation[i1] = ItemsLocation[i2];
				if (ItemsLocation[i2] == MyLoc)
					Redraw = 1;
				break;
			}
		case 76:	// LOOK
			Look();
			break;
		case 85:	// ECHO_NOUN_CR
			OutBuf(Nouns[NounToken]);
			OutBuf("\n");
			break;
		case 102:
			OutBuf("CONGRATULATIONS !!! But your Adventure is not over yet...\n");
			break;
		case 103:
			OutBuf("Reading expands the mind\n");
			break;
		case 104:
			OutBuf("The Parrot crys:\n");
			break;
		case 105:
			OutBuf("'Check the bag matey'\n");
			break;
		case 106:
			OutBuf("'Check the chest matey'\n");
			break;
		case 107:
			OutBuf("from the other side!\n");
			break;
		case 108:
			OutBuf("Open the book!\n");
			break;
		case 109:
			OutBuf("There's multiple exits here!\n");
			break;
		case 110:
			OutBuf("Crocs eat fish and leave\n");
			break;
		case 111:
			OutBuf("I'm underwater, I guess I don't swim well. Blub Blub...\n");
			break;
		case 112:
			OutBuf("'Pieces of eight'\n");
			break;
		case 113:
			OutBuf("Its stuck in the sand\n");
			break;
		case 114:
			OutBuf("OK\n");
			break;
		case 115:
			OutBuf("Pirate says: 'Aye me Buckeroo, we be waiting for the tide to come in!'\n");
			break;
		case 116:
			OutBuf("The tide is out\n");
			break;
		case 117:
			OutBuf("The tide is coming in\n");
			break;
		case 118:
			OutBuf("About 20 pounds. Try: 'SET SAIL'\n");
			break;
		case 119:
			OutBuf("'Tides be a changing matey'\n");
			break;
		case 120:
			OutBuf("Note here says: 'I be liking parrots, they be smart matey!'\n");
			break;
		case 121:
			OutBuf("Pirate follows me ashore as if expecting something\n");
			break;
		case 122:
			OutBuf("Climb stairs...\n");
			break;
		case 123:
			OutBuf("Got anything to eat matey?\n");
			break;
		case 124:
			OutBuf("Parrot attacks crocs but is beaten off\n");
			break;
		case 125:
			OutBuf("Bird flys off looking very unhappy\n");
			break;
		case 126:
			OutBuf("Parrot ate a cracker.\n");
			break;
		case 127:
			OutBuf("Yummy\n");
			break;
		case 128:
			OutBuf("I hear nothing now\n");
			break;
		case 129:
			OutBuf("Pirate says:'First Yee be getting that ACCURSED thing off me ship!'\n");
			break;
		case 130:
			OutBuf("read it?\n");
			break;
		case 131:
			OutBuf("Flyer says: 'Atari 7800 does not need a keyboard!'\n");
			break;
		case 132:
			OutBuf("I'm not feeling destructive!\n");
			break;
		case 133:
			OutBuf("'Check the book, matey!'\n");
			break;
		case 134:
			OutBuf("All right, POOF the GAME is destroyed!\n");
			break;
		case 135:
			OutBuf("I see nothing special\n");
			break;
		case 136:
			OutBuf("I don't know where to look!\n");
			break;
		case 137:
			OutBuf("Its stuck\n");
			break;
		default:
			//fprintf(stderr,"Unknown action %d [Param begins %d %d]\n",
			//      act[cc],param[pptr],param[pptr+1]);
			break;
		}
		cc++;
	}
	return (1 + continuation);
}

static int PerformActions(int vb, int no)
{
	static int disable_sysfunc = 0;	/* Recursion lock */
	int d = BitFlags & (1 << DARKBIT);

	int ct = 0;
	int fl;
	int doagain = 0;

	if (vb == 1 && no == -1) {
		OutBuf("Give me a direction too.\n");
		wrefresh();
		return (0);
	}
	if (vb == VERB_LOAD && no == NOUN_GAME) {
		OutBuf("No saved game found.\n");
		wrefresh();
		return (0);
	}
	if (vb == 1 && no >= 1 && no <= 6) {
		int nl;
		if (ItemsLocation[LIGHT_SOURCE] == MyLoc ||
		    ItemsLocation[LIGHT_SOURCE] == CARRIED)
			d = 0;
		if (d) {
			OutBuf("Dangerous to move in the dark!\n");
		}
		nl = Rooms[MyLoc].Exits[no - 1];
		if (nl != 0) {
			MyLoc = nl;
			Look();
			return (0);
		}
		if (d) {
			OutBuf("I fell down and broke my neck.\n");
			wrefresh();
			//sleep(5);
			//endwin();
			return -3;
		}
		OutBuf("I can't go in that direction.\n");
		wrefresh();
		return (0);
	}
	fl = -1;
	while (ct < ACTIONS_TOTAL) {
		int vv, nv;
		vv = Actions[ct].Vocab;
		/* Think this is now right. If a line we run has an action73
		   run all following lines with vocab of 0,0 */
		if (vb != 0 && (doagain && vv != 0))
			break;
		/* Oops.. added this minor cockup fix 1.11 */
		if (vb != 0 && !doagain && fl == 0)
			break;
		nv = vv % 150;
		vv /= 150;
		if ((vv == vb) || (doagain && Actions[ct].Vocab == 0)) {
			if ((vv == 0 && RandomPercent(nv)) || doagain ||
			    (vv != 0 && (nv == no || nv == 0))) {
				int f2;
				if (fl == -1)
					fl = -2;
				f2 = PerformLine(ct);
				if (f2 > 0) {
					/* ahah finally figured it out ! */
					fl = 0;
					if (f2 == 2)
						doagain = 1;
					if (vb != 0 && doagain == 0)
						return 0;
				}
				if (f2 == -3)
					fl = -3;
			}
		}
		ct++;
		if (Actions[ct].Vocab != 0)
			doagain = 0;
	}
	if (fl != 0 && disable_sysfunc == 0) {
		int i;
		if (ItemsLocation[LIGHT_SOURCE] == MyLoc ||
		    ItemsLocation[LIGHT_SOURCE] == CARRIED)
			d = 0;
		if (vb == 10 || vb == 18) {
			/* Yes they really _are_ hardcoded values */
			if (vb == 10) {
				if (strcasecmp(Nouns[NounToken], "ALL") == 0) {
					int ct = 0;
					int f = 0;

					if (d) {
						OutBuf("It is dark.\n");
						return 0;
					}
					while (ct < ITEMS_TOTAL) {
						if (ItemsLocation[ct] == MyLoc
						    && ItemsAutoGet[ct] != NULL
						    && ItemsAutoGet[ct][0] !=
						    '*') {
							no = WhichWord
							    (ItemsAutoGet[ct],
							     Nouns);
							disable_sysfunc = 1;	/* Don't recurse into auto get ! */
							PerformActions(vb, no);	/* Recursively check each items table code */
							disable_sysfunc = 0;
							if (CountCarried() ==
							    MaxCarry) {
								OutBuf
								    ("I've too much to carry.\n");
								return (0);
							}
							ItemsLocation[ct] =
							    CARRIED;
							Redraw = 1;
#ifdef USESFX
							playsfx0(sfx_advpickup);
#endif
							OutBuf(ItemsText[ct]);
							OutBuf(": O.K.\n");
							f = 1;
						}
						ct++;
					}
					if (f == 0)
						OutBuf("Nothing taken.\n");
					return (0);
				}
				if (no == -1) {
					OutBuf("What ?\n");
					return (0);
				}
				if (CountCarried() == MaxCarry) {
					OutBuf("I've too much to carry.\n");
					return (0);
				}
				i = MatchUpItem(Nouns[NounToken], MyLoc);
				if (i == -1) {
					OutBuf
					    ("It's beyond my power to do that.\n");
					return (0);
				}
				ItemsLocation[i] = CARRIED;
#ifdef USESFX
				playsfx0(sfx_advpickup);
#endif
				OutBuf("O.K.\n");
				Redraw = 1;
				return (0);
			}
			if (vb == 18) {
				if (strcasecmp(Nouns[NounToken], "ALL") == 0) {
					int ct = 0;
					int f = 0;
					while (ct < ITEMS_TOTAL) {
						if (ItemsLocation[ct] == CARRIED
						    && ItemsAutoGet[ct]
						    && ItemsAutoGet[ct][0] !=
						    '*') {
							no = WhichWord
							    (ItemsAutoGet[ct],
							     Nouns);
							disable_sysfunc = 1;
							PerformActions(vb, no);
							disable_sysfunc = 0;
							ItemsLocation[ct] =
							    MyLoc;
							OutBuf(ItemsText[ct]);
							OutBuf(": O.K.\n");
							Redraw = 1;
							f = 1;
						}
						ct++;
					}
					if (f == 0) {
						OutBuf("Nothing dropped.\n");
					} else {
#ifdef USESFX
				playsfx0(sfx_advdrop);
#endif
					}
					return (0);
				}
				if (no == -1) {
					OutBuf("What ?\n");
					return (0);
				}
				i = MatchUpItem(Nouns[NounToken], CARRIED);
				if (i == -1) {
					OutBuf
					    ("It's beyond my power to do that.\n");
					return (0);
				}
				ItemsLocation[i] = MyLoc;
#ifdef USESFX
				playsfx0(sfx_advdrop);
#endif
				OutBuf("O.K.\n");
				Redraw = 1;
				return (0);
			}
		}
	}
	return (fl);
}

#define TRUE 1
#define FALSE 0
typedef enum { SCROLL_MODE, VERB_MODE, NOUN_MODE, END_GAME } mode_t;

static mode_t gamemode = SCROLL_MODE;

typedef enum {
	FIRST_LOOK,
	TIME_ADVANCE,
	SECOND_LOOK,
	USER_INPUT,
	USER_ACTION,
	LIGHT_FADE
} state_t;

static state_t gamestate = FIRST_LOOK;

static void cleancl()
{
	memset(screen + (TOP_HEIGHT + STORY_LINES - 1) * LINE_LEN, 0, LINE_LEN);
	cursor(1);
	gotoxy(0, TOP_HEIGHT + STORY_LINES - 1);
	textcolor(1);
	cputc('?');
	textcolor(2);
	gotoxy(1, TOP_HEIGHT + STORY_LINES - 1);
}

void display_input(int verb_index, int noun_index)
{
	cleancl();
	textcolor(1);
	if (gamemode == NOUN_MODE) {
		if (Verbs[verb_index][0] == '*') {
			cputs(&Verbs[verb_index][1]);
		} else {
			cputs(Verbs[verb_index]);
		}
		cputc(' ');
		if (Nouns[noun_index][0] == '*') {
			cputs(&Nouns[noun_index][1]);
		} else {
			cputs(Nouns[noun_index]);
		}
	} else {
		if (Verbs[verb_index][0] == '*') {
			cputs(&Verbs[verb_index][1]);
		} else {
			cputs(Verbs[verb_index]);
		}
	}
	textcolor(2);
}

static int solution_index = -1;
extern char solution[];

static clock_t now;

static void first_look()
{
	if (Redraw != 0) {
		Look();
	}
	gamestate = TIME_ADVANCE;
}

static void time_advance()
{
	PerformActions(0, 0);
	wrefresh();
	gamestate = SECOND_LOOK;
}

static void second_look()
{
	if (Redraw != 0) {
		Look();
	}
	gamestate = USER_INPUT;
	cleancl();
}

static int verb_index;
static int noun_index;
static int vb, no;
static int sorted_verb_index = 20;
static int sorted_noun_index = 56;

static unsigned char WaitforRelease;

static void user_input()
{
	unsigned char joy;

	joy = joy_read(JOY_1) | joy_read(JOY_2);
	if ((joy == 0) && (readselect() == 0)) WaitforRelease = 0;

	if (JOY_UP(joy)) {
		switch (gamemode) {
		case END_GAME:
			break;
		case SCROLL_MODE:
			gamemode = VERB_MODE;
			display_input(verb_index, noun_index);
			break;
		case VERB_MODE:
			if (sorted_verb_index > 0)
				sorted_verb_index--;
			else
				sorted_verb_index = 61;
			verb_index = VerbsSorted[sorted_verb_index];
			display_input(verb_index, noun_index);
			break;
		case NOUN_MODE:
			if (sorted_noun_index > 0)
				sorted_noun_index--;
			else
				sorted_noun_index = 82;
			noun_index = NounsSorted[sorted_noun_index];
			display_input(verb_index, noun_index);
			break;
		}
	}
	if (JOY_DOWN(joy)) {
		switch (gamemode) {
		case END_GAME:
			break;
		case SCROLL_MODE:
			gamemode = VERB_MODE;
			display_input(verb_index, noun_index);
			break;
		case VERB_MODE:
			if (sorted_verb_index < 61)
				sorted_verb_index++;
			else
				sorted_verb_index = 0;
			verb_index = VerbsSorted[sorted_verb_index];
			display_input(verb_index, noun_index);
			break;
		case NOUN_MODE:
			if (sorted_noun_index < 82)
				sorted_noun_index++;
			else
				sorted_noun_index = 0;
			noun_index = NounsSorted[sorted_noun_index];
			display_input(verb_index, noun_index);
			break;
		}
	}
	if (JOY_LEFT(joy)) {
		switch (gamemode) {
		default:
			break;
		case VERB_MODE:
			gamemode = SCROLL_MODE;
			wrefresh();
			break;
		case NOUN_MODE:
			gamemode = VERB_MODE;
			display_input(verb_index, noun_index);
			break;
		}
	}
	if (JOY_RIGHT(joy)) {
		switch (gamemode) {
		case SCROLL_MODE:
			gamemode = VERB_MODE;
			display_input(verb_index, noun_index);
			break;
		case VERB_MODE:
			gamemode = NOUN_MODE;
			display_input(verb_index, noun_index);
			break;
		default:
			break;
		}
	}
	if (JOY_BTN_1(joy) && !WaitforRelease) {
		WaitforRelease = 1;
		switch (gamemode) {
		case END_GAME:
			break;
		case SCROLL_MODE:
			OutReset();
			OutBuf("Use joypad first");
			wrefresh();
			break;
		case VERB_MODE:
			vb = WhichWord(Verbs[verb_index], Verbs);
			no = -1;
			NounToken = -1;
			gamestate = USER_ACTION;
			scroll();
			break;
		case NOUN_MODE:
			if (!strcmp(Verbs[verb_index], "load")) {
				vb = VERB_LOAD;
			} else {
				vb = WhichWord(Verbs[verb_index], Verbs);
			}
			no = WhichWord(Nouns[noun_index], Nouns);
			NounToken = noun_index;
			gamestate = USER_ACTION;
			scroll();
			break;
		}
	}
	if ((JOY_BTN_2(joy) || readselect()) && !WaitforRelease) {
		WaitforRelease = 1;
		if (solution_index == -1)
			solution_index = 0;
		//if (gamemode == END_GAME)
		//      break;
		if (solution_index >= 0) {
			gamemode = NOUN_MODE;
			verb_index = solution[solution_index++];
			noun_index = solution[solution_index++];
			if (noun_index > 0)
				gamemode = NOUN_MODE;
			else
				gamemode = VERB_MODE;
			display_input(verb_index, noun_index);
			wrefresh();
			vb = WhichWord(Verbs[verb_index], Verbs);
			if (noun_index > 0) {
				no = WhichWord(Nouns[noun_index], Nouns);
				NounToken = noun_index;
			} else {
				no = -1;
				NounToken = -1;
			}
			gamestate = USER_ACTION;
			scroll();
		} else {
			textcolor(1);
			OutBuf("look\n");
			textcolor(2);
			wrefresh();
			vb = WhichWord(Verbs[VERB_LOOK], Verbs);
			no = -1;
			NounToken = -1;
			gamestate = USER_ACTION;
		}
	}
	now = clock() + 5;
	while (clock() < now) ;
}

static void user_action()
{
	switch (PerformActions(vb, no)) {
	case -1:
		OutBuf("I don't understand your command.\n");
		wrefresh();
		gamemode = SCROLL_MODE;
		break;
	case -2:
		OutBuf("I can't do that yet.\n");
		wrefresh();
		gamemode = SCROLL_MODE;
		break;
	case -3:		// End of game
		gamemode = END_GAME;
		break;
	default:
		gamemode = SCROLL_MODE;
		break;
	}
	wrefresh();
	gamestate = LIGHT_FADE;
}

static void light_fade()
{
	/* Brian Howarth games seem to use -1 for forever */
	if (ItemsLocation[LIGHT_SOURCE] /*==-1*/ !=DESTROYED
	    && LightTime != -1) {
		LightTime--;
		if (LightTime < 1) {
			//BitFlags|=(1<<LIGHTOUTBIT);
			if (ItemsLocation[LIGHT_SOURCE] ==
			    CARRIED || ItemsLocation[LIGHT_SOURCE] == MyLoc) {
				OutBuf("Your light has run out. ");
				wrefresh();
			}
		} else if (LightTime < 25) {
			if (ItemsLocation[LIGHT_SOURCE] ==
			    CARRIED || ItemsLocation[LIGHT_SOURCE] == MyLoc) {
				if (LightTime % 5 == 0) {
					OutBuf("Your light is growing dim. ");
					wrefresh();
				}
			}
		}
	}
	gamestate = FIRST_LOOK;
}

void main(void)
{
	joy_install(joy_static_stddrv);
	cursor(0);
	now = clock() + 5;
	clrscr();
	OutReset();
	srand(1234);
	textcolor(2);
	//gotoxy(2,1);
	//OutBuf("Piratecove font");
	//for (i = 0; i < 128; i++) {
	//	screen[60 + i] = i+i;
	//}
	//while (1) ;
	Look();
	verb_index = VerbsSorted[sorted_verb_index];
	noun_index = NounsSorted[sorted_noun_index];
	while (1) {
		switch (gamestate) {
		case FIRST_LOOK:
			first_look();
			break;
		case TIME_ADVANCE:
			time_advance();
			break;
		case SECOND_LOOK:
			second_look();
			break;
		case USER_INPUT:
			user_input();
			break;
		case USER_ACTION:
			user_action();
			break;
		case LIGHT_FADE:
			light_fade();
			break;
		}
	}
}
