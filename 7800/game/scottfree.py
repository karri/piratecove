import sys
import subprocess

class scottfree:
	def __init__(self, fname, compiler):
		self.name = fname
		self.compiler = compiler
		with open('../../advdata/' + fname + '.sco', 'r') as f:
			self.data = [line for line in f.readlines() if line.strip()]
		self.index = 0
		self.parseheader()
		self.parseactions()
		self.parsevocab()
		self.parserooms()
		self.parsemessages()
		self.parseitems()
		self.parsetitles()

	def getcompiler(self):
		return self.compiler

	def parseheader(self):
		self.advsize = int(self.data[0])
		self.nritems = int(self.data[1])
		self.nractions = int(self.data[2])
		self.nrvocab = int(self.data[3])
		self.nrrooms = int(self.data[4])
		self.nrcarry = int(self.data[5])
		self.startroom = int(self.data[6])
		self.nrtreasures = int(self.data[7])
		self.wordlen = int(self.data[8])
		self.timelimit = int(self.data[9])
		self.nrmessages = int(self.data[10])
		self.treasureroom = int(self.data[11])
		self.index = 12

	def printheader(self):
		print("*** HEADER ***")
		print("Total size", self.advsize)
		print("Items", self.nritems)
		print("Actions", self.nractions)
		print("Rooms", self.nrrooms)
		print("Vocabulary", self.nrvocab)
		print("Carry", self.nrcarry)
		print("Starting room", self.startroom)
		print("Treasures", self.nrtreasures)
		print("Word length", self.wordlen)
		print("Timelimit", self.timelimit)
		print("Messages", self.nrmessages)
		print("Treasureroom", self.treasureroom)

	def parseactionline(self):
		words = self.data[self.index].split(' ')
		self.index = self.index + 1
		actionvec = []
		for i in range(8):
			actionvec.append(int(words[i]))
		return actionvec

	def parseactions(self):
		self.actions=[]
		for i in range(self.nractions + 1):
			vec = []
			actionvec = self.parseactionline()
			vn = actionvec[0]
			v = int(vn / 150)
			vec.append(v)
			n = vn - 150*v
			vec.append(n)
			cn1 = actionvec[1]
			n1 = int(cn1 / 20)
			vec.append(n1)
			c1 = cn1 - n1 * 20
			vec.append(c1)
			cn2 = actionvec[2]
			n2 = int(cn2 / 20)
			vec.append(n2)
			c2 = cn2 - n2 * 20
			vec.append(c2)
			cn3 = actionvec[3]
			n3 = int(cn3 / 20)
			vec.append(n3)
			c3 = cn3 - n3 * 20
			vec.append(c3)
			cn4 = actionvec[4]
			n4 = int(cn4 / 20)
			vec.append(n4)
			c4 = cn4 - n4 * 20
			vec.append(c4)
			cn5 = actionvec[5]
			n5 = int(cn5 / 20)
			vec.append(n5)
			c5 = cn5 - n5 * 20
			vec.append(c5)
			cmd12 = actionvec[6]
			cmd1 = int(cmd12 / 150)
			vec.append(cmd1)
			cmd2 = cmd12 - 150 * cmd1
			vec.append(cmd2)
			cmd34 = actionvec[7]
			cmd3 = int(cmd34 / 150)
			vec.append(cmd3)
			cmd4 = cmd34 - 150 * cmd3
			vec.append(cmd4)
			self.actions.append(vec)

	def writeactionsheader(self, f):
		f.write('\n')
		f.write('#define RANDOM 0\n')
		f.write('#define EMPTY 0, 0\n')
		f.write('\n')
		f.write('#define GETX 52\n')
		f.write('#define DROPX 53\n')
		f.write('#define GOTOY 54\n')
		f.write('#define XTORM0 55\n')
		f.write('#define NIGHT 56\n')
		f.write('#define DAY 57\n')
		f.write('#define SETZ 58\n')
		f.write('#define XTORM02 59\n')
		f.write('#define CLRZ 60\n')
		f.write('#define DEAD 61\n')
		f.write('#define XTOY 62\n')
		f.write('#define FINI 63\n')
		f.write('#define DISPRM 64\n')
		f.write('#define SCORE 65\n')
		f.write('#define INV 66\n')
		f.write('#define SET0 67\n')
		f.write('#define CLR0 68\n')
		f.write('#define FILL 69\n')
		f.write('#define CLS 70\n')
		f.write('#define SAVE 71\n')
		f.write('#define EXX 72\n')
		f.write('#define CONT 73\n')
		f.write('#define AGETX 74\n')
		f.write('#define BYXFX 75\n')
		f.write('#define DISPRM2 76\n')
		f.write('#define CSUB1 77\n')
		f.write('#define DISPC 78\n')
		f.write('#define CFP1 79\n')
		f.write('#define EXRM0 80\n')
		f.write('#define EXMC 81\n')
		f.write('#define CADD 82\n')
		f.write('#define CSUB 83\n')
		f.write('#define SAYN 84\n')
		f.write('#define SAYNCR 85\n')
		f.write('#define SAYCR 86\n')
		f.write('#define EXCC 87\n')
		f.write('#define DELAY 88\n')
		f.write('#define DRAWPIC 89\n')
		f.write('\n')
		f.write('#define PAR 0\n')
		f.write('#define HAS 1\n')
		f.write('#define INW 2\n')
		f.write('#define AVL 3\n')
		f.write('#define IN 4\n')
		f.write('#define NINW 5\n')
		f.write('#define NHAS 6\n')
		f.write('#define NIN 7\n')
		f.write('#define BIT 8\n')
		f.write('#define NBIT 9\n')
		f.write('#define ANY 10\n')
		f.write('#define NANY 11\n')
		f.write('#define NAVL 12\n')
		f.write('#define NRM0 13\n')
		f.write('#define RM0 14\n')
		f.write('#define CTLE 15\n')
		f.write('#define CTGT 16\n')
		f.write('#define ORIG 17\n')
		f.write('#define NORIG 18\n')
		f.write('#define CTEQ 19\n')

	def mapcond(self, cond):
		if cond == 0:
			return 'PAR'
		if cond == 1:
			return 'HAS'
		if cond == 2:
			return 'INW'
		if cond == 3:
			return 'AVL'
		if cond == 4:
			return 'IN'
		if cond == 5:
			return 'NINW'
		if cond == 6:
			return 'NHAS'
		if cond == 7:
			return 'NIN'
		if cond == 8:
			return 'BIT'
		if cond == 9:
			return 'NBIT'
		if cond == 10:
			return 'ANY'
		if cond == 11:
			return 'NANY'
		if cond == 12:
			return 'NAVL'
		if cond == 13:
			return 'NRM0'
		if cond == 14:
			return 'RM0'
		if cond == 15:
			return 'CTLE'
		if cond == 16:
			return 'CTGT'
		if cond == 17:
			return 'ORIG'
		if cond == 18:
			return 'NORIG'
		if cond == 19:
			return 'CTEQ'
		return str(cond)

	def mapnr(self, cond, nr):
		if cond == 1:
			return self.itemsid[nr]
		if cond == 2:
			return self.itemsid[nr]
		if cond == 3:
			return self.itemsid[nr]
		if cond == 4:
			return self.roomname[nr]
		if cond == 5:
			return self.itemsid[nr]
		if cond == 6:
			return self.itemsid[nr]
		if cond == 7:
			return self.roomname[nr]
		if cond == 17:
			return self.itemsid[nr]
		if cond == 18:
			return self.itemsid[nr]
		return str(nr)

	def mapcode(self, code):
		if code < 52 and code > 0:
			return 'MSG'+str(code)
		if code >= 102 and code <= 149:
			return 'MSG'+str(code - 50)
		if code == 52:
			return 'GETX'
		if code == 53:
			return 'DROPX'
		if code == 54:
			return 'GOTOY'
		if code == 55:
			return 'XTORM0'
		if code == 56:
			return 'NIGHT'
		if code == 57:
			return 'DAY'
		if code == 58:
			return 'SETZ'
		if code == 59:
			return 'XTORM02'
		if code == 60:
			return 'CLRZ'
		if code == 61:
			return 'DEAD'
		if code == 62:
			return 'XTOY'
		if code == 63:
			return 'FINI'
		if code == 64:
			return 'DISPRM'
		if code == 66:
			return 'INV'
		if code == 65:
			return 'SCORE'
		if code == 67:
			return 'SET0'
		if code == 68:
			return 'CLR0'
		if code == 69:
			return 'FILL'
		if code == 70:
			return 'CLS'
		if code == 71:
			return 'SAVE'
		if code == 72:
			return 'EXX'
		if code == 73:
			return 'CONT'
		if code == 74:
			return 'AGETX'
		if code == 75:
			return 'BYXFX'
		if code == 76:
			return 'DISPRM2'
		if code == 77:
			return 'CSUB1'
		if code == 78:
			return 'DISPC'
		if code == 79:
			return 'CFP1'
		if code == 80:
			return 'EXRM0'
		if code == 81:
			return 'EXMC'
		if code == 82:
			return 'CADD'
		if code == 83:
			return 'CSUB'
		if code == 84:
			return 'SAYN'
		if code == 85:
			return 'SAYNCR'
		if code == 86:
			return 'SAYCR'
		if code == 87:
			return 'EXCC'
		if code == 88:
			return 'DELAY'
		return str(code)

	def writeactionscode(self, f):
		f.write('\n')
		f.write('unsigned char const Actions[] = {\n')
		for i in range(len(self.actions)):
			f.write('	//' + self.titles[i].strip() + '\n')
			vec = self.actions[i]
			if vec[0] == 0:
				f.write('	' + 'RANDOM' +
					', ' + str(vec[1]) + ',\n')
			else:
				f.write('	' + self.verbsid[vec[0]] +
					', ' + self.nounsid[vec[1]] + ',\n')
			f.write('	' + self.mapnr(vec[3], vec[2]) +
				', ' + self.mapcond(vec[3]) + ',\n')
			f.write('	' + self.mapnr(vec[5], vec[4]) +
				', ' + self.mapcond(vec[5]) + ',\n')
			f.write('	' + self.mapnr(vec[7], vec[6]) +
				', ' + self.mapcond(vec[7]) + ',\n')
			f.write('	' + self.mapnr(vec[9], vec[8]) +
				', ' + self.mapcond(vec[9]) + ',\n')
			f.write('	' + self.mapnr(vec[11], vec[10]) +
				', ' + self.mapcond(vec[11]) + ',\n')
			f.write('	' + self.mapcode(vec[12]) + ',\n')
			f.write('	' + self.mapcode(vec[13]) + ',\n')
			f.write('	' + self.mapcode(vec[14]) + ',\n')
			f.write('	' + self.mapcode(vec[15]))
			if i < len(self.actions) - 1:
				f.write(',\n')
			else:
				f.write('\n')
		f.write('};\n')
		f.write('\n')
		f.write('#define nr_actions ' + str(len(self.actions)) + '\n')

	def printactions(self):
		for i in range(self.nractions):
			val = self.actions[i]
			print(val)

	def parsevocab(self):
		self.verbs = []
		self.nouns = []
		self.items = []
		self.verbsid = []
		self.nounsid = []
		self.itemsid = []
		self.verbsbaseid = []
		self.nounsbaseid = []
		lastverb = ''
		lastnoun = ''
		for i in range(self.nrvocab + 1):
			veno = self.data[self.index].strip().split(' ')
			self.index = self.index + 1
			val = veno[0]
			if val != '""':
				self.verbs.append(val)
				verbsid = ''
				if val == '""':
					verbsid = 'VERB_' + str(i)
					lastverb = verbsid
				else:
					if val[1] != '*' :
						if val[1] == '.':
							verbsid = 'VERB_' + str(i)
						else:
							verbsid = 'VERB_' + val[1:-1]
						lastverb = verbsid
					else:
						if val[2] == '.':
							verbsid = 'VERB_' + str(i)
						else:
							verbsid = 'VERB_' + val[2:-1]
				if verbsid in self.verbsid :
					verbsid = 'VERB_' + str(i)
				self.verbsid.append(verbsid)
				self.verbsbaseid.append(lastverb)
			val = veno[1]
			self.nouns.append(val)
			nounsid = ''
			if val == '""':
				nounsid = 'NOUN_' + str(i)
				lastnoun = nounsid
			else:
				if (val.find('.') != -1) or (val.find('-') != -1) or (val == '""') or (val.find('$') != -1):
					nounsid = 'NOUN_' + str(i)
				else:
					if val[1] != '*' :
						nounsid = 'NOUN_' + val[1:-1]
						lastnoun = nounsid
					else:
						nounsid = 'NOUN_' + val[2:-1]
			if nounsid in self.nounsid :
				nounsid = 'NOUN_' + str(i)
			self.nounsid.append(nounsid)
			self.nounsbaseid.append(lastnoun)
		self.sverbs = []
		for i in range(len(self.verbs)) :
			if i > 0 and self.verbs[i] != '""' and self.verbs[i] != '"."' and self.verbs[i] != '"*."' :
				if self.verbs[i][1] != '*':
					self.sverbs.append('VERB_' + self.verbs[i][1:-1])
				else:
					self.sverbs.append('VERB_' + self.verbs[i][2:-1])
		self.sverbs.sort()
		self.snouns = []
		for i in range(len(self.nouns)) :
			if (self.nouns[i].find('.') == -1) and (self.nouns[i] != '""'):
				if self.nouns[i][1] != '*':
					key = self.nouns[i][1:] + '        '
				else:	
					key = self.nouns[i][2:] + '        '
				key = key[:8] + self.nounsid[i]
				self.snouns.append(key)
		self.snouns.sort()

	def writevocabheader(self, f):
		f.write('\n')
		for i in range(len(self.verbs)) :
			f.write('#define ' + self.verbsid[i] + ' ' + str(i) + '\n')
		f.write('\n')
		for i in range(len(self.nouns)) :
			f.write('#define ' + self.nounsid[i] + ' ' + str(i) + '\n')
		f.write('\n')
		f.write('#define NR_VERBS_SORTED ' + str(len(self.sverbs)) + '\n')
		f.write('#define NR_NOUNS_SORTED ' + str(len(self.snouns)) + '\n')

	def writevocabcode(self, f):
		f.write('\n')
		f.write('ramchip unsigned char sorted_verb_index;\n')
		f.write('ramchip unsigned char sorted_noun_index;\n')
		f.write('ramchip signed char verb_index;\n')
		f.write('ramchip signed char noun_index;\n')
		f.write('\n')
		f.write('char * const Verbs[] = {\n')
		for i in range(len(self.verbs)) :
			f.write('	' + self.verbs[i].lower())
			if i < len(self.verbs) - 1:
				f.write(',\n')
			else:
				f.write('\n')
		f.write('};\n')
		f.write('\n')
		f.write('unsigned char const VerbsBase[] = {\n')
		for i in range(len(self.verbs)) :
			f.write('	' + self.verbsbaseid[i])
			if i < len(self.verbs) - 1:
				f.write(',\n')
			else:
				f.write('\n')
		f.write('};\n')
		f.write('\n')
		f.write('#define nr_verbs ' + str(len(self.verbs)) + '\n')
		f.write('\n')
		f.write('unsigned char const VerbsSorted[] = {\n')
		for i in range(len(self.sverbs)) :
			f.write('	' + self.sverbs[i])
			if i < len(self.sverbs) - 1:
				f.write(',\n')
			else:
				f.write('\n')
		f.write('};\n')
		f.write('\n')
		f.write('char * const Nouns[] = {\n')
		for i in range(len(self.nouns)) :
			f.write('	' + self.nouns[i].lower())
			if i < len(self.nouns) - 1:
				f.write(',\n')
			else:
				f.write('\n')
		f.write('};\n')
		f.write('\n')
		f.write('unsigned char const NounsBase[] = {\n')
		for i in range(len(self.nouns)) :
			f.write('	' + self.nounsbaseid[i])
			if i < len(self.nouns) - 1:
				f.write(',\n')
			else:
				f.write('\n')
		f.write('};\n')
		f.write('\n')
		f.write('#define nr_nouns ' + str(len(self.nouns)) + '\n')
		f.write('\n')
		f.write('unsigned char const NounsSorted[] = {\n')
		for i in range(len(self.snouns)) :
			f.write('	' + self.snouns[i][8:])
			if i < len(self.snouns) - 1:
				f.write(',\n')
			else:
				f.write('\n')
		f.write('};\n')
		f.write('\n')
		f.write('unsigned char const NounsDirections[] = {\n')
		for i in range(6) :
			f.write('	' + self.nounsbaseid[i])
			if i < 6 - 1:
				f.write(',\n')
			else:
				f.write('\n')
		f.write('};\n')

	def printvocab(self):
		print("*** VOCABULARY ***")
		print("Verbs:")
		for i in range(len(self.verbs)):
		    print(self.verbs[i], self.verbsid[i], self.verbsbaseid[i])
		print("\nNouns:")
		for i in range(len(self.nouns)):
		    print(self.nouns[i], self.nounsid[i], self.nounsbaseid[i])

	def parseroomsline(self):
		words = self.data[self.index].split(' ')
		actionvec = []
		for i in range(6):
			actionvec.append(int(words[i]))
		return actionvec

	def parserooms(self):
		self.exits = []
		self.rooms = []
		left = self.nrrooms + 1
		while left > 0:
			thisexits = self.parseroomsline()
			self.exits.append(thisexits)
			w = self.data[self.index]
			room = w[w.find('"'):].strip()
			self.index = self.index + 1
			while room[-1:] != '"':
				room = room + ' ' + self.data[self.index].strip()
				self.index = self.index + 1
			self.rooms.append(room)
			left = left - 1

	def createroomnames(self):
		self.roomname = []
		for i in range(self.nrrooms+1) :
			self.roomname.append('ROOM_' + str(i))

	def writeroomheader(self, f):
		self.createroomnames()
		f.write('\n')
		for i in range(self.nrrooms+1) :
			f.write('#define ' + self.roomname[i] + ' ' + str(i) + '\n')
		f.write('\n')
		f.write('#define START_ROOM ROOM_' + str(self.startroom) + '\n')
		f.write('#define TREASURE_ROOM ROOM_' + str(self.treasureroom) + '\n')

	def writeroomcode(self, f):
		f.write('\n')
		f.write('// North, South, East, West, Up, Down\n')
		f.write('unsigned char const RoomExits[' + str(6 * (self.nrrooms + 1)) + '] = {\n')
		for i in range(self.nrrooms+1) :
			f.write('	/* ' + self.roomname[i] + ' */\n')
			for j in range(6):
				if self.exits[i][j] == 0:
					f.write('	0')
				else:
					f.write('	' + self.roomname[self.exits[i][j]])
				if (j != 5) or (i != self.nrrooms):
					f.write(',\n')
				else:
					f.write('\n')
		f.write('};\n')
		f.write('\n')
		f.write('char * const Rooms[] = {\n')
		for i in range(self.nrrooms+1) :
			f.write('	/* ' + self.roomname[i] + ' */ ')
			f.write(self.rooms[i])
			if i != self.nrrooms:
				f.write(',\n')
			else:
				f.write('\n')
		f.write('};\n')
		f.write('\n')
		f.write('#define nr_rooms ' + str(self.nrrooms + 1) + '\n')
		f.write('\n')
		f.write('/*\n')
		f.write('** TopGraphics and BotGraphics are ASCII art that\n')
		f.write('** you can define to be drawn at the top part and\n')
		f.write('** bottom part of the screen\n')
		f.write('**\n')
		f.write('** The top part is 9 rows of 40 characters (do not use newlines)\n')
		f.write('** The bottom part is 3 rows of 40 characters\n')
		f.write('**\n')
		f.write('** The code does not include sound effects\n')
		f.write('** or background music.\n')
		f.write('*/\n')
		f.write('\n')
		f.write('const char * TopGraphics[] = {\n')
		for i in range(self.nrrooms+1) :
			graphics = 'londontop'
			if i == 0:
				graphics = 'nowheretop'
			if i == 1:
				graphics = 'londontop'
			if i == 2:
				graphics = 'alcovetop'
			if i == 3:
				graphics = 'passagewaytop'
			if i == 4:
				graphics = 'attictop'
			if i == 5:
				graphics = 'windowtop'
			if i == 6:
				graphics = 'islandtop'
			if i == 7:
				graphics = 'mazetop'
			if i == 8:
				graphics = 'meadowtop'
			if i == 9:
				graphics = 'shacktop'
			if i == 10:
				graphics = 'oceantop'
			if i == 11:
				graphics = 'pittop'
			if i == 12:
				graphics = 'mazetop'
			if i == 13:
				graphics = 'mazetop'
			if i == 14:
				graphics = 'hilltop'
			if i == 15:
				graphics = 'shedtop'
			if i == 16:
				graphics = 'hallwaytop'
			if i == 17:
				graphics = 'caverntop'
			if i == 18:
				graphics = 'toptop'
			if i == 19:
				graphics = 'mazetop'
			if i == 20:
				graphics = 'shiptop'
			if i == 21:
				graphics = 'beachtop'
			if i == 22:
				graphics = 'graveyardtop'
			if i == 23:
				graphics = 'fieldtop'
			if i == 24:
				graphics = 'lagoontop'
			if i == 25:
				graphics = 'monasterytop'
			if i == 26:
				graphics = 'helltop'
			f.write('	/* ' + self.roomname[i] + ' */ ' + graphics)
			if i < self.nrrooms:
				f.write(',\n')
			else:
				f.write('\n')
		f.write('};\n')
		f.write('\n')
		f.write('const char * BotGraphics[] = {\n')
		for i in range(self.nrrooms+1) :
			graphics = 'londonbot'
			if i == 0:
				graphics = 'nowherebot'
			if i == 1:
				graphics = 'londonbot'
			if i == 2:
				graphics = 'alcovebot'
			if i == 3:
				graphics = 'passagewaybot'
			if i == 4:
				graphics = 'atticbot'
			if i == 5:
				graphics = 'windowbot'
			if i == 6:
				graphics = 'islandbot'
			if i == 7:
				graphics = 'mazebot'
			if i == 8:
				graphics = 'meadowbot'
			if i == 9:
				graphics = 'shackbot'
			if i == 10:
				graphics = 'oceanbot'
			if i == 11:
				graphics = 'pitbot'
			if i == 12:
				graphics = 'mazebot'
			if i == 13:
				graphics = 'mazebot'
			if i == 14:
				graphics = 'hillbot'
			if i == 15:
				graphics = 'shedbot'
			if i == 16:
				graphics = 'hallwaybot'
			if i == 17:
				graphics = 'cavernbot'
			if i == 18:
				graphics = 'topbot'
			if i == 19:
				graphics = 'mazebot'
			if i == 20:
				graphics = 'shipbot'
			if i == 21:
				graphics = 'beachbot'
			if i == 22:
				graphics = 'graveyardbot'
			if i == 23:
				graphics = 'fieldbot'
			if i == 24:
				graphics = 'lagoonbot'
			if i == 25:
				graphics = 'monasterybot'
			if i == 26:
				graphics = 'hellbot'
			f.write('	/* ' + self.roomname[i] + ' */ ' + graphics)
			if i < self.nrrooms:
				f.write(',\n')
			else:
				f.write('\n')
		f.write('};\n')

	def printrooms(self):
		print("*** ROOMS ***")
		for i in range(self.nrrooms+1) :
			print(self.exits[i], self.rooms[i])

	def parsemessages(self):
		self.messages = []
		left = self.nrmessages + 1
		while left > 0:
			msg = self.data[self.index].replace("\n","\\n")
			self.index = self.index + 1
			while msg.find('"', 1) == -1:
				msg = msg + self.data[self.index].replace("\n","\\n")
				self.index = self.index + 1
			msg = msg[:-2]
			self.messages.append(msg)
			left = left - 1

	def writemessagesheader(self, f):
		f.write('\n')
		for i in range(len(self.messages)) :
			if i < 52:
				f.write('#define MSG' + str(i) + ' ' + str(i) + '\n')
			else:
				f.write('#define MSG' + str(i) + ' ' + str(i + 50) + '\n')

	def writemessagescode(self, f):
		f.write('\n')
		f.write('char * const Messages[] = {\n')
		for i in range(self.nrmessages + 1) :
			f.write('	/* MSG' + str(i) + '*/ ' + self.messages[i])
			if i < self.nrmessages:
				f.write(',\n')
			else:
				f.write('\n')
		f.write('};\n')
		f.write('\n')
		f.write('#define nr_messages ' + str(self.nrmessages + 1) + '\n')
		f.write('\n')
		f.write('/*\n')
		f.write('** It is possible to add sound effects for actions\n')
		f.write('** You can add the name of the sfx routine you want\n')
		f.write('** to run by inserting the hame of the routine to\n')
		f.write('** this table\n')
		f.write('** Look at the sfx library for effects available\n')
		f.write('*/\n')
		f.write('\n')
		f.write('char * const SoundEffects[] = {\n')
		for i in range(self.nrmessages + 1) :
			if i == 5:
				f.write('	/* MSG' + str(i) + '*/ (char *)sfx_advbite')
			elif i == 4:
				f.write('	/* MSG' + str(i) + '*/ (char *)sfx_eek')
			elif i == 9:
				f.write('	/* MSG' + str(i) + '*/ (char *)sfx_eek2')
			elif i == 10:
				f.write('	/* MSG' + str(i) + '*/ (char *)sfx_hahaha')
			elif i == 19:
				f.write('	/* MSG' + str(i) + '*/ (char *)sfx_eek')
			elif i == 23:
				f.write('	/* MSG' + str(i) + '*/ (char *)sfx_teleported')
			elif i == 30:
				f.write('	/* MSG' + str(i) + '*/ (char *)sfx_eek2')
			elif i == 53:
				f.write('	/* MSG' + str(i) + '*/ (char *)sfx_plonk')
			else:
				f.write('	/* MSG' + str(i) + '*/ 0')
			t = ' //' + self.messages[i]
			if i < self.nrmessages:
				f.write(',' + t + '\n')
			else:
				f.write(t + '\n')
		f.write('};\n')

	def printmessages(self):
		print("*** MESSAGES ***")
		for i in range(self.nrmessages + 1) :
			print(self.messages[i])

	def mapautoget(self, autoget):
		if autoget.find('$') != -1:
			for i in range(len(self.nouns)):
				if self.nouns[i].find(autoget[autoget.find('$'):]) != -1:
					return self.nounsid[i]
		return autoget

	def parseitems(self):
		self.items = []
		self.itemsid = []
		self.locations = []
		self.autoget = []
		itemnr = 0
		while self.data[self.index].strip()[-1] != '"':
			obj = self.data[self.index].strip()
			self.index = self.index + 1
			while obj.find('"', 1) == -1:
				obj = obj + ' ' + self.data[self.index].strip()
				self.index = self.index + 1
			quote = obj.find('"', 1)
			item = obj[:quote + 1]
			if len(item) > 2:
				place = int(obj[quote + 2:])
			else:
				place = 0
			quote = obj.find('/', 1)
			if quote > 0:
				autoget = 'ITEM_' + item[quote + 1:]
				item = item[:quote] + '"'
				quote = autoget.find('/')
				if quote > 0:
					autoget = autoget[:quote]
			else:
				autoget = '0'
			self.items.append(item)
			self.itemsid.append('ITEM_' + str(itemnr))
			self.locations.append(place)
			self.autoget.append(self.mapautoget(autoget))
			itemnr = itemnr + 1

	def writeitemsheader(self, f):
		f.write('\n')
		for i in range(len(self.itemsid)) :
			f.write('#define ' + self.itemsid[i] + ' ' + str(i) + '\n')
		f.write('\n')
		f.write('#define TREASURES ' + str(self.nrtreasures) + '\n')

	def writeitemscode(self, f):
		f.write('\n')
		f.write('char * const ItemsText[] = {\n')
		for i in range(len(self.items)) :
			f.write('	/* ' + self.itemsid[i] + ' */ ' + self.items[i])
			if i < len(self.items) - 1:
				f.write(',\n')
			else:
				f.write('\n')
		f.write('};\n')
		f.write('\n')
		f.write('#define nr_items ' + str(len(self.items)) + '\n')
		f.write('\n')
		f.write('ramchip unsigned char ItemsLocation[' + str(len(self.items)) + '];\n')
		f.write('\n')
		f.write('unsigned char const ItemsInitialLoc[] = {\n')
		for i in range(len(self.items)) :
			if self.locations[i] == -1:
				f.write('	CARRIED')
			else:
				f.write('	' + self.roomname[self.locations[i]])
			if i < len(self.items) - 1:
				f.write(',\n')
			else:
				f.write('\n')
		f.write('};\n')
		f.write('\n')
		f.write('unsigned char const ItemsAutoget[] = {\n')
		for i in range(len(self.items)) :
			if len(self.autoget[i]) > 1:
				f.write('	' + self.itemsid[i])
				t = '	// ' + self.autoget[i]
			else:
				f.write('	0')
				t = ''
			if i < len(self.items) - 1:
				f.write(',' + t + '\n')
			else:
				f.write(t + '\n')
		f.write('};\n')
		f.write('\n')
		f.write('unsigned char const BaseNounForItems[] = {\n')
		for i in range(len(self.items)) :
			if len(self.autoget[i]) > 1:
				f.write('	NOUN_' + self.autoget[i][5:])
				t = '	// ' + self.itemsid[i]
			else:
				f.write('	0')
				t = ''
			if i < len(self.items) - 1:
				f.write(',' + t + '\n')
			else:
				f.write(t + '\n')
		f.write('};\n')

	def printitems(self):
		print("*** OBJECTS ***")
		for i in range(self.nritems) :
			print(self.items[i], self.locations[i], self.autoget[i])

	def parsetitles(self):
		self.titles = []
		left = self.nractions + 1
		while left > 0:
			title = self.data[self.index]
			self.index = self.index + 1
			self.titles.append(title)
			left = left - 1

	def printtitles(self):
		print("*** TITLES ***")
		for i in range(self.nractions+1) :
			print(self.titles[i], end="")

	def writeheader(self, f):
		f.write('/*\n')
		f.write('** ' + self.name + '\n')
		f.write('** for atari 7800\n')
		f.write('**\n')
		f.write('** 2024 Karri Kaksonen\n')
		f.write('**\n')
		f.write('** The code does not include sound effects\n')
		f.write('** or background music.\n')
		f.write('**\n')
		f.write('** You can compile this file manually by the command\n')
		f.write('** cl65 -t atari7800 -C ' + self.name + '.cfg -m ' + self.name + '.map atari7800-mono.o ' + self.name + '.c exehdr.s -o ' + self.name + '.a78')
		f.write('**\n')
		f.write('** sign7800 -w ' + self.name + '.a78\n')
		f.write('*/\n')
		f.write('\n')

	def writeincludes(self, f):
		if self.compiler == 'cc65':
			f.write('#include <atari7800.h>\n')
			f.write('#include <time.h>\n')
			f.write('#include <conio.h>\n')
		else:
			f.write('#include <kconio.h>\n')
		f.write('#include <joystick.h>\n')
		f.write('#include <string.h>\n')
		f.write('#include <stdlib.h>\n')
		f.write('#include "nowhere.c"\n')
		f.write('#include "london.c"\n')
		f.write('#include "alcove.c"\n')
		f.write('#include "passageway.c"\n')
		f.write('#include "attic.c"\n')
		f.write('#include "window.c"\n')
		f.write('#include "island.c"\n')
		f.write('#include "maze.c"\n')
		f.write('#include "meadow.c"\n')
		f.write('#include "shack.c"\n')
		f.write('#include "ocean.c"\n')
		f.write('#include "pit.c"\n')
		f.write('#include "hill.c"\n')
		f.write('#include "shed.c"\n')
		f.write('#include "hallway.c"\n')
		f.write('#include "cavern.c"\n')
		f.write('#include "top.c"\n')
		f.write('#include "ship.c"\n')
		f.write('#include "beach.c"\n')
		f.write('#include "graveyard.c"\n')
		f.write('#include "field.c"\n')
		f.write('#include "lagoon.c"\n')
		f.write('#include "monastery.c"\n')
		f.write('#include "hell.c"\n')
		f.write('#include "tide.c"\n')
		f.write('\n')
		if self.compiler == 'cc65':
			code = '''
#define CONST const
#define SWCHB RIOT.swchb
#define ramchip
'''
		else:
			code = '''
#define CONST
'''
		f.write(code)
		f.write('#define USESFX\n')
		if self.compiler == 'cc65':
			code = '''
extern unsigned char sfx_advpickup[];
extern unsigned char sfx_advdrop[];
extern unsigned char sfx_advbite[];
extern unsigned char sfx_eek[];
extern unsigned char sfx_eek2[];
extern unsigned char sfx_transporter[];
extern unsigned char sfx_wolfwhistle[];
extern unsigned char sfx_teleported[];
extern unsigned char sfx_electroswitch[];
extern unsigned char sfx_hophipquick[];
extern unsigned char sfx_whoa[];
extern unsigned char sfx_hahaha[];
extern unsigned char sfx_plonk[];
extern unsigned char sfx_simplebuzz[];
extern unsigned char sfx_falling[];
extern unsigned char sfx_yahoo[];
extern unsigned char tt_wait;
#pragma zpsym("tt_wait");
extern void __fastcall__ playsfx0(unsigned char *);

unsigned char tunenr = 0;

extern void __fastcall__ sub_start_song(unsigned char tune);
'''
			f.write(code)
		f.write('	#define TOP_HEIGHT 10\n')
		f.write('	#define STORY_LINES 15\n')
		f.write('	#define STORY_LINE_LEN 40\n')
		if self.compiler == 'cc65':
			f.write('	#define TOTAL_LINES 28\n')
		else:	
			f.write('	#define TOTAL_LINES 25\n')
		f.write('#ifndef LINE_LEN\n')
		f.write('	#define LINE_LEN 40\n')
		f.write('#endif\n')
		f.write('#define SCROLL_DELAY 0\n')
		if self.compiler == 'cc65':
			f.write('extern char screen[];\n')
		f.write('\n')
		f.write('#define LIGHT_SOURCE    9               /* Always 9 how odd */\n')
		f.write('#define CARRIED         255             /* Carried */\n')
		f.write('#define NOTFOUND        254             /* Not found in room */\n')
		f.write('#define DESTROYED       0               /* Destroyed */\n')
		f.write('#define DARKBIT         0x4000\n')
		f.write('#define LIGHTOUTBIT     0x8000          /* Light gone out */\n')
		f.write('#define WordLen 3\n')
		f.write('#define MaxCarry 7\n')
		
	def writecodeincludes(self, f):
		pass
		
	def writeinterpreter(self, f):
		if self.compiler == 'cc65':
			f.write('#define COMPILER_CC65\n')
			code = '''
void delete_line(unsigned char y)
{
	gotoxy(0, y);
	memset(screen + y * LINE_LEN, 0, LINE_LEN);
}

void delay(unsigned char len)
{
	clock_t w;

	w = clock() + len;
	while (clock() < w) ;
}

unsigned char curs;
'''
		else:
			f.write('#define COMPILER_CC7800\n')
			code = '''
#define cputc(c) _ms_tmp = (c); _conio_putch()

unsigned char realtime;

void interrupt dli()
{
	realtime++;
}

void delay(unsigned char len)
{
	realtime = 0;
	while (realtime < len) ;
}

ramchip unsigned char seed;

ramchip unsigned char luck;

unsigned char rand()
{
	seed += 0x84;
	seed >>= 1;
	seed = seed ^ 0x55;
	return seed;
}

void delete_line(unsigned char y)
{
	gotoxy(0, y);
	delline();
}
'''
		f.write(code)
		code = '''
unsigned char readselect() { 
	if (SWCHB & 0x02) {
		return 0;
	}
	return 1;
}

unsigned char value(const char *buf)
{
	return *buf;
}

unsigned char star(const char *buf)
{
	return value(buf) == '*';
}

unsigned char tab(const char *buf)
{
	return value(buf) == 9;
}

void OutReset()
{
	delete_line(TOP_HEIGHT + STORY_LINES - 1);
}

void cleantextarea()
{
        unsigned char i;

        for (i = 0; i < STORY_LINES - 1; i++) {
                delete_line(TOP_HEIGHT + i);
        }
        gotoxy(0, TOP_HEIGHT);
        textcolor(1);
}

void scroll()
{
        if (wherey() == 24) {
                cleantextarea();
        } else {
                gotoxy(0, wherey() + 1);
                delete_line(wherey());
        }
	delay(10);
}

void printword(char * buf, unsigned char len) {
#ifdef COMPILER_CC65
	unsigned char i;

	for (i = 0; i < len; i++) {
		unsigned char ch;
		ch = buf[i];
		if (wherey() > TOP_HEIGHT + STORY_LINES - 1) {
			scroll();
		}
		cputc(ch);
	}
#endif
#ifdef COMPILER_CC7800
        Y = len;
        _ms_tmp = len;
        _ms_tmpptr2 = buf;
        _conio_cputs2();
#endif
}

unsigned char nextwordlen(char *buf) {
        unsigned char wp;

        wp = 0;
        while (*buf) {
                if (value(buf) == ' ') return wp;
                if (value(buf) == '\\n') return wp;
                wp++;
                buf++;
        }
        return wp;
}

void OutBuf(char *buffer)
{
	unsigned char wp;
	unsigned char color;

	while (*buffer) {
		if (value(buffer) == '\\n') {
			scroll();
			buffer++;
		} else {
			if (tab(buffer)) {
                                buffer++;
				color = *buffer - '0';
                                textcolor(color);
                                buffer++;
			}
			wp = nextwordlen(buffer);
			if ((wherex() + wp) > STORY_LINE_LEN) {
				scroll();
			} else {
				printword(buffer, wp);
				buffer += wp;
				if (value(buffer) == '\\n') {
					scroll();
				} else {
					if (wherex() > 0) {
						cputc(' ');
					}
				}
				if (value(buffer) == 0) {
					return;
				}
				buffer++;
			}
		}
	}
}

ramchip unsigned char Redraw;
ramchip unsigned char Relook;

ramchip unsigned char MyLoc;
ramchip unsigned int BitFlags;

unsigned char checkBitFlags(unsigned char n) {
	unsigned char val;
	unsigned char bf;
	unsigned char i;

	if (n > 7) {
		bf = BitFlags >> 8;
		n = n - 8;
	} else {
		bf = BitFlags;
	}
	val = 1;
	for (i = 0; i < n; i++) {
		val += val;
	}
	return bf & val;
}

void clearBitFlags(unsigned char n) {
	unsigned char lo;
	unsigned char hi;
	unsigned char val;
	unsigned char i;

	lo = BitFlags;
	hi = BitFlags >> 8;
	if (n > 7) {
		n = n - 8;
		val = 1;
		for (i = 0; i < n; i++) {
			val += val;
		}
		hi &= ~val;
	} else {
		val = 1;
		for (i = 0; i < n; i++) {
			val += val;
		}
		lo &= ~val;
	}
	BitFlags = hi << 8;
	BitFlags |= lo;
}

void setBitFlags(unsigned char n) {
	unsigned char lo;
	unsigned char hi;
	unsigned char val;
	unsigned char i;

	lo = BitFlags;
	hi = BitFlags >> 8;
	if (n > 7) {
		n = n - 8;
		val = 1;
		for (i = 0; i < n; i++) {
			val += val;
		}
		hi |= val;
	} else {
		val = 1;
		for (i = 0; i < n; i++) {
			val += val;
		}
		lo |= val;
	}
	BitFlags = hi << 8;
	BitFlags |= lo;
}

unsigned char getExit(unsigned char ex) {
	unsigned char i;

	i = MyLoc;
	i += MyLoc;
	i += MyLoc;
	i += MyLoc;
	i += MyLoc;
	i += MyLoc;
	i += ex;
	i = RoomExits[i];
	return i;
}

unsigned char hasLight() {
	unsigned char loc;

	// Is it day?
        if (checkBitFlags(14) == 0) return 1;
	// It is night
	loc = ItemsLocation[LIGHT_SOURCE];
        if (loc != CARRIED) {
        	if (loc != MyLoc) {
			// There is no lamp nearby
			return 0;
		}
	}
	// Is the lamp still burning?
        if (checkBitFlags(15) == 0) return 1;
	// No light
        return 0;
}

void printbg(char *buf)
{
        unsigned char ch;

        do {
                ch = *buf;
                buf++;
                if (ch == 0) {
                        cputc(' ');
                } else {
                        if (ch != 10) {
                                cputc(ch);
                        }
                }
        } while (ch != 10);
}

unsigned char getItemsLocation(unsigned char item)
{
	return ItemsLocation[item];
}

unsigned char isAvailable(unsigned char i)
{
	if (getItemsLocation(i) == CARRIED)
		return 1;
	return getItemsLocation(i) == MyLoc;
}

void wrefresh()
{
	gotoxy(0, TOP_HEIGHT + STORY_LINES - 1);
	Redraw = 0;
}

static char *last_bg;

void setpalettes(
	unsigned char p0,
	unsigned char p1,
	unsigned char p2,
	unsigned char p3,
	unsigned char p4,
	unsigned char p5,
	unsigned char p6,
	unsigned char p7)
{
	MARIA.p0c1 = p0;
	MARIA.p0c2 = p0;
	MARIA.p0c3 = p0;
	MARIA.p1c2 = p1;
	MARIA.p2c2 = p2;
	MARIA.p3c2 = p3;
	MARIA.p4c2 = p4;
	MARIA.p5c2 = p5;
	MARIA.p6c2 = p6;
	MARIA.p7c2 = p7;
}

unsigned char flicker = 0;

void switchpalettes()
{
	switch (MyLoc) {
	case 1: // L london
		// White background, red text
		MARIA.bkgrnd = 0x0c;
		setpalettes(0x33,0x15,0x33,0x33,0x33,0x33,0x33,0x33);
		break;
	case 2: // L alcove
		// White background, red text
		MARIA.bkgrnd = 0x0c;
		setpalettes(0x33,0x15,0x20,0x22,0x24,0x24,0x26,0x1f);
		break;
	case 3: // L passageway
		// White background, red text
		MARIA.bkgrnd = 0x0c;
		setpalettes(0x33,0x15,0x20,0x22,0x24,0x24,0x1f,0x26);
		break;
	case 4: // L attic
		// White background, red text
		MARIA.bkgrnd = 0x0c;
		setpalettes(0x33,0x15,0x16,0x16,0x16,0x16,0x16,0x16);
		break;
	case 5: // L window
		// White background, red text
		MARIA.bkgrnd = 0x0c;
		setpalettes(0x33,0x15,0x33,0x33,0x33,0x33,0x33,0x33);
		break;
	case 6: // island
	case 10: // ocean
	case 18: // top
		// Blue background, yellow text
		MARIA.bkgrnd = 0x74;
		if (flicker) {
			setpalettes(0xec,0x00,0xc8,0xc8,0xc8,0xc8,0xc8,0x2a);
		} else {
			setpalettes(0xec,0x00,0xc8,0xc8,0xc8,0xc8,0xc8,0x1b);
		}
		break;
	case 7: // maze
	case 12: // maze
	case 13: // maze
	case 15: // shed
	case 17: // cavern
	case 19: // maze
		// Black background, green text
		MARIA.bkgrnd = 0x00;
		setpalettes(0xc8,0x15,0x88,0x88,0x88,0x88,0x88,0x88);
		break;
	case 8: // meadow
		// Blue background, yellow text
		MARIA.bkgrnd = 0x74;
		setpalettes(0xec,0x15,0xc8,0xc8,0x88,0xc8,0x1b,0x1b);
		break;
	case 9: // shack
		// Black background, green text
		MARIA.bkgrnd = 0x00;
		setpalettes(0xc8,0x15,0x16,0x19,0x19,0x19,0x19,0x19);
		break;
	case 11: // pit
		// Black background, green text
		MARIA.bkgrnd = 0x00;
		setpalettes(0xc8,0x15,0x16,0x16,0x16,0x16,0x16,0x16);
		break;
	case 14: // hill
		// Blue background, yellow text
		MARIA.bkgrnd = 0x74;
		if (flicker) {
			setpalettes(0xec,0x15,0xc8,0xc8,0x16,0x16,0xb8,0x2a);
		} else {
			setpalettes(0xec,0x15,0xc8,0xc8,0x16,0x16,0xb8,0x1b);
		}
		break;
	case 16: // hallway
		// Black background, green text
		MARIA.bkgrnd = 0x00;
		setpalettes(0xc8,0x15,0x20,0x22,0x24,0x24,0x1f,0x26);
		break;
	case 20: // ship
		// Purple background, yellow text
		MARIA.bkgrnd = 0x50;
		setpalettes(0xec,0x15,0x00,0x00,0x00,0x00,0x00,0x27);
		break;
	case 21: // beach
		// Purple background, yellow text
		MARIA.bkgrnd = 0x50;
		setpalettes(0xec,0x15,0x00,0x00,0x00,0x00,0x00,0x27);
		break;
	case 22: // graveyard
		// Purple background, yellow text
		MARIA.bkgrnd = 0x50;
		setpalettes(0xec,0x15,0x00,0x00,0x00,0x00,0x00,0x00);
		break;
	case 23: // field
		// Purple background, yellow text
		MARIA.bkgrnd = 0x50;
		setpalettes(0xec,0x15,0x00,0x00,0x00,0x0f,0x00,0x19);
		break;
	case 24: // lagoon
		// Blue background, yellow text
		MARIA.bkgrnd = 0x74;
		if (flicker) {
			setpalettes(0xec,0x00,0xc8,0xc8,0xc8,0xc8,0xc8,0x2a);
		} else {
			setpalettes(0xec,0x00,0xc8,0xc8,0xc8,0xc8,0xc8,0x1b);
		}
		break;
	case 25: // monastery
		// Purple background, yellow text
		MARIA.bkgrnd = 0x50;
		if (flicker) {
			setpalettes(0xec,0x15,0x0f,0x0f,0x0f,0x0f,0x0f,0x2a);
		} else {
			setpalettes(0xec,0x15,0x0f,0x0f,0x0f,0x0f,0x0f,0x1b);
		}
		break;
	case 26: // hell
		// Black background, green text
		MARIA.bkgrnd = 0x00;
		if (flicker) {
			setpalettes(0xc8,0x15,0x33,0x33,0x33,0x33,0x33,0x33);
		} else {
			setpalettes(0xc8,0x15,0x2a,0x2a,0x2a,0x2a,0x2a,0x2a);
		}
		break;
	default:
		setpalettes(0xc8,0x15,0xc8,0xc8,0xc8,0xc8,0xc8,0xc8);
		break;
	}
}

void printbackground()
{
        char *e;

        if (hasLight()) {
		switchpalettes();
                e = (char *)TopGraphics[MyLoc];
		if (e != last_bg) {
                	gotoxy(0, 0);
                	printbg(e);
			last_bg = e;
		}
        } else {
                clrscr();
		last_bg = 0;
        }
	wrefresh();
}

void printfooter()
{
        char *e;

        if (hasLight()) {
                gotoxy(0, TOP_HEIGHT + STORY_LINES);
                e = (char *)BotGraphics[MyLoc];
                printbg(e);
	}
}

void ClearScreen(void)
{
	unsigned char i;

	for (i = TOP_HEIGHT; i < TOP_HEIGHT + STORY_LINES; i++) {
		delete_line(i);
	}
	gotoxy(0, TOP_HEIGHT);
}

unsigned char getItemsInitialLoc(unsigned char item)
{
	return ItemsInitialLoc[item];
}

void LookItems(unsigned char loc)
{
	unsigned char ct;
	unsigned char f;
	char *s;

	ct = 0;
	f = 0;
	while (ct < nr_items) {
		if (getItemsLocation(ct) == loc) {
			if (f == 0) {
				OutBuf("I can also see:\\n");
				f++;
			}
			OutBuf("- ");
			s = ItemsText[ct];
			OutBuf(s);
			scroll();
		}
		ct++;
	}
}

char *w;

void stripstarw(char *buf)
{
        w = buf;
        if (star(w))
                w++;
}

char *const ExitNames[6] = {
	" North", " South", " East", " West", " Up", " Down"
};

void printexit(unsigned char index)
{
        unsigned char e;
        unsigned char f;
        char *r;

        e = RoomExits[index];
        if (e) {
                //f = RoomNouns[e];
                r = Nouns[f]; 
                stripstarw(r);
                textcolor(2);
                OutBuf(w);
        }
}

void Look()
{
	char *r;
	unsigned char ct, f, u;

	Relook = 0;
	Redraw = 0;
	if (!hasLight()) {
		printbackground();
		ClearScreen();
		textcolor(1);
		OutBuf("I can't see. It is too dark!\\n");
		curs = wherey();
		return;
	}
	printbackground();
	ClearScreen();
	textcolor(1);
	r = Rooms[MyLoc];
	if (star(r)) {
		r++;
	} else {
		OutBuf("I'm in a ");
	}
	OutBuf(r);
	scroll();
	f = 0;
	for (ct = 0; ct < 6; ct++) {
		f |= getExit(ct);
	}
	if (f) {
		OutBuf("Obvious exits:");
		for (ct = 0; ct < 6; ct++) {
			u = getExit(ct);
			if (u) {
				r = ExitNames[ct];
				OutBuf(r);
			}
		}
	}
	scroll();
	LookItems(MyLoc);
	curs = wherey();
}

ramchip signed int LightTime;
ramchip int CurrentCounter;

unsigned char RandomPercent(unsigned char n)
{
#ifdef COMPILER_CC65
        unsigned int rv = rand();
        rv %= 100;
        if (rv < n)
                return (1);
        return (0);
#endif
#ifdef COMPILER_CC7800
	if (n == 100) return 1;
        return realtime < n + n;
#endif
}

unsigned char CountCarried()
{
	unsigned char ct;
	unsigned char n;

	ct = 0;
	n = 0;
	while (ct < nr_items) {
		if (getItemsLocation(ct) == CARRIED)
			n++;
		ct++;
	}
	return (n);
}

ramchip unsigned char NounToken;

#define CONDITION_FAILED 0
#define SUCCESS 1
#define PROCESS_AGAIN 2
#define NOT_UNDERSTOOD 3
#define NOT_YET 4
#define QUIT_GAME 5

ramchip unsigned char param[5];

unsigned char CheckConditions(CONST char *action)
{
	unsigned char pptr;
	unsigned char cc, i;

	pptr = 0;
	cc = 0;
	while (cc < 5) {
		unsigned char cv, dv;
		dv = *action;
		action++;
		cv = *action;
		action++;
		switch (cv) {
		case PAR:
			param[pptr++] = dv;
			break;
		case HAS:
			if (getItemsLocation(dv) != CARRIED)
				return CONDITION_FAILED;
			break;
		case INW:
			if (getItemsLocation(dv) != MyLoc)
				return CONDITION_FAILED;
			break;
		case AVL:
			if (!isAvailable(dv))
				return CONDITION_FAILED;
			break;
		case IN:
			if (MyLoc != dv) {
				return CONDITION_FAILED;
			}
			break;
		case NINW:
			if (getItemsLocation(dv) == MyLoc)
				return CONDITION_FAILED;
			break;
		case NHAS:
			if (getItemsLocation(dv) == CARRIED)
				return CONDITION_FAILED;
			break;
		case NIN:
			if (MyLoc == dv)
				return CONDITION_FAILED;
			break;
		case BIT:
			if (checkBitFlags(dv) == 0)
				return CONDITION_FAILED;
			break;
		case NBIT:
			if (checkBitFlags(dv))
				return CONDITION_FAILED;
			break;
		case ANY:
			if (CountCarried() == 0)
				return CONDITION_FAILED;
			break;
		case NANY:
			if (CountCarried())
				return CONDITION_FAILED;
			break;
		case NAVL:
			if (isAvailable(dv))
				return CONDITION_FAILED;
			break;
		case NRM0:
			if (getItemsLocation(dv) == 0)
				return CONDITION_FAILED;
			break;
		case RM0:
			if (getItemsLocation(dv))
				return CONDITION_FAILED;
			break;
		case CTLE:
			if (CurrentCounter > dv)
				return CONDITION_FAILED;
			break;
		case CTGT:
			if (CurrentCounter <= dv)
				return CONDITION_FAILED;
			break;
		case ORIG:
			i = ItemsLocation[dv];
			if (i != getItemsInitialLoc(dv))
				return CONDITION_FAILED;
			break;
		case NORIG:
			i = ItemsLocation[dv];
			if (i == getItemsInitialLoc(dv))
				return CONDITION_FAILED;
			break;
		case CTEQ:
			if (CurrentCounter != dv)
				return CONDITION_FAILED;
			break;
		}
		cc++;
	}
	return SUCCESS;
}


void GetItem(unsigned char item)
{
	if (CountCarried() == MaxCarry) {
		OutBuf("I've too much to carry! ");
		return;
	}
	if (getItemsLocation(item) == MyLoc) {
		Redraw = 1;
#ifdef USESFX
		playsfx0(sfx_advpickup);
#endif
	}
	ItemsLocation[item] = CARRIED;
} 

void DropItem(unsigned char item)
{
	Redraw = 1;
	ItemsLocation[item] = MyLoc;
}

void DrawMessage(unsigned char msg)
{
	char *s;

	if (wherey() == TOP_HEIGHT + STORY_LINES - 1) {
		gotoxy(0, TOP_HEIGHT);
	}
	s = Messages[msg];
	OutBuf(s);
#ifdef USESFX
	if (SoundEffects[msg]) {
		playsfx0((unsigned char *)SoundEffects[msg]);
	}
#endif
	scroll();
}

void DrawNoun(unsigned char noun)
{
	char *s;

	s = Nouns[noun];
	if (star(s)) {
		s++;
	}
	OutBuf(s);
}

unsigned char const Room0VerbsSorted[] = {
	VERB_QUIT,
	VERB_SCORE,
};

// flat
unsigned char const Room1VerbsSorted[] = {
	VERB_CLIMB, // used
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_EXAMINE, // always
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_JUMP, // always
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_PULL, // used
	VERB_QUIT, // always
	VERB_REMOVE, // used
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_WAIT, // always
	VERB_WALK // always
};

// alcove
unsigned char const Room2VerbsSorted[] = {
	VERB_CLOSE, // used
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // used
	VERB_EXAMINE, // always
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_JUMP, // used
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_SHUT, // used
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_UNLOCK, // used
	VERB_WAIT, // always
	VERB_WALK, // always
};

// passageway
unsigned char const Room3VerbsSorted[] = {
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_JUMP,
	VERB_KILL,
	VERB_LEAVE,
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_RELEASE,
	VERB_REMOVE,
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_UNLOCK,
	VERB_WAIT, // always
	VERB_WALK, // always
};

// attic
unsigned char const Room4VerbsSorted[] = {
	VERB_ATTACK, // used
	VERB_CLIMB, // used
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_JUMP,
	VERB_KILL,
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_WAIT, // always
	VERB_WAKE, // used
	VERB_WALK, // always
};

// window
unsigned char const Room5VerbsSorted[] = {
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_FLY, // used
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_JUMP, // used
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_WAIT, // always
	VERB_WALK, // always
};

// beach
unsigned char const Room6VerbsSorted[] = {
	VERB_BUILD, // used
	VERB_DIG, // used
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_FIND, // used
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_JUMP, // used
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_MAKE, // used
	VERB_OPEN, // always
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_WAIT, // always
	VERB_WALK, // always
};

// maze
unsigned char const Room7VerbsSorted[] = {
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_WAIT, // always
	VERB_WALK, // always
};

// meadow 
unsigned char const Room8VerbsSorted[] = {
	VERB_DIG, // used
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_WAIT, // always
	VERB_WALK, // always
};

// shack
unsigned char const Room9VerbsSorted[] = {
	VERB_ATTACK, // used
	VERB_BREAK, // used
	VERB_BURN, // used
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_FEEL, // used
	VERB_FIND,
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_KILL,
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_SMASH, // used
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_UNLOCK, // always
	VERB_WAIT, // always
	VERB_WALK, // always
};

// ocean
unsigned char const Room10VerbsSorted[] = {
	VERB_CATCH, // used
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_WAIT, // always
	VERB_WALK, // always
};

// pit
unsigned char const Room11VerbsSorted[] = {
	VERB_ATTACK, // used
	VERB_BREAK, // used
	VERB_BURN, // used
	VERB_DIG, // used
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_FEEL, // used
	VERB_FIND,
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_SMASH, // used
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_UNLOCK, // always
	VERB_WAIT, // always
	VERB_WALK, // always
};

// maze
unsigned char const Room12VerbsSorted[] = {
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_WAIT, // always
	VERB_WALK, // always
};

// maze
unsigned char const Room13VerbsSorted[] = {
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_WAIT, // always
	VERB_WALK, // always
};

// hill
unsigned char const Room14VerbsSorted[] = {
	VERB_CLIMB, // used
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_FOLLOW,
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_WAIT, // always
	VERB_WALK, // always
};

// shed
unsigned char const Room15VerbsSorted[] = {
	VERB_ATTACK, // used
	VERB_BURN, // used
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_FIND,
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_JUMP,
	VERB_KILL,
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_UNLOCK,
	VERB_WAIT, // always
	VERB_WALK, // always
};

// hallway
unsigned char const Room16VerbsSorted[] = {
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_WAIT, // always
	VERB_WALK, // always
};

// cavern
unsigned char const Room17VerbsSorted[] = {
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_WAIT, // always
	VERB_WALK, // always
};

// top of hill
unsigned char const Room18VerbsSorted[] = {
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_WAIT, // always
	VERB_WALK, // always
};

// maze
unsigned char const Room19VerbsSorted[] = {
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_WAIT, // always
	VERB_WALK, // always
};

// ship
unsigned char const Room20VerbsSorted[] = {
	VERB_ATTACK, // used
	VERB_BURN,
	VERB_CAST, // used
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_FIND,
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_KILL,
	VERB_LEAVE,
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_SAIL, // used
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SET, // used
	VERB_SHAKE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_UNLOCK,
	VERB_WAIT, // always
	VERB_WALK, // always
	VERB_WEIGH // used
};

// beach
unsigned char const Room21VerbsSorted[] = {
	VERB_DIG, // used
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_FIND,
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_KILL,
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_PACE, // used
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_UNLOCK,
	VERB_WAIT, // always
	VERB_WALK, // always
};

// graveyard
unsigned char const Room22VerbsSorted[] = {
	VERB_ATTACK, // used
	VERB_BURN,
	VERB_DIG, // used
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_FIND,
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_KILL,
	VERB_LEAVE,
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_PACE, // used
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_WAIT, // always
	VERB_WAKE, // used
	VERB_WALK, // always
};

// field
unsigned char const Room23VerbsSorted[] = {
	VERB_BURN,
	VERB_DIG, // used
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_FIND,
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_PACE, // used
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_WAIT, // always
	VERB_WALK, // always
};

// lagoon
unsigned char const Room24VerbsSorted[] = {
	VERB_BURN,
	VERB_DIG, // used
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_WAIT, // always
	VERB_WALK, // always
};

// monastery
unsigned char const Room25VerbsSorted[] = {
	VERB_ATTACK, // used
	VERB_BURN,
	VERB_DIG, // used
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_KILL,
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_PACE, // used
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_RELEASE,
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_WAIT, // always
	VERB_WALK, // always
};

// hell
unsigned char const Room26VerbsSorted[] = {
	VERB_ATTACK, // used
	VERB_BURN,
	VERB_DIG, // used
	VERB_DRINK, // always
	VERB_DROP, // always
	VERB_EAT, // always
	VERB_EMPTY, // always
	VERB_ENTER, // always
	VERB_EXAMINE, // always
	VERB_GET, // always
	VERB_GIVE, // always
	VERB_GO, // always
	VERB_HELP, // always
	VERB_INVENTORY, // always
	VERB_JUMP,
	VERB_KILL,
	VERB_LEAVE,
	VERB_LIGHT, // always
	VERB_LISTEN, // always
	VERB_LOOK, // always
	VERB_OPEN, // always
	VERB_QUIT, // always
	VERB_READ, // always
	VERB_RELEASE,
	VERB_REMOVE,
	VERB_SAY, // always
	VERB_SCORE, // always
	VERB_SHAKE, // always
	VERB_TAKE, // always
	VERB_UNLIGHT, // always
	VERB_UNLOCK,
	VERB_WAIT, // always
	VERB_WALK, // always
};

const unsigned char verbsize[] = {
	sizeof(Room0VerbsSorted),
	sizeof(Room1VerbsSorted),
	sizeof(Room2VerbsSorted),
	sizeof(Room3VerbsSorted),
	sizeof(Room4VerbsSorted),
	sizeof(Room5VerbsSorted),
	sizeof(Room6VerbsSorted),
	sizeof(Room7VerbsSorted),
	sizeof(Room8VerbsSorted),
	sizeof(Room9VerbsSorted),
	sizeof(Room10VerbsSorted),
	sizeof(Room11VerbsSorted),
	sizeof(Room12VerbsSorted),
	sizeof(Room13VerbsSorted),
	sizeof(Room14VerbsSorted),
	sizeof(Room15VerbsSorted),
	sizeof(Room16VerbsSorted),
	sizeof(Room17VerbsSorted),
	sizeof(Room18VerbsSorted),
	sizeof(Room19VerbsSorted),
	sizeof(Room20VerbsSorted),
	sizeof(Room21VerbsSorted),
	sizeof(Room22VerbsSorted),
	sizeof(Room23VerbsSorted),
	sizeof(Room24VerbsSorted),
	sizeof(Room25VerbsSorted),
	sizeof(Room26VerbsSorted),
};

const unsigned char * const verbtable[] = {
	&Room0VerbsSorted[0],
	&Room1VerbsSorted[0],
	&Room2VerbsSorted[0],
	&Room3VerbsSorted[0],
	&Room4VerbsSorted[0],
	&Room5VerbsSorted[0],
	&Room6VerbsSorted[0],
	&Room7VerbsSorted[0],
	&Room8VerbsSorted[0],
	&Room9VerbsSorted[0],
	&Room10VerbsSorted[0],
	&Room11VerbsSorted[0],
	&Room12VerbsSorted[0],
	&Room13VerbsSorted[0],
	&Room14VerbsSorted[0],
	&Room15VerbsSorted[0],
	&Room16VerbsSorted[0],
	&Room17VerbsSorted[0],
	&Room18VerbsSorted[0],
	&Room19VerbsSorted[0],
	&Room20VerbsSorted[0],
	&Room21VerbsSorted[0],
	&Room22VerbsSorted[0],
	&Room23VerbsSorted[0],
	&Room24VerbsSorted[0],
	&Room25VerbsSorted[0],
	&Room26VerbsSorted[0],
};

unsigned char getRoomVerbLen() {
	return verbsize[MyLoc];
}

unsigned char getRoomVerb() {
	return verbtable[MyLoc][sorted_verb_index];
}

unsigned char decrease_verb_index() {
	if (sorted_verb_index > 0) {
		sorted_verb_index--;
        } else {
		sorted_verb_index = getRoomVerbLen() - 1;
	}
	return getRoomVerb();
}

unsigned char increase_verb_index() {
	if (sorted_verb_index < getRoomVerbLen() - 1) {
		sorted_verb_index++;
        } else {
		sorted_verb_index = 0;
	}
	return getRoomVerb();
}

unsigned char match_verb_index()
{
        for (sorted_verb_index = 0; sorted_verb_index < getRoomVerbLen(); sorted_verb_index++) {
                if (verb_index == getRoomVerb()) {
                        return verb_index;
                }
        }
        sorted_verb_index = 0;
        return getRoomVerb();
}

unsigned char const Room0NounsSorted[] = {
	NOUN_GAME
};

// flat
unsigned char const Room1NounsSorted[] = {
	NOUN_ANCHOR, // item
	NOUN_AWAY, // used
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_BUNYON, // used
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_STAIRS, // used
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_UPSTAIRS, // used
};

// alcove
unsigned char const Room2NounsSorted[] = {
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_BUNYON, // used
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DOWN, // used
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PASSAGEWAY, // used
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WINDOW, // used
	NOUN_WINGS, // item
};

// passageway
unsigned char const Room3NounsSorted[] = {
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_BUNYON, // used
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_EAST, // used
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WEST, // used
	NOUN_WINGS, // item
};

// attic
unsigned char const Room4NounsSorted[] = {
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_BUNYON,
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PIRATE, // used
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WEST, // used
	NOUN_WINGS, // item
};

// window
unsigned char const Room5NounsSorted[] = {
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_BUNYON,
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WINDOW, // used
	NOUN_WINGS, // item
	NOUN_YOHO // used
};

// beach
unsigned char const Room6NounsSorted[] = {
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOAT, // used
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_EAST, // used
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LAGOON, // used
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SAND, // used
	NOUN_SHIP, // used
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WINGS, // item
	NOUN_YOHO // used
};

// maze
unsigned char const Room7NounsSorted[] = {
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DOWN, // used
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_EAST, // used
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SAND, // item
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_SOUTH, // used
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WEST, // used
	NOUN_WINGS, // item
};

// meadow 
unsigned char const Room8NounsSorted[] = {
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_EAST, // used
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SAND, // item
	NOUN_SHACK, // used
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WEST, // used
	NOUN_WINGS, // item
};

// shack
unsigned char const Room9NounsSorted[] = {
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DESTRUCTIVE, // used
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PIRATE, // used
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SAND, // item
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WEST, // used
	NOUN_WINGS, // item
};

// ocean
unsigned char const Room10NounsSorted[] = {
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_EAST, // used
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NORTH, // used
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SALT, // item
	NOUN_SAND, // item
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_SOUTH, // used
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WATER, // item
	NOUN_WEST, // used
	NOUN_WINGS, // item
};

// pit
unsigned char const Room11NounsSorted[] = {
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DESTRUCTIVE, // used
	NOUN_DOOR, // used
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HALLWAY, // used
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SALT, // item
	NOUN_SAND, // item
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WATER, // item
	NOUN_UP, // used
	NOUN_WINGS, // item
};

// maze
unsigned char const Room12NounsSorted[] = {
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_EAST, // used
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NORTH, // used
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SAND, // item
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WEST, // used
	NOUN_WINGS, // item
};

// maze
unsigned char const Room13NounsSorted[] = {
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_EAST, // used
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NORTH, // used
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SAND, // item
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_SOUTH, // used
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WEST, // used
	NOUN_WINGS, // item
};

// hill
unsigned char const Room14NounsSorted[] = {
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_CAVES, // used
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_HILL, // used
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PATH, // used
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SAND, // item
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WEST, // used
	NOUN_WINGS, // item
};

// shed
unsigned char const Room15NounsSorted[] = {
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NORTH, // used
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SAND, // item
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WINGS, // item
};

// hallway
unsigned char const Room16NounsSorted[] = {
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_EAST, // used
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PIT, // used
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SAND, // item
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WINGS, // item
};

// cavern
unsigned char const Room17NounsSorted[] = {
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_CHEST, // item
	NOUN_CRACK, // used
	NOUN_CRACKERS, // item
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SAND, // item
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WEST, // used
	NOUN_WINGS, // item
};

// top of hill
unsigned char const Room18NounsSorted[] = {
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_CHEST, // item
	NOUN_CRACK, // used
	NOUN_CRACKERS, // item
	NOUN_DOWN, // used
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WINGS, // item
};

// maze
unsigned char const Room19NounsSorted[] = {
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_EAST, // used
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_SOUTH, // used
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WEST, // used
	NOUN_WINGS, // item
};

// ship
unsigned char const Room20NounsSorted[] = {
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BEACH, // used
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NOTE, // item
	NOUN_OFF, // used
	NOUN_PARROT, // item
	NOUN_PIRATE, // used
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SHORE, // used
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WINGS, // item
};

// beach
unsigned char const Room21NounsSorted[] = {
	NOUN_30,
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BOAT, // used
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SHIP, // used
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_SOUTH, // used
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WINGS, // item
};

// graveyard
unsigned char const Room22NounsSorted[] = {
	NOUN_30,
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_EAST, // used
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NORTH, // used
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PIRATE, // used
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WINGS, // item
};

// field
unsigned char const Room23NounsSorted[] = {
	NOUN_30,
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_EAST, // used
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONASTERY, // used
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WEST, // used
	NOUN_WINGS, // item
};

// lagoon
unsigned char const Room24NounsSorted[] = {
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_EAST, // used
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NORTH, // used
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_SOUTH, // used
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WEST, // used
	NOUN_WINGS, // item
};

// monastery
unsigned char const Room25NounsSorted[] = {
	NOUN_30,
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SHOVEL, // item
	NOUN_SNAKES, // used
	NOUN_SNEAKERS, // item
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WEST, // used
	NOUN_WINGS, // item
};

// hell
unsigned char const Room26NounsSorted[] = {
	NOUN_ANCHOR, // item
	NOUN_BAG, // item
	NOUN_BONES, // item
	NOUN_BOOK, // item
	NOUN_BOTTLE, // item
	NOUN_BOX, // item
	NOUN_CHEST, // item
	NOUN_CRACKERS, // item
	NOUN_DUBLEONS, // item
	NOUN_DUFFEL, // item
	NOUN_FISH, // item
	NOUN_FLYER, // item
	NOUN_HAMMER, // item
	NOUN_INVENTORY, // always
	NOUN_KEYS, // item
	NOUN_LUMBER, // item
	NOUN_MAP, // item
	NOUN_MATCHES, // item
	NOUN_MONGOOSE, // item
	NOUN_NAILS, // item
	NOUN_NOTE, // item
	NOUN_PARROT, // item
	NOUN_PLANS, // item
	NOUN_RUG, // item
	NOUN_RUM, // item
	NOUN_SACK, // item
	NOUN_SAILS, // item
	NOUN_SHOVEL, // item
	NOUN_SNEAKERS, // item
	NOUN_STAMPS, // item
	NOUN_TORCH, // item
	NOUN_WINGS, // item
};

const unsigned char * const nountable[] = {
	&Room0NounsSorted[0],
	&Room1NounsSorted[0],
	&Room2NounsSorted[0],
	&Room3NounsSorted[0],
	&Room4NounsSorted[0],
	&Room5NounsSorted[0],
	&Room6NounsSorted[0],
	&Room7NounsSorted[0],
	&Room8NounsSorted[0],
	&Room9NounsSorted[0],
	&Room10NounsSorted[0],
	&Room11NounsSorted[0],
	&Room12NounsSorted[0],
	&Room13NounsSorted[0],
	&Room14NounsSorted[0],
	&Room15NounsSorted[0],
	&Room16NounsSorted[0],
	&Room17NounsSorted[0],
	&Room18NounsSorted[0],
	&Room19NounsSorted[0],
	&Room20NounsSorted[0],
	&Room21NounsSorted[0],
	&Room22NounsSorted[0],
	&Room23NounsSorted[0],
	&Room24NounsSorted[0],
	&Room25NounsSorted[0],
	&Room26NounsSorted[0],
};

const unsigned char nounsize[] = {
	sizeof(Room0NounsSorted),
	sizeof(Room1NounsSorted),
	sizeof(Room2NounsSorted),
	sizeof(Room3NounsSorted),
	sizeof(Room4NounsSorted),
	sizeof(Room5NounsSorted),
	sizeof(Room6NounsSorted),
	sizeof(Room7NounsSorted),
	sizeof(Room8NounsSorted),
	sizeof(Room9NounsSorted),
	sizeof(Room10NounsSorted),
	sizeof(Room11NounsSorted),
	sizeof(Room12NounsSorted),
	sizeof(Room13NounsSorted),
	sizeof(Room14NounsSorted),
	sizeof(Room15NounsSorted),
	sizeof(Room16NounsSorted),
	sizeof(Room17NounsSorted),
	sizeof(Room18NounsSorted),
	sizeof(Room19NounsSorted),
	sizeof(Room20NounsSorted),
	sizeof(Room21NounsSorted),
	sizeof(Room22NounsSorted),
	sizeof(Room23NounsSorted),
	sizeof(Room24NounsSorted),
	sizeof(Room25NounsSorted),
	sizeof(Room26NounsSorted),
};

unsigned char getRoomNounLen() {
	return nounsize[MyLoc];
}

unsigned char getRoomNoun() {
	return nountable[MyLoc][sorted_noun_index];
}

// This routine will filter away pickable items that are not accessible
// right now
unsigned char isValidNoun(unsigned char noun)
{
	unsigned char n;
	unsigned char item;

	for (item = 0; item < nr_items; item++) {
		n = BaseNounForItems[item];
		if (n == noun) {
			if (getItemsLocation(item) == CARRIED) {
				return 1;
			}
			if (getItemsLocation(item) == MyLoc) {
				return 1;
			}
			if (n == NOUN_RUG) {
				if (getItemsLocation(ITEM_30) == CARRIED) {
					return 1;
				}
				if (getItemsLocation(ITEM_30) == MyLoc) {
					return 1;
				}
			}
			if (n == NOUN_BOTTLE) {
				if (getItemsLocation(ITEM_7) == CARRIED) {
					return 1;
				}
				if (getItemsLocation(ITEM_7) == MyLoc) {
					return 1;
				}
				if (getItemsLocation(ITEM_25) == CARRIED) {
					return 1;
				}
				if (getItemsLocation(ITEM_25) == MyLoc) {
					return 1;
				}
				if (getItemsLocation(ITEM_42) == CARRIED) {
					return 1;
				}
				if (getItemsLocation(ITEM_42) == MyLoc) {
					return 1;
				}
				if (getItemsLocation(ITEM_49) == CARRIED) {
					return 1;
				}
				if (getItemsLocation(ITEM_49) == MyLoc) {
					return 1;
				}
			}
			if (n == NOUN_CHEST) {
				if (getItemsLocation(ITEM_13) == CARRIED) {
					return 1;
				}
				if (getItemsLocation(ITEM_13) == MyLoc) {
					return 1;
				}
				if (getItemsLocation(ITEM_28) == CARRIED) {
					return 1;
				}
				if (getItemsLocation(ITEM_28) == MyLoc) {
					return 1;
				}
			}
			if ((MyLoc == ROOM_20) && (n == NOUN_SAILS)) {
				return 1;
			}
			if (n == NOUN_BOOK) {
				if (getItemsLocation(ITEM_2) == CARRIED) {
					return 1;
				}
				if (getItemsLocation(ITEM_2) == MyLoc) {
					return 1;
				}
				if (getItemsLocation(ITEM_3) == CARRIED) {
					return 1;
				}
				if (getItemsLocation(ITEM_3) == MyLoc) {
					return 1;
				}
			}
			if ((MyLoc == ROOM_1) && (n == NOUN_NAILS)) {
				return 1;
			}
			if (n == NOUN_TORCH) {
				if (getItemsLocation(ITEM_8) == CARRIED) {
					return 1;
				}
				if (getItemsLocation(ITEM_8) == MyLoc) {
					return 1;
				}
				if (getItemsLocation(ITEM_9) == CARRIED) {
					return 1;
				}
				if (getItemsLocation(ITEM_9) == MyLoc) {
					return 1;
				}
				if (getItemsLocation(ITEM_63) == CARRIED) {
					return 1;
				}
				if (getItemsLocation(ITEM_63) == MyLoc) {
					return 1;
				}
			}
			if (n == NOUN_WATER) {
				if (getItemsLocation(ITEM_42) == CARRIED) {
					return 1;
				}
				if (getItemsLocation(ITEM_42) == MyLoc) {
					return 1;
				}
			}
			return 0;
		}
	}
	return 1;
}

unsigned char possible_noun() {
	unsigned char noun;

	noun = getRoomNoun();
	noun = value((char *)NounsBase + noun);
	return isValidNoun(noun);
}

unsigned char decrease_noun_index() {
	do {
		if (sorted_noun_index > 0) {
			sorted_noun_index--;
        	} else {
			sorted_noun_index = getRoomNounLen() - 1;
		}
	} while (!possible_noun());
	return getRoomNoun();
}

unsigned char increase_noun_index() {
	do {
		if (sorted_noun_index < getRoomNounLen() - 1) {
			sorted_noun_index++;
        	} else {
			sorted_noun_index = 0;
		}
	} while (!possible_noun());
	return getRoomNoun();
}

unsigned char match_noun_index()
{
        for (sorted_noun_index = 0; sorted_noun_index < getRoomNounLen(); sorted_noun_index++) {
                if (noun_index == getRoomNoun()) {
			break;
                }
        }
	increase_noun_index();
	return decrease_noun_index();
}

unsigned char possible(unsigned char v, unsigned char n) {
	unsigned char found = 0;

        for (sorted_verb_index = 0; sorted_verb_index < getRoomVerbLen(); sorted_verb_index++) {
                if (v == getRoomVerb()) {
                        found = 1;
                }
        }
	if (!found) return 0;
	if (n != NOUN_ANY) {
		found = 0;

		n = value((char *)NounsBase + n);
		if (isValidNoun(n)) {
			found = 1;
		}
	}
	return found;
}

unsigned char PerformLine(CONST char *action)
{
	unsigned char continuation;
	unsigned char pptr;
	unsigned char cc, i, j;
	unsigned char ct;
	unsigned char n;
	char *s;
	unsigned char i1;
	unsigned char i2;
	unsigned char t;
	unsigned char u;

	i = CheckConditions(action);
	if (i == CONDITION_FAILED)
		return i;
	action += 10;
	continuation = 0;
	/* Actions */
	cc = 0;
	pptr = 0;
	while (cc < 4) {
		switch ((unsigned char)value(action)) {
		case 0:	/* NOP */
			break;
		case GETX:	//GET
			GetItem(param[pptr]);
			pptr++;
			break;
		case DROPX:	// MOVE_INTO_AR
			DropItem(param[pptr]);
			pptr++;
			break;
		case GOTOY:	// GOTO
			Relook = 1;
			MyLoc = param[pptr++];
			break;
		case XTORM02:
		case XTORM0:	//REMOVE
			i = param[pptr];
			if (getItemsLocation(i) == MyLoc) {
				Redraw = 1;
			}
			ItemsLocation[i] = 0;
			pptr++;
			break;
		case NIGHT:	// SET_NIGHT
			setBitFlags(14);
			break;
		case DAY:	// SET_DAY
			clearBitFlags(14);
			break;
		case SETZ:	// SET_BIT
			setBitFlags(param[pptr]);
			pptr++;
			break;
		case CLRZ:	// CLEAR_BIT
			clearBitFlags(param[pptr]);
			pptr++;
			break;
		case DEAD:	// KILL_PLAYER
			OutBuf("I am dead.\\n");
			clearBitFlags(14);
			MyLoc = nr_rooms - 1;	/* It seems to be what the code says! */
			Look();
			break;
		case XTOY:	// MOVE_X_INTO_Y
			i = param[pptr];
			pptr++;
			j = param[pptr];
			pptr++;
			ItemsLocation[i] = j;
			Redraw = 1;
			break;
		case FINI:	// QUIT
doneit:			OutBuf("The game is now over.\\n");
			wrefresh();
			return QUIT_GAME;
			break;
		case DISPRM2:
		case DISPRM:	// LOOK
			if (CurrentCounter == 1) { // BUNYON
#ifdef USESFX
				playsfx0(sfx_transporter);
#endif
				CurrentCounter = 0;
			}
			if (CurrentCounter == 2) { // CHOP TREE
#ifdef USESFX
				playsfx0(sfx_falling);
#endif
				CurrentCounter = 0;
			}
			if (CurrentCounter == 3) {
#ifdef USESFX
				playsfx0(sfx_wolfwhistle); // GENIE
#endif
				CurrentCounter = 0;
			}
			if (CurrentCounter == 4) { // Explosion
#ifdef USESFX
				playsfx0(sfx_teleported);
#endif
				CurrentCounter = 0;
			}
			if (CurrentCounter == 5) { // Unlock door
#ifdef USESFX
				playsfx0(sfx_electroswitch);
#endif
				CurrentCounter = 0;
			}
			if (CurrentCounter == 6) { // Jump
#ifdef USESFX
				playsfx0(sfx_hophipquick);
#endif
				CurrentCounter = 0;
			}
			if (CurrentCounter == 7) { // AWAY
#ifdef USESFX
				playsfx0(sfx_teleported);
#endif
				CurrentCounter = 0;
			}
			if (CurrentCounter == 8) { // scream
#ifdef USESFX
				playsfx0(sfx_whoa);
#endif
				CurrentCounter = 0;
			}
			if (MyLoc == ROOM_24) {
#ifdef USESFX
				playsfx0(sfx_hahaha);
#endif
			}
			Look();
			break;
		case SCORE:	// SCORE
			ct = 0;
			n = 0;
			while (ct < nr_items) {
				if (getItemsLocation(ct) == TREASURE_ROOM) {
					s = ItemsText[ct];
					if (star(s))
						n++;
				}
				ct++;
			}
			if (n == TREASURES) {
				OutBuf
				    ("Your adventure is over.");
				goto doneit;
			} else {
				OutBuf
				    ("You need to bring back two treasures to win the game.\\n");
				if (n == 1) {
					OutBuf
					    ("Now you have only one.\\n");
				} else {
					OutBuf
					    ("Now you have none.\\n");
				}
			}
			break;
		case INV:	// INVENTORY
			{
				unsigned char ct;
				unsigned char f;
				char *s;
				ct = 0;
				f = 0;
				OutBuf("I'm carrying:");
				scroll();
				while (ct < nr_items) {
					if (getItemsLocation(ct) ==
					    CARRIED) {
						OutBuf(" - ");
						s = ItemsText[ct];
						OutBuf(s);
						scroll();
						f = 1;
					}
					ct++;
				}
				if (f == 0) {
					OutBuf("Nothing");
					scroll();
				}
				break;
			}
		case SET0:	// SET_BIT
			setBitFlags(0);
			break;
		case CLR0:	// CLEAR_BIT
			clearBitFlags(0);
			break;
		case FILL:	// FILL
			LightTime=150;
			ItemsLocation[LIGHT_SOURCE] = CARRIED;
			clearBitFlags(15);
			break;
		case CLS:	// CLS
			ClearScreen();	/* pdd. */
			OutReset();
			break;
		case SAVE:	// SAVE
			OutBuf("Cannot save game to eeprom\\n");
			break;
		case EXX:	// SWAP_ITEMS
			i1 = param[pptr++];
			i2 = param[pptr++];
			t = ItemsLocation[i1];
			u = ItemsLocation[i2];
			if (t == MyLoc
			    || u == MyLoc)
				Redraw = 1;
			ItemsLocation[i1] = u;
			ItemsLocation[i2] = t;
			break;
		case CONT:
			continuation=1;
			break;
		case AGETX:
			i = param[pptr];
			pptr++;
			ItemsLocation[i] = CARRIED;
			break;
		case BYXFX:	// PUT_X_WITH_Y
			i1 = param[pptr++];
			i2 = param[pptr++];
			t = ItemsLocation[i1];
			u = ItemsLocation[i2];
			if (t == MyLoc)
				Redraw = 1;
			ItemsLocation[i1] = u;
			if (getItemsLocation(i2) == MyLoc)
				Redraw = 1;
			break;
		case CSUB1:	// Decrement CurrentCounter
			if(CurrentCounter >= 0)
				CurrentCounter--;
			break;
		case DISPC:	// Display CurrentCounter
			//OutputNumber(CurrentCounter);
			break;
		case CFP1:	// Set CurrentCounter to value
			CurrentCounter = param[pptr++];
			break;
		case EXRM0:	// EXRM0 - not implemented
			//int t=MyLoc;
			//MyLoc=SavedRoom;
			//SavedRoom=t;
			break;
		case EXMC:	// EXMC - not implemented
			//int t=param[pptr++];
			//int c1=CurrentCounter;
			//CurrentCounter=Counters[t];
			//Counters[t] = c1;
			break;
		case CADD:	// Add to CurrentCounter
			i = param[pptr];
			pptr++;
			CurrentCounter += i;
			break;
		case CSUB:	// Subtract from CurrentCounter
			i = param[pptr];
			pptr++;
			CurrentCounter -= i;
			if (CurrentCounter < -1)
				CurrentCounter = -1;
			break;
		case SAYN:	// ECHO_NOUN
			DrawNoun(NounToken);
			break;
		case SAYNCR:	// ECHO_NOUN_CR
			DrawNoun(NounToken);
			scroll();
			break;
		case SAYCR:	// ECHO_CR
			scroll();
			break;
		case EXCC:	// EXCC - not implemented
			//int p = param[pptr++];
			//int sr = MyLoc;
			//MyLoc = RoomSaved[p];
			//RoomSaved[p] = sr;
			break;
		case DELAY:	// DELAY - not implemented
			break;
		case DRAWPIC:	// DELAY - not implemented
			pptr++;
			break;
		default:
			i = *action;
			if (i > 101)
				i -= 50;
			DrawMessage(i);
			break;
		}
		cc++;
		action++;
	}
	if (continuation) {
		return PROCESS_AGAIN;
	}
	return SUCCESS;
}

unsigned char getItem(unsigned char noun)
{
	unsigned char n;
	unsigned char item;

	for (item = 0; item < nr_items; item++) {
		n = BaseNounForItems[item];
		if ((n == noun) && (getItemsLocation(item) == MyLoc)) {
			return item;
		}
	}
	return NOTFOUND;
}

unsigned char getCarriedItem(unsigned char noun)
{
	unsigned char n;
	unsigned char item;

	for (item = 0; item < nr_items; item++) {
		n = BaseNounForItems[item];
		if ((n == noun) && (getItemsLocation(item) == CARRIED)) {
			return item;
		}
	}
	return NOTFOUND;
}

unsigned char PerformActions(int vb, int no)
{
	CONST unsigned char *action;
	unsigned char ct;
	unsigned char fl;
	unsigned char t;
	unsigned char u;

	gotoxy(0, curs);
	// Save given noun in case I want to say it
	if (no == -1) {
		NounToken = 0;
	} else {
		NounToken = no;
	}

	// Check if direction is given.
	t = VerbsBase[vb];
	if (t == 1 && no == -1) {
		OutBuf("Give me a direction too.\\n");
		wrefresh();
		return (0);
	}

	// The movement in obvious direction is carried out first
	if (t == 1 && no >= 1 && no <= 6) {
		if (!hasLight()) {
			OutBuf("Dangerous to move in the dark!\\n");
		}
		u = getExit(no - 1);
		if (u) {
			// Go to new room
			MyLoc = u;
			Look();
			return (0);
		}
		if (!hasLight()) {
			OutBuf("I fell down and broke my neck.\\n");
			wrefresh();
			return QUIT_GAME;
		}
		OutBuf("I can't go in that direction.\\n");
		wrefresh();
		return (0);
	}
	fl = NOT_UNDERSTOOD;
	action = Actions;
	ct = 0;
	while (ct < nr_actions) {
		unsigned char vv, nv;
		vv = *action;
		action++;
		nv = *action;
		action++;
		t = vv == value((char *)VerbsBase + vb);
		if (t) {
			if ((vv == 0 && RandomPercent(nv)) ||
			    (vv != 0 && (nv == value((char *)NounsBase + no) || nv == 0))) {
				unsigned char f2;
				if (fl == NOT_UNDERSTOOD)
					fl = NOT_YET;
				do {
					f2 = PerformLine((char *)action);
					if (f2 == PROCESS_AGAIN) {
						ct++;
						action += 16;
					}
				} while (f2 == PROCESS_AGAIN);
				switch (f2) {
				case CONDITION_FAILED:
					break;
				case QUIT_GAME:
					fl = QUIT_GAME;
					break;
				case SUCCESS:
					if (vb)
						return 0;
					break;
				}
			}
		}
		ct++;
		action += 14;
	}
	if (fl != 0) {
		int i;
		unsigned char vv;
		unsigned char nv;
		vv = value((char *)VerbsBase + vb);
		nv = value((char *)NounsBase + no);
		if (vv == VERB_GET) {
			if (no == -1) {
				OutBuf("What?\\n");
				return (0);
			}
			if (CountCarried() == MaxCarry) {
				OutBuf("I've too much to carry.\\n");
				return (0);
			}
			i = getItem(nv);
			if (i == NOTFOUND) {
				OutBuf
				    ("It's beyond my power to do that.\\n");
				return (0);
			}
			ItemsLocation[i] = CARRIED;
#ifdef USESFX
			playsfx0(sfx_advpickup);
#endif
			OutBuf("O.K.\\n");
			Relook = 1;
			return (0);
		}
		if (vv == VERB_DROP) {
			if (no == -1) {
				OutBuf("What?\\n");
				return (0);
			}
			i = getCarriedItem(nv);
			if (i == NOTFOUND) {
				OutBuf
				    ("It's beyond my power to do that.\\n");
				return (0);
			}
			ItemsLocation[i] = MyLoc;
#ifdef USESFX
			playsfx0(sfx_advdrop);
#endif
			OutBuf("O.K.\\n");
			Relook = 1;
			return (0);
		}
	}
	return (fl);
}

#define SCROLL_MODE 0
#define VERB_MODE 1
#define NOUN_MODE 2
#define END_GAME 3

ramchip unsigned char gamemode;

#define FIRST_LOOK 0
#define TIME_ADVANCE 1
#define SECOND_LOOK 2
#define USER_INPUT 3
#define USER_ACTION 4
#define LIGHT_FADE 5

ramchip unsigned char gamestate;

void cleancl()
{
	delete_line(TOP_HEIGHT + STORY_LINES - 1);
	//cursor(1);
	gotoxy(0, TOP_HEIGHT + STORY_LINES - 1);
	textcolor(1);
	cputc('?');
	printfooter();
	gotoxy(1, TOP_HEIGHT + STORY_LINES - 1);
}

void display_input(int vi, int ni)
{
	char *s;

	cleancl();
	textcolor(7);
	s = Verbs[vi];
	if (star(s)) {
		s++;
	}
	cputs(s);
	if ((gamemode == NOUN_MODE) && (ni >= 0)) {
		cputc(' ');
		s = Nouns[ni];
		if (star(s)) {
			s++;
		}
		cputs(s);
	}
}

void first_look()
{
	if (Relook != 0) {
		Look();
	}
	gamestate = TIME_ADVANCE;
}

void time_advance()
{
	PerformActions(0, 0);
	gamestate = SECOND_LOOK;
}

void second_look()
{
	if (Relook != 0) {
		Look();
	}
	gamestate = USER_INPUT;
	cleancl();
}

ramchip unsigned char WaitforRelease;
ramchip unsigned char joy;
ramchip unsigned char restoregui;


unsigned char joystick_pressed()
{
#ifdef COMPILER_CC65
	return joy;
#else
	luck++;
	joystick_update();
	if (joystick[0] & JOYSTICK_UP) return 1;
	if (joystick[0] & JOYSTICK_RIGHT) return 1;
	if (joystick[0] & JOYSTICK_LEFT) return 1;
	if (joystick[0] & JOYSTICK_DOWN) return 1;
	if (joystick[0] & JOYSTICK_BUTTON1) return 1;
	if (joystick[0] & JOYSTICK_BUTTON2) return 1;
	return 0;
#endif
}

void read_joysticks()
{
#ifdef COMPILER_CC65
	joy = joy_read(JOY_1) | joy_read(JOY_2);
	if ((joy == 0) && (readselect() == 0)) WaitforRelease = 0;
#else
	if (!joystick_pressed()) WaitforRelease = 0;
#endif
}

unsigned char joystick_up()
{
#ifdef COMPILER_CC65
	return JOY_UP(joy);
#else
	return joystick[0] & JOYSTICK_UP;
#endif
}

unsigned char joystick_down()
{
#ifdef COMPILER_CC65
	return JOY_DOWN(joy);
#else
	return joystick[0] & JOYSTICK_DOWN;
#endif
}

unsigned char joystick_left()
{
#ifdef COMPILER_CC65
	return JOY_LEFT(joy);
#else
	return joystick[0] & JOYSTICK_LEFT;
#endif
}

unsigned char joystick_right()
{
#ifdef COMPILER_CC65
	return JOY_RIGHT(joy);
#else
	return joystick[0] & JOYSTICK_RIGHT;
#endif
}

unsigned char joystick_btn1w()
{
#ifdef COMPILER_CC65
	return JOY_BTN_1(joy) && !WaitforRelease && (gamemode != END_GAME);
#else
	if (WaitforRelease) return 0;
	return joystick[0] & JOYSTICK_BUTTON1;
#endif
}

unsigned char joystick_btn2w()
{
#ifdef COMPILER_CC65
	return (JOY_BTN_2(joy) || readselect()) && !WaitforRelease && (gamemode != END_GAME);
#else
	return joystick[0] & JOYSTICK_BUTTON2;
#endif
}

void joystick_toggled()
{
	while (!joystick_pressed()) ;
	while (joystick_pressed()) ;
}

static int solution_index = -1;
const char solution[] = {
	// at flat
	VERB_GET, NOUN_BOTTLE,
	VERB_GET, NOUN_CRACKERS,
	VERB_GET, NOUN_SNEAKERS,
	VERB_REMOVE, NOUN_RUG,
	VERB_CLIMB, NOUN_STAIRS,
	VERB_GET, NOUN_BOOK,
	VERB_OPEN, NOUN_BOOK,
	VERB_SHAKE, NOUN_BOOK,
	VERB_LOOK, NOUN_ANY,
	VERB_READ, NOUN_FLYER,
	VERB_READ, NOUN_NOTE,
	VERB_READ, NOUN_BOOK,
	VERB_ENTER, NOUN_PASSAGEWAY,
	VERB_GO, NOUN_EAST,
	VERB_OPEN, NOUN_BAG,
	VERB_LOOK, NOUN_ANY,
	VERB_GET, NOUN_TORCH,
	VERB_GET, NOUN_MATCHES,
	VERB_GO, NOUN_WEST,
	VERB_GO, NOUN_WEST,
	VERB_ENTER, NOUN_WINDOW,
	VERB_SAY, NOUN_YOHO,
	// drop book at beach
	VERB_DROP, NOUN_BOOK,
	// get parrot
	VERB_GO, NOUN_EAST,
	VERB_ENTER, NOUN_SHACK,
	VERB_GIVE, NOUN_BOTTLE,
	VERB_GET, NOUN_PARROT,
	VERB_GO, NOUN_WEST,
	// get parts for boat
	VERB_GO, NOUN_EAST,
	VERB_CLIMB, NOUN_PATH,
	VERB_ENTER, NOUN_CRACK,
	VERB_LIGHT, NOUN_TORCH,
	VERB_GET, NOUN_SAILS,
	VERB_DROP, NOUN_PARROT,
	VERB_DROP, NOUN_CRACKERS,
	VERB_ENTER, NOUN_CRACK,
	VERB_DROP, NOUN_SAILS,
	VERB_ENTER, NOUN_CRACK,
	VERB_ENTER, NOUN_SHED,
	VERB_GET, NOUN_HAMMER,
	VERB_GET, NOUN_SHOVEL,
	VERB_GET, NOUN_WINGS,
	VERB_GO, NOUN_NORTH,
	VERB_DROP, NOUN_SHOVEL,
	VERB_ENTER, NOUN_CRACK,
	VERB_UNLIGHT, NOUN_TORCH,
	VERB_GET, NOUN_SAILS,
	VERB_GO, NOUN_DOWN,
	VERB_GO, NOUN_WEST,
	VERB_GO, NOUN_WEST,
	// at boat building place
	VERB_DROP, NOUN_MATCHES,
	VERB_DROP, NOUN_TORCH,
	VERB_DROP, NOUN_SAILS,
	VERB_DROP, NOUN_WINGS,
	// go for bottle, nails and keys
	VERB_GET, NOUN_BOOK,
	VERB_SAY, NOUN_YOHO,
	VERB_ENTER, NOUN_WINDOW,
	VERB_ENTER, NOUN_PASSAGEWAY,
	VERB_GO, NOUN_EAST,
	VERB_GET, NOUN_BOTTLE,
	VERB_GO, NOUN_WEST,
	VERB_GO, NOUN_WEST,
	VERB_GO, NOUN_DOWN,
	VERB_GET, NOUN_NAILS,
	VERB_REMOVE, NOUN_RUG,
	VERB_LOOK, NOUN_ANY,
	VERB_DROP, NOUN_RUG,
	VERB_GET, NOUN_KEYS,
	VERB_CLIMB, NOUN_STAIRS,
	VERB_ENTER, NOUN_WINDOW,
	VERB_SAY, NOUN_YOHO,
	// back at beach
	VERB_DROP, NOUN_BOOK,
	VERB_DROP, NOUN_NAILS,
	VERB_DROP, NOUN_HAMMER,
	// tackle chest
	VERB_GO, NOUN_EAST,
	VERB_ENTER, NOUN_SHACK,
	VERB_UNLOCK, NOUN_CHEST,
	VERB_EXAMINE, NOUN_CHEST,
	VERB_GET, NOUN_PLANS,
	VERB_READ, NOUN_PLANS,
	VERB_EXAMINE, NOUN_CHEST,
	VERB_GET, NOUN_MAP,
	VERB_READ, NOUN_MAP,
	VERB_GO, NOUN_WEST,
	VERB_GO, NOUN_WEST,
	VERB_DROP, NOUN_PLANS,
	VERB_DROP, NOUN_MAP,
	// catch fish
	VERB_GET, NOUN_WINGS,
	VERB_ENTER, NOUN_LAGOON,
	VERB_GO, NOUN_NORTH,
	VERB_GET, NOUN_WATER,
	VERB_CATCH, NOUN_FISH,
	VERB_GO, NOUN_SOUTH,
	VERB_GO, NOUN_SOUTH,
	VERB_DROP, NOUN_WINGS,
	// get rid of crocodiles
	VERB_GET, NOUN_MATCHES,
	VERB_GET, NOUN_TORCH,
	VERB_GO, NOUN_EAST,
	VERB_GO, NOUN_EAST,
	VERB_ENTER, NOUN_CAVES,
	VERB_LIGHT, NOUN_TORCH,
	VERB_GO, NOUN_DOWN,
	VERB_GIVE, NOUN_FISH,
	VERB_DROP, NOUN_BOTTLE,
	VERB_WAIT, 1,
	// unlock door
	VERB_UNLOCK, NOUN_DOOR,
	VERB_DROP, NOUN_KEYS, // don't need them anymore
	// get stuff from cavern
	VERB_ENTER, NOUN_HALLWAY,
	VERB_GO, NOUN_EAST,
	VERB_GET, NOUN_SHOVEL,
	VERB_GET, NOUN_LUMBER,
	VERB_GET, NOUN_PARROT,
	VERB_GET, NOUN_CRACKERS,
	VERB_GO, NOUN_WEST,
	VERB_GO, NOUN_PIT,
	VERB_GO, NOUN_UP,
	VERB_GO, NOUN_EAST,
	VERB_GO, NOUN_SOUTH,
	VERB_UNLIGHT, NOUN_TORCH,
	VERB_DROP, NOUN_TORCH, // don't need them anymore
	VERB_DROP, NOUN_MATCHES, // don't need them anymore
	VERB_GO, NOUN_WEST,
	// at beach
	VERB_GO, NOUN_WEST,
	VERB_DROP, NOUN_LUMBER,
	VERB_GET, NOUN_WINGS,
	VERB_ENTER, NOUN_LAGOON,
	VERB_WAIT, 2,
	VERB_DIG, NOUN_ANY,
	VERB_GET, NOUN_ANCHOR,
	VERB_GO, NOUN_SOUTH,
	VERB_DROP, NOUN_ANCHOR,
	VERB_DROP, NOUN_WINGS,
	VERB_BUILD, NOUN_SHIP,
	// wait for tide out
	VERB_GET, NOUN_HAMMER,
	VERB_GET, NOUN_MAP,
	VERB_GO, NOUN_BOAT,
	VERB_DROP, NOUN_PARROT,
	VERB_DROP, NOUN_HAMMER,
	VERB_DROP, NOUN_SHOVEL,
	VERB_DROP, NOUN_CRACKERS,
	VERB_DROP, NOUN_MAP,
	VERB_GO, NOUN_SHORE,
	VERB_GET, NOUN_BOOK,
	VERB_SAY, NOUN_YOHO,
	VERB_ENTER, NOUN_WINDOW,
	VERB_ENTER, NOUN_PASSAGEWAY,
	VERB_GO, NOUN_EAST,
	VERB_WAKE, NOUN_PIRATE,
	VERB_GO, NOUN_WEST,
	VERB_GO, NOUN_WEST,
	VERB_ENTER, NOUN_WINDOW,
	VERB_SAY, NOUN_YOHO,
	VERB_DROP, NOUN_BOOK,
	VERB_GO, NOUN_BOAT,
	VERB_GET, NOUN_SHOVEL,
	VERB_GET, NOUN_HAMMER,
	VERB_WAIT, 3,
	VERB_SET, NOUN_SAILS,
	VERB_GO, NOUN_SHORE,
	VERB_DIG, NOUN_ANY,
	VERB_WAIT, 4,
	VERB_ENTER, NOUN_BOAT,
	VERB_GET, NOUN_CRACKERS,
	VERB_GET, NOUN_PARROT,
	VERB_GO, NOUN_SHORE,
	VERB_GO, NOUN_SOUTH,
	VERB_GO, NOUN_EAST,
	VERB_WALK, NOUN_30,
	VERB_DIG, NOUN_ANY,
	VERB_DROP, NOUN_SHOVEL,
	VERB_GET, NOUN_BOX,
	VERB_OPEN, NOUN_BOX,
	VERB_DROP, NOUN_HAMMER,
	VERB_DROP, NOUN_BOX,
	VERB_GET, NOUN_STAMPS,
	VERB_ENTER, NOUN_MONASTERY,
	VERB_DROP, NOUN_PARROT,
	VERB_WAIT, 5,
	VERB_DROP, NOUN_CRACKERS,
	VERB_GET, NOUN_DUBLEONS,
	VERB_GO, NOUN_WEST,
	VERB_GO, NOUN_WEST,
	VERB_WAKE, NOUN_PIRATE,
	VERB_GO, NOUN_NORTH,
	VERB_GO, NOUN_BOAT,
	VERB_SET, NOUN_SAILS,
	VERB_GO, NOUN_SHORE,
	VERB_GET, NOUN_BOOK,
	VERB_SAY, NOUN_YOHO,
	VERB_ENTER, NOUN_WINDOW,
	VERB_GO, NOUN_DOWN,
	VERB_DROP, NOUN_DUBLEONS,
	VERB_DROP, NOUN_STAMPS,
	VERB_SCORE, NOUN_ANY
};

void user_input()
{
	read_joysticks();
	flicker = 1 - flicker;
	switchpalettes();
	if (joystick_up()) {
		switch (gamemode) {
		case END_GAME:
			break;
		case SCROLL_MODE:
			gamemode = VERB_MODE;
			verb_index = match_verb_index();
			display_input(verb_index, noun_index);
			break;
		case VERB_MODE:
#if 1
			verb_index = decrease_verb_index();
#else
			if (sorted_verb_index > 0)
				sorted_verb_index--;
			else
				sorted_verb_index = NR_VERBS_SORTED - 1;
			verb_index = VerbsSorted[sorted_verb_index];
#endif
			display_input(verb_index, noun_index);
			break;
		case NOUN_MODE:
#if 1
			noun_index = decrease_noun_index();
#else
			if (sorted_noun_index > 0)
				sorted_noun_index--;
			else
				sorted_noun_index = NR_NOUNS_SORTED - 1;
			noun_index = NounsSorted[sorted_noun_index];
#endif
			display_input(verb_index, noun_index);
			break;
		}
	}
	if (joystick_down()) {
		switch (gamemode) {
		case END_GAME:
			break;
		case SCROLL_MODE:
			gamemode = VERB_MODE;
			verb_index = match_verb_index();
			display_input(verb_index, noun_index);
			break;
		case VERB_MODE:
#if 1
			verb_index = increase_verb_index();
#else
			if (sorted_verb_index < NR_VERBS_SORTED - 1)
				sorted_verb_index++;
			else
				sorted_verb_index = 0;
			verb_index = VerbsSorted[sorted_verb_index];
#endif
			display_input(verb_index, noun_index);
			break;
		case NOUN_MODE:
#if 1
			noun_index = increase_noun_index();
#else
			if (sorted_noun_index < NR_NOUNS_SORTED - 1)
				sorted_noun_index++;
			else
				sorted_noun_index = 0;
			noun_index = NounsSorted[sorted_noun_index];
#endif
			display_input(verb_index, noun_index);
			break;
		}
	}
	if (joystick_left()) {
		switch (gamemode) {
		case VERB_MODE:
			gamemode = SCROLL_MODE;
			wrefresh();
			break;
		case NOUN_MODE:
			gamemode = VERB_MODE;
			display_input(verb_index, noun_index);
			break;
		default:
			break;
		}
	}
	if (joystick_right()) {
		switch (gamemode) {
		case SCROLL_MODE:
			gamemode = VERB_MODE;
			verb_index = match_verb_index();
			display_input(verb_index, noun_index);
			break;
		case VERB_MODE:
			gamemode = NOUN_MODE;
			noun_index = match_noun_index();
			display_input(verb_index, noun_index);
			break;
		default:
			break;
		}
	}
	if (joystick_btn1w()) {
		WaitforRelease = 1;
		switch (gamemode) {
		case END_GAME:
			break;
		case SCROLL_MODE:
			OutReset();
			OutBuf("Use joypad first");
			wrefresh();
			break;
		case VERB_MODE:
			noun_index = -1;
			gamestate = USER_ACTION;
			//scroll();
			gotoxy(0, TOP_HEIGHT);
			cleantextarea();
			Relook = 0;
			Redraw = 0;
			break;
		case NOUN_MODE:
			gamestate = USER_ACTION;
			//scroll();
			gotoxy(0, TOP_HEIGHT);
			cleantextarea();
			Relook = 0;
			Redraw = 0;
			break;
		}
	}
	if (joystick_btn2w()) {
		//WaitforRelease = 1;
		if (solution_index == -1)
			solution_index = 0;
		//if (gamemode == END_GAME)
		//      break;
		if (solution_index >= 0) {
			gamemode = NOUN_MODE;
			verb_index = solution[solution_index];
			noun_index = solution[solution_index+1];
			if (verb_index == VERB_WAIT) {
				switch (noun_index) {
				case 1:
					// Wait for crocodiles to leave
					if (getItemsLocation(ITEM_17) == MyLoc) {
						noun_index = NOUN_ANY;
					} else {
						solution_index +=2;
						verb_index = solution[solution_index];
						noun_index = solution[solution_index+1];
						solution_index +=2;
					}
					break;
				case 2:
					// Wait for anchor
					if (getItemsLocation(ITEM_15) == MyLoc) {
						solution_index +=2;
						verb_index = solution[solution_index];
						noun_index = solution[solution_index+1];
						solution_index +=2;
					} else {
						noun_index = NOUN_ANY;
					}
					break;
				case 3:
					// Wait for tide
					if (getItemsLocation(ITEM_53) == ROOM_24) {
						solution_index +=2;
						verb_index = solution[solution_index];
						noun_index = solution[solution_index+1];
						solution_index +=2;
					} else {
						noun_index = NOUN_ANY;
					}
					break;
				case 4:
					// Wait for pirate to vanish with rum
					if (getItemsLocation(ITEM_12) == MyLoc) {
						noun_index = NOUN_ANY;
					} else {
						solution_index +=2;
						verb_index = solution[solution_index];
						noun_index = solution[solution_index+1];
						solution_index +=2;
					}
					break;
				case 5:
					// Wait for snakes to leave
					if (getItemsLocation(ITEM_23) == MyLoc) {
						noun_index = NOUN_ANY;
					} else {
						solution_index +=2;
						verb_index = solution[solution_index];
						noun_index = solution[solution_index+1];
						solution_index +=2;
					}
					break;
				default:
					solution_index +=2;
				}
			} else {
				solution_index += 2;
			}
			if (noun_index > 0)
				gamemode = NOUN_MODE;
			else
				gamemode = VERB_MODE;
			display_input(verb_index, noun_index);
			wrefresh();
			if (possible(verb_index, noun_index)) {
				gamestate = USER_ACTION;
			}
			scroll();
		} else {
			textcolor(1);
			OutBuf("look\\n");
			textcolor(2);
			wrefresh();
			gamestate = USER_ACTION;
		}
	}
	delay(5);
}

void user_action()
{
	unsigned char dirty;

	dirty = 0;
        cleantextarea();
	gotoxy(0, TOP_HEIGHT);
	curs = wherey();
	switch (PerformActions(verb_index, noun_index)) {
	case NOT_UNDERSTOOD:
		OutBuf("I don't understand your command.\\n");
		wrefresh();
		gamemode = SCROLL_MODE;
		break;
	case NOT_YET:
		OutBuf("I can't do that yet.\\n");
		wrefresh();
		gamemode = SCROLL_MODE;
		break;
	case QUIT_GAME:		// End of game
		gamemode = END_GAME;
		break;
	default:
		//if (dirty) {
		//	wrefresh();
		//}
		
		gamemode = SCROLL_MODE;
		break;
	}
	wrefresh();
	gamestate = LIGHT_FADE;
}

void light_fade()
{
	if (getItemsLocation(LIGHT_SOURCE) == DESTROYED) {
		// Burning lamp is at nowhere
		gamestate = FIRST_LOOK;
		return;
	}
	if (LightTime != -1) {
		LightTime--;
		if (LightTime < 1) {
			setBitFlags(15);
			if (getItemsLocation(LIGHT_SOURCE) ==
			    CARRIED || getItemsLocation(LIGHT_SOURCE) == MyLoc) {
				OutBuf("Your light has run out. ");
				wrefresh();
			}
		} else if (LightTime < 25) {
			if (getItemsLocation(LIGHT_SOURCE) ==
			    CARRIED || getItemsLocation(LIGHT_SOURCE) == MyLoc) {
				if (LightTime % 5 == 0) {
					OutBuf("Your light is growing dim. ");
					wrefresh();
				}
			}
		}
	}
	gamestate = FIRST_LOOK;
}

void main(void)
{
	unsigned char i;
	unsigned char t;

#ifdef COMPILER_CC65
	joy_install(joy_static_stddrv);
	cursor(0);
	srand(1234);
#endif
	clrscr();
        gotoxy(0, TOP_HEIGHT + STORY_LINES - 1);
#ifdef COMPILER_CC7800
	multisprite_enable_dli(7);
#endif
	MyLoc = START_ROOM;
	CurrentCounter = 0;
	BitFlags = 0;
	//setBitFlags(13);
	gamemode = SCROLL_MODE;
	gamestate = FIRST_LOOK;
	restoregui = 1;
	LightTime = 150;
	for (i = 0; i < nr_items; i++) {
		t = ItemsInitialLoc[i];
		ItemsLocation[i] = t;
	}
	OutReset();
	sorted_verb_index = 0;
	sorted_noun_index = 0;
	verb_index = VerbsSorted[sorted_verb_index];
	noun_index = NounsSorted[sorted_noun_index];
	Relook = 1;

#ifdef USESFX
	playsfx0(sfx_advpickup);
#endif
        sub_start_song(tunenr);
        tt_wait = 0;
        //tunenr = 1 - tunenr;

	//MyLoc = ROOM_1;  // london
	//MyLoc = ROOM_2;  // alcove
	//MyLoc = ROOM_3;  // passageway
	//MyLoc = ROOM_4;  // attic
	//MyLoc = ROOM_6;  // island
	//MyLoc = ROOM_7;  // maze
	//MyLoc = ROOM_8;  // meadow
	//MyLoc = ROOM_9;  // shack
	//ItemsLocation[ITEM_54] = CARRIED;
	//MyLoc = ROOM_10;  // ocean
	//MyLoc = ROOM_11;  // pit
	//MyLoc = ROOM_14;  // hill
	//MyLoc = ROOM_15;  // shed
	//MyLoc = ROOM_16;  // hallway
	//MyLoc = ROOM_17;  // cavern
	//MyLoc = ROOM_18;  // top
	//MyLoc = ROOM_20;  // ship
	//MyLoc = ROOM_21;  // beach
	//MyLoc = ROOM_22;  // graveyard
	//MyLoc = ROOM_23;  // field
	//MyLoc = ROOM_24;  // lagoon
	//MyLoc = ROOM_25;  // monastery
	//MyLoc = ROOM_26;  // hell

	while (1) {
		switch (gamestate) {
		case FIRST_LOOK:
			first_look();
			break;
		case TIME_ADVANCE:
			time_advance();
			break;
		case SECOND_LOOK:
			second_look();
			break;
		case USER_INPUT:
			user_input();
			break;
		case USER_ACTION:
			user_action();
			break;
		case LIGHT_FADE:
			light_fade();
			break;
		}
	}
}
'''
		f.write(code)

	def createexehdr(self):
		fname = 'exehdr.s'
		code = '''
;
; Karri Kaksonen, 2022
;
; This header contains data for emulators
;
        .export         __EXEHDR__: absolute = 1

; ------------------------------------------------------------------------
; EXE header
	.segment "EXEHDR"
	.byte	3					; version
	.byte	'A','T','A','R','I','7','8','0','0',' ',' ',' ',' ',' ',' ',' '
	.byte	'P','i','r','a','t','e','s',' ','C','o','v','e',0,0,0,0
	.byte	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	.byte	0,0,$C0,0					; Size
    ;    bit  0 - pokey at 4000
    ;    bit  1 - supergame bank switched
    ;    bit  2 - supergame ram at $4000
    ;    bit  3 - rom at $4000
    ;    bit  4 - bank 6 at $4000
    ;    bit  5 - supergame banked ram
    ;    bit  6 - pokey at $450
    ;    bit  7 - mirror ram at $4000
    ;    bit  8 - activision banking
    ;    bit  9 - absolute banking
    ;    bit 10 - pokey at $440
    ;    bit 11 - ym2151 at $461/462
    ;    bit 12 - souper
    ;    bit 13-15 - Special
    ;   0 = Normal cart
	.byte	0,0					; 0 = Normal cart
	.byte	1					; 1 = Joystick, 2 = lightgun
	.byte	0					; No joystick 2
        .byte	0					; bit0 = 0:NTSC,1:PAL bit1 = 0:component,1:composite
        .byte   0  ; Save data peripheral - 1 byte (version 2)
    ;    0 = None / unknown (default)
    ;    1 = High Score Cart (HSC)
    ;    2 = SaveKey

        .byte	0					; 63   Expansion module
    ;    0 = No expansion module (default on all currently released games)
    ;    1 = Expansion module required
	.byte	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	.byte	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	.byte	0,0,0,0,0,0,0,0
        .byte	'A','C','T','U','A','L',' ','C','A','R','T',' ','D','A','T','A',' ','S','T','A','R','T','S',' ','H','E','R','E'
'''
		with open(fname, 'w') as f:
			f.write(code)

	def createcfg(self):
		fname = self.name + '.cfg'
		code = '''
# Atari VCS 7800 linker configuration file for cc65
# In order to add the a78 header to the build you can add
# "--force-import __EXEHDR__" to the command line

SYMBOLS {
    __STACKSIZE__:        type = weak, value = $0600; # C stack
    __CARTSIZE__:         type = weak, value = $c000;
    __VEC_BOTTOM__:       value = $fffa, type = export;
    __VEC_SIZE__:         value = $6, type = export;
    __ENCRYPT_BOTTOM__:   value = $ff7a, type = export;
    __ENCRYPT_SIZE__:     value = $80, type = export;
    __MEMORY_TOP__:       value = __ENCRYPT_BOTTOM__, type = export;
    __INIT_SIZE__:        value = 187, type = export;
    __MEMORY_INIT__:      value = __MEMORY_TOP__ - __INIT_SIZE__, type = export;
    __MEMORY_BOTTOM__:    value = $10000 - __CARTSIZE__, type = weak;
    __GRAPHICS_SIZE__:    value = 4096, type = export;
    __CODE_BOTTOM__:      value = __MEMORY_BOTTOM__, type = weak;
    __FREE_ROM_SIZE__:    value = __MEMORY_INIT__ - __CODE_BOTTOM__, type = export;
}

MEMORY {
    ZP:     file = "", define = yes, start = $0040, size = $00C0, type = rw;
    SP:     file = "", define = yes, start = $0140, size = $00C0, type = rw;
    RAM1:   file = "", define = yes, start = $1800, size = $0840, type = rw;
    RAM2:   file = "", define = yes, start = $2100, size = $0040, type = rw;
    RAM3:   file = "", define = yes, start = $2200, size = $0600, type = rw;
    # For emulators you also need a header file
    HEADER: file = %O, start = $0000, size = 128;
    # "Normal" cartridge rom. Multiple banks arent supported
    # by this script. You may change the rom size, but keep
    # two things in mind:
    # - start must be a multiple of $1000
    # - ROM must end at $ff79
    TILES:  file = %O, define = yes, start = __MEMORY_BOTTOM__, size = __GRAPHICS_SIZE__, type = ro, fill = yes, fillval = $ff;
    TILES2:  file = %O, define = yes, start = __MEMORY_BOTTOM__+__GRAPHICS_SIZE__, size = __GRAPHICS_SIZE__, type = ro, fill = yes, fillval = $ff;
    ROM:    file = %O, define = yes, start = __CODE_BOTTOM__, size = __FREE_ROM_SIZE__, type = ro, fill = yes, fillval = $ff;
    ROMS:   file = %O, define = yes, start = __MEMORY_INIT__, size = __INIT_SIZE__, type = ro, fill = yes, fillval = $ff;
    # Encryption stuff
    ROME:   file = %O, start = __ENCRYPT_BOTTOM__, size = __ENCRYPT_SIZE__, type = ro, fill = yes, fillval = $ff;
    # Interrupt vectors
    ROMV:   file = %O, start = __VEC_BOTTOM__, size = __VEC_SIZE__, type = ro, fill = yes, fillval = $ff;
}

SEGMENTS {
    ZEROPAGE:   load = ZP,              type = zp;
    EXTZP:      load = ZP,              type = zp;
    EXEHDR:     load = HEADER,          type = ro,  optional = yes;
    STARTUP:    load = ROMS,            type = ro,  define = yes;
    ONCE:       load = ROMS,            type = ro,  define = yes;
    RODATA:     load = ROM,             type = ro,  define = yes, align = 256;
    DATA:       load = ROM, run = RAM1, type = rw,  define = yes;
    GUI:        load = TILES,           type = ro,  optional = yes;
    CODE:       load = ROM,             type = ro,  define = yes;
    BSS:        load = RAM1,            type = bss, define = yes;
    VECTORS:    load = ROMV,            type = ro,  define = yes;
    ENCRYPTION: load = ROME,            type = ro   define = yes;
}

FEATURES {
    CONDES: type    = constructor,
            label   = __CONSTRUCTOR_TABLE__,
            count   = __CONSTRUCTOR_COUNT__,
            segment = ONCE;
    CONDES: type    = destructor,
            label   = __DESTRUCTOR_TABLE__,
            count   = __DESTRUCTOR_COUNT__,
            segment = RODATA;
    CONDES: type    = interruptor,
            label   = __INTERRUPTOR_TABLE__,
            count   = __INTERRUPTOR_COUNT__,
            segment = RODATA,
            import  = __CALLIRQ__;
}
'''
		with open(fname, 'w') as f:
			f.write(code)

	def compilecode(self):
		subprocess.run('cl65 -t atari7800 -C ' + self.name + '.cfg -m ' + self.name + '.map atari7800-mono.o ' + self.name + '.c exehdr.s -o ' + self.name + '.a78', shell=True)
		subprocess.run('sign7800 -w ' + self.name + '.a78', shell=True)

fname = sys.argv[1]
adv = scottfree(fname, 'cc65')
'''adv = scottfree(fname, 'cc7800')'''

'''fname = sys.argv[1] + '.h'
with open(fname, 'w') as f:
	adv.writeheader(f)
	adv.writeincludes(f)
	adv.writevocabheader(f)
	adv.writeitemsheader(f)
	adv.writeroomheader(f)
	adv.writemessagesheader(f)
	adv.writeactionsheader(f)
'''

fname = sys.argv[1] + '.c'
with open(fname, 'w') as f:
	adv.writeheader(f)
	adv.writeincludes(f)
	adv.writevocabheader(f)
	adv.writeitemsheader(f)
	adv.writeroomheader(f)
	adv.writemessagesheader(f)
	adv.writeactionsheader(f)
	adv.writevocabcode(f)
	adv.writeroomcode(f)
	adv.writeitemscode(f)
	adv.writemessagescode(f)
	adv.writeactionscode(f)
	adv.writeinterpreter(f)
	if adv.getcompiler() == 'cc65':
		adv.createcfg()
		adv.createexehdr()

