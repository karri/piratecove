#define ITEMS_TOTAL 67

static const char *const ItemsText[ITEMS_TOTAL] = {
	"Flight of stairs",
	"Open window",
	"Books in a bookcase",
	"Large blood soaked book",
	"Bookcase with secret passage beyond",
	"Pirate's duffel bag",
	"Sign says:\n 'Bring *TREASURES* here, say: SCORE'",
	"Empty bottle",
	"Unlit torch",
	"Lit torch",
	"Matches",
	"Small ship's keel and mast",
	"Wicked looking pirate",
	"Treasure chest",
	"Mongoose",
	"Rusty anchor",
	"Grass shack",
	"Mean and hungry looking crocodiles",
	"Locked door",
	"Open door with hall beyond",
	"Pile of sails",
	"Fish",
	"*COMPASS* embroded with jewels.",
	"Deadly mamba snakes",
	"Parrot",
	"Bottle of rum",
	"Rug",
	"Ring of keys",
	"Open treasure chest",
	"Set of plans",
	"Rug",
	"Claw hammer",
	"Nails",
	"Pile of precut lumber",
	"Tool shed",
	"Locked door",
	"Open door with pit beyond",
	"Pirate ship",
	"Rock wall with narrow crack in it",
	"Narrow crack in the rock",
	"Salt water",
	"Sleeping pirate",
	"Bottle of salt water",
	"Rum bottle smashed into pieces. Sign 'Opposite of LIGHT is Unlight'",
	"Safety sneakers",
	"Map",
	"Shovel",
	"Mouldy old bones",
	"Sand",
	"Bottles of rum",
	"*STAMPS* of great value.",
	"Lagoon",
	"The tide is out",
	"The tide is coming in",
	"Water wings",
	"Flotsam and jetsam",
	"Monastary",
	"Wooden box",
	"Dead squirrel",
	"Sign in the sand says:\n'Private property! Watch out for the tide! Have a nice day!'",
	"Sack of crackers",
	"Note",
	"Small advertising flyer",
	"Burnt out torch",
	"nothing",
	"at",
	"all",
};

static unsigned char ItemsLocation[ITEMS_TOTAL] = {
	1, 2, 2, 0, 0, 4, 1, 0, 4, 0, 0, 6, 9, 9, 8, 24,
	8, 11, 11, 0, 17, 10, 25, 25, 9, 1, 0, 0, 0, 0,
	1, 15, 0, 17, 17, 16, 0, 0, 18, 17, 10, 0, 0, 4,
	1, 0, 15, 0, 6, 0, 0, 6, 24, 0, 15, 0, 23, 0, 0,
	6, 1, 0, 0, 0, 0, 0, 0
};

static const unsigned char const ItemsInitialLoc[ITEMS_TOTAL] = {
	1, 2, 2, 0, 0, 4, 1, 0, 4, 0, 0, 6, 9, 9, 8, 24,
	8, 11, 11, 0, 17, 10, 25, 25, 9, 1, 0, 0, 0, 0,
	1, 15, 0, 17, 17, 16, 0, 0, 18, 17, 10, 0, 0, 4,
	1, 0, 15, 0, 6, 0, 0, 6, 24, 0, 15, 0, 23, 0, 0,
	6, 1, 0, 0, 0, 0, 0, 0
};

static const char *const ItemsAutoGet[ITEMS_TOTAL] = {
	"",
	"",
	"",
	"book",
	"",
	"bag",
	"",
	"bottle",
	"torch",
	"torch",
	"matches",
	"",
	"",
	"chest",
	"mongoose",
	"anchor",
	"",
	"",
	"",
	"",
	"sails",
	"fish",
	"compass",
	"",
	"parrot",
	"bottle",
	"rug",
	"keys",
	"chest",
	"plans",
	"",
	"hammer",
	"nails",
	"lumber",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"bottle",
	"",
	"sneakers",
	"map",
	"shovel",
	"bones",
	"sand",
	"bottles",
	"stamps",
	"",
	"",
	"",
	"wings",
	"",
	"",
	"box",
	"",
	"",
	"crackers",
	"note",
	"flyer",
	"torch",
	"",
	"",
	"all"
};

static int MatchUpItem(const char *text, int loc)
{
	const char const *word = MapSynonym(text);
	int ct = 0;

	if (word == NULL)
		word = text;

	while (ct < ITEMS_TOTAL) {
		if (ItemsAutoGet[ct] && (ItemsLocation[ct] == loc) &&
		    (strncasecmp(ItemsAutoGet[ct], word, WordLen) == 0))
			return (ct);
		ct++;
	}
	return (-1);
}

#if 0
static int MatchUpItemToken(char *text, int loc)
{
	int ct = MapSynonymToken(text);

	if (ct >= 0) {
		if (ItemsAutoGet[ct] && (ItemsLocation[ct] == loc)) {
			return (ct);
		}
	}
	return (-1);
}
#endif

static void OutBuf(const char *buffer);

static void LookItems(unsigned char loc)
{
	unsigned char ct;
	unsigned char f;

	ct = 0;
	f = 0;
	while (ct < ITEMS_TOTAL) {
		if (ItemsLocation[ct] == loc) {
			if (f == 0) {
				OutBuf("I can also see:");
				f++;
			}
			OutBuf("\n - ");
			OutBuf(ItemsText[ct]);
		}
		ct++;
	}
	if (f != 0) {
		OutBuf("\n");
	}
}
