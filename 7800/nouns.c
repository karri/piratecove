#define NOUNS_TOTAL WORDS+5

#define NOUN_ANY 0
#define NOUN_NORTH 1
#define NOUN_SOUTH 2
#define NOUN_EAST 3
#define NOUN_WEST 4
#define NOUN_UP 5
#define NOUN_DOWN 6

#define NOUN_PASSAGE 8
#define NOUN_HALLWAY 9
#define NOUN_BOOK 10
#define NOUN_BOTTLE 11
#define NOUN_RUM 12
#define NOUN_WINDOW 13
#define NOUN_GAME 14
#define NOUN_MONGOOSE 15
#define NOUN_PIRATE 16
#define NOUN_AROUND 17
#define NOUN_BAG 18
#define NOUN_DUFFEL 19
#define NOUN_TORCH 20
#define NOUN_OFF 21
#define NOUN_MATCHES 22
#define NOUN_YOHO 23
#define NOUN_30 24
#define NOUN_LUMBER 25
#define NOUN_RUG 26
#define NOUN_KEY 27
#define NOUN_INVENTORY 28
#define NOUN_COMPASS 29
#define NOUN_SAIL 30
#define NOUN_FISH 31
#define NOUN_ANCHOR 32
#define NOUN_SHACK 33
#define NOUN_PLANS 34
#define NOUN_CAVE 35
#define NOUN_SIGN 36
#define NOUN_DOOR 37
#define NOUN_CHEST 38
#define NOUN_PARROT 39
#define NOUN_HAMMER 40
#define NOUN_NAILS 41
#define NOUN_BOAT 42
#define NOUN_SHIP 43
#define NOUN_SHED 44
#define NOUN_CRACK 45
#define NOUN_WATER 46
#define NOUN_SALT 47
#define NOUN_LAGOON 48
#define NOUN_TIDE 49
#define NOUN_PIT 50
#define NOUN_SHOVEL 51
#define NOUN_BEACH 52
#define NOUN_MAP 53
#define NOUN_PAC 54
#define NOUN_BONES 55
#define NOUN_HOLE 56
#define NOUN_SAND 57
#define NOUN_BOX 58
#define NOUN_SNEAKERS 59
#define NOUN_CRACKERS 60
#define NOUN_SACK 61
#define NOUN_PIECES 62
#define NOUN_KEEL 63
#define NOUN_FLOTSAM 64
#define NOUN_JETSAM 65
#define NOUN_STAIRS 66
#define NOUN_UPSTAIRS 67
#define NOUN_PATH 68
#define NOUN_HILL 69

#define NOUN_AWAKE 71
#define NOUN_BUN 72

#define NOUN_NOTE 74
#define NOUN_FLYER 75
#define NOUN_DESTRUCTIVE 76
#define NOUN_CROCODILES 77
#define NOUN_SNAKES 78
#define NOUN_TREASURE 79
#define NOUN_WINGS 80
#define NOUN_MONASTARY 81
#define NOUN_SHORE 82
#define NOUN_STAMPS 83
#define NOUN_DUBLOONS 84

static const char *const Nouns[NOUNS_TOTAL] = {
	"any",
	"north",
	"south",
	"east",
	"west",
	"up",
	"down",
	".",
	"passage",
	"hallway",
	"book",
	"bottle",
	"*rum",
	"window",
	"game",
	"mongoose",
	"pirate",
	"around",
	"bag",
	"*duffel bag",
	"torch",
	"off",
	"matches",
	"yoho",
	"30",
	"lumber",
	"rug",
	"key",
	"inventory",
	"compass",
	"sails",
	"fish",
	"anchor",
	"shack",
	"plans",
	"cave",
	"sign",
	"door",
	"chest",
	"parrot",
	"hammer",
	"nails",
	"boat",
	"*ship",
	"shed",
	"crack",
	"water",
	"*salt water",
	"lagoon",
	"*tide",
	"pit",
	"shovel",
	"*beach",
	"map",
	"pace",
	"bones",
	"hole",
	"sand",
	"box",
	"sneakers",
	"crackers",
	"*sack",
	"pieces",
	"keel",
	"flotsam",
	"*jetsam",
	"stairs",
	"*upstairs",
	"path",
	"*hill",
	"yoho",
	"awake",
	"*bun",
	"pieces",
	"note",
	"flyer",
	"destructive",
	"crocodiles",
	"snakes",
	"treasure",
	"wings",
	"monastary",
	"shore",
	"stamps",
	"dubloons"
};

static const char const NounsSorted[NOUNS_TOTAL] = {
	NOUN_30,
	NOUN_ANCHOR,
	NOUN_ANY,
	NOUN_AROUND,
	NOUN_AWAKE,
	NOUN_BAG,
	NOUN_BEACH,
	NOUN_BOAT,
	NOUN_BONES,
	NOUN_BOOK,
	NOUN_BOTTLE,
	NOUN_BOX,
	NOUN_BUN,
	NOUN_CAVE,
	NOUN_CHEST,
	NOUN_COMPASS,
	NOUN_CRACK,
	NOUN_CRACKERS,
	NOUN_CROCODILES,
	NOUN_DESTRUCTIVE,
	NOUN_DOOR,
	NOUN_DOWN,
	NOUN_DUBLOONS,
	NOUN_DUFFEL,
	NOUN_EAST,
	NOUN_FISH,
	NOUN_FLOTSAM,
	NOUN_FLYER,
	NOUN_GAME,
	NOUN_HALLWAY,
	NOUN_HAMMER,
	NOUN_HILL,
	NOUN_HOLE,
	NOUN_INVENTORY,
	NOUN_JETSAM,
	NOUN_KEEL,
	NOUN_KEY,
	NOUN_LAGOON,
	NOUN_LUMBER,
	NOUN_MAP,
	NOUN_MATCHES,
	NOUN_MONASTARY,
	NOUN_MONGOOSE,
	NOUN_NAILS,
	NOUN_NORTH,
	NOUN_NOTE,
	NOUN_OFF,
	NOUN_PAC,
	NOUN_PARROT,
	NOUN_PASSAGE,
	NOUN_PATH,
	NOUN_PIECES,
	73,
	NOUN_PIRATE,
	NOUN_PIT,
	NOUN_PLANS,
	NOUN_RUG,
	NOUN_RUM,
	NOUN_SACK,
	NOUN_SAIL,
	NOUN_SALT,
	NOUN_SAND,
	NOUN_SHACK,
	NOUN_SHED,
	NOUN_SHIP,
	NOUN_SHORE,
	NOUN_SHOVEL,
	NOUN_SIGN,
	NOUN_SNAKES,
	NOUN_SNEAKERS,
	NOUN_SOUTH,
	NOUN_STAIRS,
	NOUN_STAMPS,
	NOUN_TIDE,
	NOUN_TORCH,
	NOUN_TREASURE,
	NOUN_UP,
	NOUN_UPSTAIRS,
	NOUN_WATER,
	NOUN_WEST,
	NOUN_WINDOW,
	NOUN_WINGS,
	NOUN_YOHO,
	70
};

// Base noun, *synonyms
static const char *const MapSynonym(const char *word)
{
	int n = 1;
	const char *tp;
	static char lastword[16];	/* Last non synonym */

	if (*word == '*')
		word++;
	while (n < NOUNS_TOTAL) {
		tp = Nouns[n];
		if (*tp == '*')
			tp++;
		else
			strcpy(lastword, tp);
		if (strncasecmp(word, tp, WordLen) == 0)
			return (lastword);
		n++;
	}
	return 0;
}

#if 0
static int MapSynonymToken(char *word)
{
	int n = 1;
	int ret = -1;
	const char *tp;

	if (*word == '*')
		word++;
	while (n < NOUNS_TOTAL) {
		tp = Nouns[n];
		if (*tp == '*')
			tp++;
		else
			ret = n;
		if (strncasecmp(word, tp, WordLen) == 0)
			return ret;
		n++;
	}
	return ret;
}
#endif
