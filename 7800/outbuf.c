static void OutReset()
{
	gotoxy(0, TOP_HEIGHT + STORY_LINES - 1);
	memset(screen + (TOP_HEIGHT + STORY_LINES - 1) * LINE_LEN, 0, LINE_LEN);
}

static void scroll()
{
	clock_t w;
	memmove(screen + TOP_HEIGHT * LINE_LEN, screen + (TOP_HEIGHT + 1) * LINE_LEN, STORY_LINES * LINE_LEN);
	OutReset();
	w = clock() + 10;
	while (clock() < w) ;
}

static void printword(const char * buf, unsigned char len) {
	unsigned char i;
	for (i = 0; i < len; i++) {
		unsigned char ch;
		unsigned char changefont = 255;
		ch = buf[i];
#ifndef MODE320
		if ((ch >= 'a') && (ch <= 'z') ||
			(ch == ':') ||
			(ch == '!') ||
			(ch == '\'')||
			(ch == '&')) {
			changefont = textcolor(0);
			if (changefont == 1) {
				textcolor(changefont);
				changefont = 255;
			}
			switch (ch) {
			case ':':
				ch = '.';
				break;
			case '!':
				ch = '?';
				break;
			case '\'':
				ch = ',';
				break;
			case '&':
				ch = '*';
				break;
			}
		}
#endif
		if (wherey() > TOP_HEIGHT + STORY_LINES - 1) {
			scroll();
		}
		cputc(ch);
		if (changefont != 255) {
			textcolor(changefont);
		}
	}
}

static unsigned char nextwordlen(char *buf) {
	unsigned char wp;
	wp = 0;
	while (buf[wp] && (buf[wp] != ' ') && (buf[wp] != '\n')) {
		wp++;
	}
	return wp;
}

static void OutBuf(const char *buffer)
{
	unsigned char wp;

	while (*buffer) {
		if (*buffer == 0) {
			return;
		}
		if (*buffer == '\n') {
			scroll();
			buffer++;
		} else {
			wp = nextwordlen(buffer);
			if ((wherex() + wp) > LINE_LEN) {
				scroll();
			} else {
				printword(buffer, wp);
				buffer += wp;
				if (buffer[0] == '\n') {
					scroll();
				} else {
					cputc(' ');
				}
				if (*buffer == 0) {
					return;
				}
				buffer++;
			}
		}
	}
}
