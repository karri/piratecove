class Player:
    def __init__(self):
        self.oname = 'player.s'
        self.of = open(self.oname, 'w')
        self.iname = 'Track_data.asm'
        self.inf = open(self.iname, 'r')

    def part1(self):
        code = '''.include "atari7800.inc"
.export _tt_wait
.constructor _tia_init
.interruptor _Player
.import _zonecounter
.import _paldetected
.include "extzp.inc"
.export _sub_start_song

NUM_AUDIO_CHANNELS = 2

.segment "ZEROPAGE" :zeropage
; ----------------------------------
; variables
_tt_wait:            .res 1
framecounter:        .res 1
audio_song:          .res 1  ; what song are we on
audio_song_ptr:      .res 2  ; address of song
audio_song_order:    .res 1  ; what order are we at in the song
audio_row_idx:       .res 1  ; where are we in the current order
audio_pattern_idx:   .res 2  ; which pattern is playing on each channel
audio_pattern_ptr:   .res 2
audio_waveform_idx:  .res 2  ; where are we in waveform on each channel
audio_waveform_ptr:  .res 2
audio_timer:         .res 2  ; time left on next action on each channel

audio_registers:
audio_cx = AUDC0
audio_fx = AUDF0 
audio_vx = AUDV0
audio_end_registers:
.code
'''
        self.of.write(code)

    def part2(self):
        code = '''_tia_init:
	lda #1
	sta _tt_wait
	lda #0
        sta framecounter
	sta audio_song
	sta audio_song_ptr
	sta audio_song_ptr+1
	sta audio_song_order
	sta audio_row_idx
	sta audio_pattern_idx
	sta audio_pattern_idx+1
	sta audio_pattern_ptr
	sta audio_pattern_ptr+1
	sta audio_waveform_idx
	sta audio_waveform_idx+1
	sta audio_waveform_ptr
	sta audio_waveform_ptr+1
	sta audio_timer
	sta audio_timer+1
	rts

_sub_start_song:
        tay
        lda #1
        sta _tt_wait
        lda SONG_TABLE_START_LO,y
        sta audio_song_ptr
        lda SONG_TABLE_START_HI,y
        sta audio_song_ptr + 1
        ldy #0
        ldx #(audio_end_registers - audio_row_idx - 1)
_song_clean_loop:
        sty audio_pattern_idx,x
        dex
        bpl _song_clean_loop
        lda (audio_song_ptr),y
        sta audio_pattern_idx
        iny
        lda (audio_song_ptr),y
        sta audio_pattern_idx+1
        iny
        sty audio_song_order
        rts

_Player:
	lda _zonecounter
	and #01
	bne @L0
	lda _tt_wait
	beq @L1
@L0:
	clc
	rts
@L1:
        ;lda _paldetected
        ;beq _PlayerLoop
        ;lda framecounter
        ;clc
        ;adc #1
        ;sta framecounter
        ;cmp #5
        ;bne _PlayerLoop
        ;lda #0
        ;sta framecounter
        ;jsr _PlayerLoop
_PlayerLoop:
            ldx #NUM_AUDIO_CHANNELS - 1
            lda sfxptr+1
            bne _sfxsound
            jmp _audio_loop
_sfxsound:
            ldy audio_timer,x
            beq _sfxaudio_next_note
            dey
            sty audio_timer,x
            jmp _audio_next_channel

_sfxaudio_next_note:
            ldy audio_pattern_idx,x 
            lda PAT_TABLE_START_LO,y
            sta audio_pattern_ptr
            lda PAT_TABLE_START_HI,y
            sta audio_pattern_ptr + 1
            ldy audio_row_idx
            lda (audio_pattern_ptr),y
            tay                       ; y is now waveform ptr
            lda WF_TABLE_START_LO,y
            sta audio_waveform_ptr
            lda WF_TABLE_START_HI,y
            sta audio_waveform_ptr + 1
            ldy audio_waveform_idx,x
            lda (audio_waveform_ptr),y
            beq _sfxaudio_advance_tracker ; check for zero 
            lsr                        ; pull first bit
            bcc _sfxset_registers         ; if set go to load registers
            lsr                        ; check second bit
            bcc _sfxset_cx_vx             ; if clear we are loading aud(c|v)x
            lsr                        ; pull duration bit for later set
            jmp _sfxset_timer_delta       ; jump to duration 
_sfxset_cx_vx: lsr
            bcc _sfxset_vx
            lsr
            jmp _sfxset_timer_delta       ; jump to duration
_sfxset_vx:
            lsr
_sfxset_timer_delta:
            lda #0
            adc #0                     ; will be 0 or 1 based on carry bit
            sta audio_timer,x
            jmp _sfxaudio_advance_waveform

_sfxset_registers:
            ; processing all registers
            pha                        ; save timer
            lsr
            lsr
            iny
            pla                      
            and #$03
            lsr
            bcs _sfxset_timer_registers
            lda (audio_waveform_ptr),y
            iny
_sfxset_timer_registers:
            sta audio_timer,x
            lda (audio_waveform_ptr),y
            lsr
            lsr
            lsr
            lsr
            lda (audio_waveform_ptr),y
            and #$0f

_sfxaudio_advance_waveform:
            iny
            sty audio_waveform_idx,x
            jmp _audio_next_channel
_sfxaudio_advance_tracker: ; got a 0 on waveform
            lda #255
            sta audio_timer,x
            sta audio_waveform_idx,x
_sfxaudio_next_channel:
            dex
_audio_loop:
            ldy audio_timer,x
            beq _audio_next_note
            dey
            sty audio_timer,x
            jmp _audio_next_channel

_audio_next_note:
            ldy audio_pattern_idx,x 
            lda PAT_TABLE_START_LO,y
            sta audio_pattern_ptr
            lda PAT_TABLE_START_HI,y
            sta audio_pattern_ptr + 1
            ldy audio_row_idx
            lda (audio_pattern_ptr),y
            tay                       ; y is now waveform ptr
            lda WF_TABLE_START_LO,y
            sta audio_waveform_ptr
            lda WF_TABLE_START_HI,y
            sta audio_waveform_ptr + 1
            ldy audio_waveform_idx,x
            lda (audio_waveform_ptr),y
            beq _audio_advance_tracker ; check for zero 
            lsr                        ; pull first bit
            bcc _set_registers         ; if set go to load registers
            lsr                        ; check second bit
            bcc _set_cx_vx             ; if clear we are loading aud(c|v)x
            lsr                        ; pull duration bit for later set
            sta audio_fx,x             ; store frequency
            jmp _set_timer_delta       ; jump to duration 
_set_cx_vx: lsr
            bcc _set_vx
            lsr
            sta audio_cx,x
            jmp _set_timer_delta       ; jump to duration
_set_vx:
            lsr
            sta audio_vx,x
_set_timer_delta:
            lda #0
            adc #0                     ; will be 0 or 1 based on carry bit
            sta audio_timer,x
            jmp _audio_advance_waveform

_set_registers:
            ; processing all registers
            pha                        ; save timer
            lsr
            lsr
            sta audio_fx,x
            iny
            pla                      
            and #$03
            lsr
            bcs _set_timer_registers
            lda (audio_waveform_ptr),y
            iny
_set_timer_registers:
            sta audio_timer,x
            lda (audio_waveform_ptr),y
            lsr
            lsr
            lsr
            lsr
            sta audio_cx,x
            lda (audio_waveform_ptr),y
            and #$0f
            sta audio_vx,x

_audio_advance_waveform:
            iny
            sty audio_waveform_idx,x
            jmp _audio_next_channel
_audio_advance_tracker: ; got a 0 on waveform
            lda #255
            sta audio_timer,x
            sta audio_waveform_idx,x
_audio_next_channel:
            dex
            bpl _audio_loop

            ; update track - check if both waveforms done
            lda audio_waveform_idx
            and audio_waveform_idx+1
            cmp #255
            bne _audio_end            
            lda #0
            sta audio_timer
            sta audio_timer+1
            sta audio_waveform_idx
            sta audio_waveform_idx+1
            ldy audio_row_idx
            iny
            lda (audio_pattern_ptr),y
            cmp #255
            beq _audio_advance_order
            sty audio_row_idx
            jmp _PlayerLoop; if not 255 loop back 
_audio_advance_order: ; got a 255 on pattern
            lda #0
            sta audio_row_idx
            ldy audio_song_order
            lda (audio_song_ptr),y
            cmp #255
            bne _audio_advance_order_advance_pattern
            ldy #0
            lda (audio_song_ptr),y
_audio_advance_order_advance_pattern:
            sta audio_pattern_idx
            iny
            lda (audio_song_ptr),y
            sta audio_pattern_idx+1
            iny
            sty audio_song_order
            jmp _PlayerLoop;  loop back 
_audio_end:
            clc
            rts
'''
        self.of.write(code)

    def part3(self):
        code = '''_tia_init:
	lda #1
	sta _tt_wait
	lda #0
        sta framecounter
	sta audio_song
	sta audio_song_ptr
	sta audio_song_ptr+1
	sta audio_song_order
	sta audio_row_idx
	sta audio_pattern_idx
	sta audio_pattern_idx+1
	sta audio_pattern_ptr
	sta audio_pattern_ptr+1
	sta audio_waveform_idx
	sta audio_waveform_idx+1
	sta audio_waveform_ptr
	sta audio_waveform_ptr+1
	sta audio_timer
	sta audio_timer+1
	rts

_sub_start_song:
        tay
        lda #1
        sta _tt_wait
        lda SONG_TABLE_START_LO,y
        sta audio_song_ptr
        lda SONG_TABLE_START_HI,y
        sta audio_song_ptr + 1
        ldy #0
        ldx #(audio_end_registers - audio_row_idx - 1)
_song_clean_loop:
        sty audio_pattern_idx,x
        dex
        bpl _song_clean_loop
        lda (audio_song_ptr),y
        sta audio_pattern_idx
        iny
        lda (audio_song_ptr),y
        sta audio_pattern_idx+1
        iny
        sty audio_song_order
        rts

_Player:
	lda _zonecounter
	and #01
	bne @L0
	lda _tt_wait
	beq @L1
@L0:
	clc
	rts
@L1:
        lda _paldetected
        beq _PlayerLoop
        lda framecounter
        clc
        adc #1
        sta framecounter
        cmp #5
        bne _PlayerLoop
        lda #0
        sta framecounter
        jsr _PlayerLoop
_PlayerLoop:
            ldx #1 ; loop over both audio channels
            lda sfxptr+1
            bne _sfxsound
            jmp _audio_loop
_sfxsound:
            ldy audio_timer,x
            beq _sfxaudio_next_note
            dey
            sty audio_timer,x
            jmp _sfxaudio_next_channel
_sfxaudio_next_note:
            ldy audio_pattern_idx,x 
            lda PAT_TABLE_START_LO,y
            sta audio_pattern_ptr
            lda PAT_TABLE_START_HI,y
            sta audio_pattern_ptr + 1
            ldy audio_row_idx
            lda (audio_pattern_ptr),y
            tay                       ; y is now waveform ptr
            lda WF_TABLE_START_LO,y
            sta audio_waveform_ptr
            lda WF_TABLE_START_HI,y
            sta audio_waveform_ptr + 1
            ldy audio_waveform_idx,x
            lda (audio_waveform_ptr),y
            beq _sfxaudio_advance_tracker ; check for zero 
            lsr                        ; .......|C pull first bit
            bcc _sfxset_all_registers     ; .......|? if clear go to load all registers
            lsr                        ; 0......|C1 pull second bit
            bcc _sfxset_cx_vx             ; 0......|?1 if clear we are loading aud(c|v)x
            lsr                        ; 00fffff|C11 pull duration bit for later set
            bpl _sfxset_timer_delta       ; jump to duration (note: should always be positive)
_sfxset_cx_vx:
            lsr                        ; 00.....|C01
            bcc _sfxset_vx                ; 00.....|?01  
            lsr                        ; 000cccc|C101
            bpl _sfxset_timer_delta       ; jump to duration (note: should always be positive)
_sfxset_vx:
            lsr                        ; 000vvvv|C001
_sfxset_timer_delta:
            rol audio_timer,x          ; set new timer to 0 or 1 depending on carry bit
            bpl _sfxaudio_advance_note    ; done (note: should always be positive)
_sfxset_all_registers:
            ; processing all registers
            lsr                        ; 00......|C0
            bcc _sfxset_suspause          ; 00......|?0 if clear we are suspausing
            lsr                        ; 0000fffff|C10 pull duration bit
            rol audio_timer,x          ; set new timer to 0 or 1 depending on carry bit
            iny                        ; advance 1 byte
            lda (audio_waveform_ptr),y ; ccccvvvv|
            lsr                        ; 0ccccvvv|
            lsr                        ; 00ccccvv|
            lsr                        ; 000ccccv|
            lsr                        ; 0000cccc|
            bpl _sfxaudio_advance_note    ; done (note: should always be positive)
_sfxset_suspause:
            lsr                        ; 000ddddd|C00 pull bit 3 (reserved)
            sta audio_timer,x          ; store timer
            bcs _sfxaudio_advance_note    ; if set we sustain
            lda #0
            sta audio_vx,x             ; clear volume
_sfxaudio_advance_note:
            iny
            sty audio_waveform_idx,x
            jmp _sfxaudio_next_channel
_sfxaudio_advance_tracker:
            ; got a 0 on waveform
            lda #255
            sta audio_timer,x
            sta audio_waveform_idx,x
_sfxaudio_next_channel:
            dex
_audio_loop:
            ldy audio_timer,x
            beq _audio_next_note
            dey
            sty audio_timer,x
            jmp _audio_next_channel
_audio_next_note:
            ldy audio_pattern_idx,x 
            lda PAT_TABLE_START_LO,y
            sta audio_pattern_ptr
            lda PAT_TABLE_START_HI,y
            sta audio_pattern_ptr + 1
            ldy audio_row_idx
            lda (audio_pattern_ptr),y
            tay                       ; y is now waveform ptr
            lda WF_TABLE_START_LO,y
            sta audio_waveform_ptr
            lda WF_TABLE_START_HI,y
            sta audio_waveform_ptr + 1
            ldy audio_waveform_idx,x
            lda (audio_waveform_ptr),y
            beq _audio_advance_tracker ; check for zero 
            lsr                        ; .......|C pull first bit
            bcc _set_all_registers     ; .......|? if clear go to load all registers
            lsr                        ; 0......|C1 pull second bit
            bcc _set_cx_vx             ; 0......|?1 if clear we are loading aud(c|v)x
            lsr                        ; 00fffff|C11 pull duration bit for later set
            sta audio_fx,x             ; store frequency
            bpl _set_timer_delta       ; jump to duration (note: should always be positive)
_set_cx_vx:
            lsr                        ; 00.....|C01
            bcc _set_vx                ; 00.....|?01  
            lsr                        ; 000cccc|C101
            sta audio_cx,x             ; store control
            bpl _set_timer_delta       ; jump to duration (note: should always be positive)
_set_vx:
            lsr                        ; 000vvvv|C001
            sta audio_vx,x             ; store volume
_set_timer_delta:
            rol audio_timer,x          ; set new timer to 0 or 1 depending on carry bit
            bpl _audio_advance_note    ; done (note: should always be positive)
_set_all_registers:
            ; processing all registers
            lsr                        ; 00......|C0
            bcc _set_suspause          ; 00......|?0 if clear we are suspausing
            lsr                        ; 0000fffff|C10 pull duration bit
            sta audio_fx,x             ; store frequency
            rol audio_timer,x          ; set new timer to 0 or 1 depending on carry bit
            iny                        ; advance 1 byte
            lda (audio_waveform_ptr),y ; ccccvvvv|
            sta audio_vx,x             ; store volume
            lsr                        ; 0ccccvvv|
            lsr                        ; 00ccccvv|
            lsr                        ; 000ccccv|
            lsr                        ; 0000cccc|
            sta audio_cx,x             ; store control
            bpl _audio_advance_note    ; done (note: should always be positive)
_set_suspause:
            lsr                        ; 000ddddd|C00 pull bit 3 (reserved)
            sta audio_timer,x          ; store timer
            bcs _audio_advance_note    ; if set we sustain
            lda #0
            sta audio_vx,x             ; clear volume
_audio_advance_note:
            iny
            sty audio_waveform_idx,x
            jmp _audio_next_channel
_audio_advance_tracker:
            ; got a 0 on waveform
            lda #255
            sta audio_timer,x
            sta audio_waveform_idx,x
_audio_next_channel:
            dex
            bpl _audio_loop

            ; update track - check if both waveforms done
            lda audio_waveform_idx
            and audio_waveform_idx+1
            cmp #255
            bne _audio_end            
            lda #0
            sta audio_timer
            sta audio_timer+1
            sta audio_waveform_idx
            sta audio_waveform_idx+1
            ldy audio_row_idx
            iny
            lda (audio_pattern_ptr),y
            cmp #255
            beq _audio_advance_order
            sty audio_row_idx
            jmp _PlayerLoop ; if not 255 loop back 
_audio_advance_order:
            ; got a 255 on pattern
            lda #0
            sta audio_row_idx
            ldy audio_song_order
            lda (audio_song_ptr),y
            cmp #255
            bne _audio_advance_order_advance_pattern
            ldy #0
            lda (audio_song_ptr),y
_audio_advance_order_advance_pattern:
            sta audio_pattern_idx
            iny
            lda (audio_song_ptr),y
            sta audio_pattern_idx+1
            iny
            sty audio_song_order
            jmp _PlayerLoop ;  loop back 
_audio_end:
            clc
            rts
'''
        self.of.write(code)

    def parse(self):
        data = self.inf.readlines()
        for l in data:
            d = l.rstrip()
            if d.strip() == 'SONG_TABLE_START_LO':
                d = d.replace('SONG_TABLE_START_LO', 'SONG_TABLE_START_LO:')
            if d.strip() == 'SONG_TABLE_START_HI':
                d = d.replace('SONG_TABLE_START_HI', 'SONG_TABLE_START_HI:')
            if d.strip() == 'SONG_0_ADDR':
                d = d.replace('SONG_0_ADDR', 'SONG_0_ADDR:')
            if d.strip() == 'SONG_1_ADDR':
                d = d.replace('SONG_1_ADDR', 'SONG_1_ADDR:')
            d = d.replace(' . - ', ' * - ')
            d = d.replace('byte', '.byte')
            d = d.replace('#include', ';#include')
            if d.strip() == 'PAT_TABLE_START_LO':
                d = d.replace('PAT_TABLE_START_LO', 'PAT_TABLE_START_LO:')
            if d.strip() == 'PAT_TABLE_START_HI':
                d = d.replace('PAT_TABLE_START_HI', 'PAT_TABLE_START_HI:')
            if d[:5] == 'PAT_S':
                d = d.replace('ADDR', 'ADDR:')
            if d.strip() == 'WF_TABLE_START_LO':
                d = d.replace('WF_TABLE_START_LO', 'WF_TABLE_START_LO:')
            if d.strip() == 'WF_TABLE_START_HI':
                d = d.replace('WF_TABLE_START_HI', 'WF_TABLE_START_HI:')
            if d[:5] == 'SEQ_S':
                d = d.replace('ADDR', 'ADDR:')
            d = d + '\n'
            self.of.write(d)

    def close(self):
        self.inf.close()
        self.of.close()

p = Player()
p.part1()
p.parse()
''' p.part2() '''
p.part3()
p.close()

