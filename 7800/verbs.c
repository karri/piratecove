#define WORDS 80

#define VERBS_TOTAL 83

#define VERB_GO 1
#define VERB_CLIMB 2
#define VERB_WALK 3
#define VERB_RUN 4
#define VERB_ENTER 5
#define VERB_PACE 6
#define VERB_FOLLOW 7
#define VERB_SAY 8
#define VERB_SAIL 9
#define VERB_GET 10
#define VERB_TAKE 11
#define VERB_CATCH 12
#define VERB_PICK 13
#define VERB_REMOVE 14
#define VERB_WEAR 15
#define VERB_PULL 16
#define VERB_FLY 17
#define VERB_DROP 18
#define VERB_RELEASE 19
#define VERB_THROW 20
#define VERB_LEAVE 21
#define VERB_GIVE 22
#define VERB_DRINK 23
#define VERB_EAT 24
#define VERB_INVENTORY 25

#define VERB_LOOK 27
#define VERB_EXAMINE 28
#define VERB_WATCH 29
#define VERB_READ 30
#define VERB_LISTEN 31

#define VERB_SCORE 33
#define VERB_SAVE 34
#define VERB_KILL 35
#define VERB_ATTACK 36
#define VERB_LIGHT 37

#define VERB_OPEN 39
#define VERB_SHAKE 40
#define VERB_UNLIGHT 41
#define VERB_HELP 42

#define VERB_SWING 45
#define VERB_QUIT 46
#define VERB_BUILD 47
#define VERB_MAKE 48
#define VERB_WAKE 49
#define VERB_SET 50
#define VERB_CAST 51
#define VERB_DIG 52
#define VERB_BURN 53
#define VERB_FIND 54
#define VERB_JUMP 55
#define VERB_EMPTY 56
#define VERB_WEIGH 57

#define VERB_BREAK 59
#define VERB_SMASH 60

#define VERB_WAIT 63
#define VERB_FEED 64

#define VERB_CLOSE 67
#define VERB_SHUT 68
#define VERB_UNLOCK 80
#define VERB_FEEL 81
#define VERB_LOAD 82

static const char *const Verbs[VERBS_TOTAL] = {
	"auto",
	"go",
	"*climb",
	"*walk",
	"*run",
	"*enter",
	"*pace",
	"*follow",
	"say",
	"sail",
	"get",
	"*take",
	"*catch",
	"*pick",
	"*remove",
	"*wear",
	"*pull",
	"fly",
	"drop",
	"*release",
	"*throw",
	"*leave",
	"*give",
	"drink",
	"*eat",
	"inventory",
	"sail",
	"look",
	"*examine",
	"*watch",
	"read",
	"listen",
	".",
	"score",
	"save",
	"kill",
	"*attack",
	"light",
	".",
	"open",
	"*shake",
	"unlight",
	"help",
	".",
	".",
	"swing",
	"quit",
	"build",
	"*make",
	"wake",
	"set",
	"cast",
	"dig",
	"burn",
	"find",
	"jump",
	"empty",
	"weigh",
	"",
	"break",
	"*smash",
	".",
	"",
	"wait",
	"feed",
	"",
	"",
	"close",
	"*shut",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"unlock",
	"feel",
	"load"
};

static const char const VerbsSorted[62] = {
	VERB_ATTACK,
	VERB_BREAK,
	VERB_BUILD,
	VERB_BURN,
	VERB_CAST,
	VERB_CATCH,
	VERB_CLIMB,
	VERB_CLOSE,
	VERB_DIG,
	VERB_DRINK,
	VERB_DROP,
	VERB_EAT,
	VERB_EMPTY,
	VERB_ENTER,
	VERB_EXAMINE,
	VERB_FEED,
	VERB_FEEL,
	VERB_FIND,
	VERB_FLY,
	VERB_FOLLOW,
	VERB_GET,
	VERB_GIVE,
	VERB_GO,
	VERB_HELP,
	VERB_INVENTORY,
	VERB_JUMP,
	VERB_KILL,
	VERB_LEAVE,
	VERB_LIGHT,
	VERB_LISTEN,
	VERB_LOAD,
	VERB_LOOK,
	VERB_MAKE,
	VERB_OPEN,
	VERB_PACE,
	VERB_PICK,
	VERB_PULL,
	VERB_QUIT,
	VERB_READ,
	VERB_RELEASE,
	VERB_REMOVE,
	VERB_RUN,
	26,
	VERB_SAIL,
	VERB_SAVE,
	VERB_SAY,
	VERB_SCORE,
	VERB_SET,
	VERB_SHAKE,
	VERB_SHUT,
	VERB_SMASH,
	VERB_SWING,
	VERB_TAKE,
	VERB_THROW,
	VERB_UNLIGHT,
	VERB_UNLOCK,
	VERB_WAIT,
	VERB_WAKE,
	VERB_WALK,
	VERB_WATCH,
	VERB_WEAR,
	VERB_WEIGH
};
