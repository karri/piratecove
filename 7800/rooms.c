#define ROOMS_TOTAL 27

#define ROOM_NONE 0
#define ROOM_FLAT 1
#define ROOM_ALCOVE 2
#define ROOM_PASSAGEWAY 3
#define ROOM_ATTIC 4
#define ROOM_LEDGE 5
#define ROOM_BEACH 6
#define ROOM_MAZE1 7
#define ROOM_MEADOW 8
#define ROOM_SHACK 9
#define ROOM_OCEAN 10
#define ROOM_PIT 11
#define ROOM_MAZE2 12
#define ROOM_MAZE3 13
#define ROOM_HILL 14
#define ROOM_SHED 15
#define ROOM_HALLWAY 16
#define ROOM_CAVERN 17
#define ROOM_HILLTOP 18
#define ROOM_MAZE4 19
#define ROOM_SHIP 20
#define ROOM_MUERTA 21
#define ROOM_GRAVEYARD 22
#define ROOM_FIELD 23
#define ROOM_LAGOON 24
#define ROOM_MONASTERY 25
#define ROOM_NEVERLAND 26
#define ROOM_NOLIGHT 27
#define ROOM_TIDE 28
static const Room const Rooms[ROOMS_TOTAL] = {
	{{0, 0, 0, 0, 0, 0}, ""},
	{{0, 0, 0, 0, 0, 0}, "flat in London"},
	{{0, 0, 0, 0, 0, 1}, "alcove"},
	{{0, 0, 4, 2, 0, 0}, "secret passageway"},
	{{0, 0, 0, 3, 0, 0}, "musty attic"},
	{{0, 0, 0, 0, 0, 0},
	 "*I'm outside an open window on the ledge of a very tall building"},
	{{0, 0, 8, 0, 0, 0}, "sandy beach on a tropical isle"},
	{{0, 12, 13, 14, 0, 11}, "maze of caves"},
	{{0, 0, 14, 6, 0, 0}, "meadow"},
	{{0, 0, 0, 8, 0, 0}, "grass shack"},
	{{10, 24, 10, 10, 0, 0}, "*I'm in the ocean"},
	{{0, 0, 0, 0, 7, 0}, "pit"},
	{{7, 0, 14, 13, 0, 0}, "maze of caves"},
	{{7, 14, 12, 19, 0, 0}, "maze of caves"},
	{{0, 0, 0, 8, 0, 0},
	 "*I'm at the foot of a cave ridden hill, a pathway leads on up to the top"},
	{{17, 0, 0, 0, 0, 0}, "tool shed"},
	{{0, 0, 17, 0, 0, 0}, "long hallway"},
	{{0, 0, 0, 16, 0, 0}, "large cavern"},
	{{0, 0, 0, 0, 0, 14},
	 "*I'm on top of a hill. Below is Tortuga. Across the sea way off in the distance I see the horizon."},
	{{0, 14, 14, 13, 0, 0}, "maze of caves"},
	{{0, 0, 0, 0, 0, 0}, "*I'm aboard a Pirate ship anchored off shore"},
	{{0, 22, 0, 0, 0, 0}, "*I'm on the beach at Isla de Muerta"},
	{{21, 0, 23, 0, 0, 0},
	 "spooky old graveyard filled with piles of empty and broken rum bottles"},
	{{0, 0, 0, 22, 0, 0}, "large barren field"},
	{{10, 6, 6, 6, 0, 0}, "shallow lagoon. To the north is the ocean"},
	{{0, 0, 0, 23, 0, 0}, "sacked and deserted monastary"},
	{{0, 0, 0, 0, 0, 0}, "*Welcome to Never Never Land"}
};
