import sys
import json

dir=str(sys.argv[len(sys.argv)-1])
fname= dir + '.json'
with open(fname) as f:
   raw = f.read()
data = json.loads(raw)

def remapdata(f, data, w):
    for j in range(0,16):
        for i in range(0,16):
            startrow = j * 4 * w
            startcol = i * 4
            startpixel = startrow + startcol
            f.write('    .byte ' + str(data[startpixel]) + '\n')
            f.write('    .byte ' + str(data[startpixel + 1]) + '\n')
            f.write('    .byte ' + str(data[startpixel + 2]) + '\n')
            f.write('    .byte ' + str(data[startpixel + 3]) + '\n')
            startrow += w
            startpixel = startrow + startcol
            f.write('    .byte ' + str(data[startpixel]) + '\n')
            f.write('    .byte ' + str(data[startpixel + 1]) + '\n')
            f.write('    .byte ' + str(data[startpixel + 2]) + '\n')
            f.write('    .byte ' + str(data[startpixel + 3]) + '\n')
            startrow += w
            startpixel = startrow + startcol
            f.write('    .byte ' + str(data[startpixel]) + '\n')
            f.write('    .byte ' + str(data[startpixel + 1]) + '\n')
            f.write('    .byte ' + str(data[startpixel + 2]) + '\n')
            f.write('    .byte ' + str(data[startpixel + 3]) + '\n')
            startrow += w
            startpixel = startrow + startcol
            f.write('    .byte ' + str(data[startpixel]) + '\n')
            f.write('    .byte ' + str(data[startpixel + 1]) + '\n')
            f.write('    .byte ' + str(data[startpixel + 2]) + '\n')
            f.write('    .byte ' + str(data[startpixel + 3]) + '\n')

def plaindata(f, data, fname):
    f.write('static const unsigned char const ' + fname + 'top[] = {\n')
    for j in range(0,10):
        f.write('    ')
        for i in range(0,40):
            f.write(str(data[i + j*40] - 1) + ', ')
        f.write('\n')
    f.write('};\n\n')
    f.write('static const unsigned char const ' + fname + 'bot[] = {\n')
    for j in range(25,28):
        f.write('    ')
        for i in range(0,40):
            f.write(str(data[i + j*40] - 1) + ', ')
        f.write('\n')
    f.write('};\n')

def printmap(f, data, w, s):
    f.write('static unsigned char ' + s + 'map[] = {')
    i = -1
    for d in data:
        if i > -1: 
            f.write(',')
        else:
            i = 0
        if i == 0:
            f.write('\n')
        i = i + 1
        if i >= w:
            i = 0
        if d < 10:
            f.write(' '+str(d))
        else:
            f.write(str(d))
    f.write('};\n')
    f.write('\n')

def printentrypoints(f, objects, w):
    f.write("#define nrentrypoints " + str(len(objects)) + '\n')
    f.write('static unsigned int entrypoints[] = {\n')
    i = -1
    for o in objects:
        if i > 0: 
            f.write(',')
        else:
            i = 0
        if i == 0:
            f.write('\n')
        f.write(str(int(o['x']/16) + w * int(o['y']/16)))
        i = i + 1
        if i >= w:
            i = 0
            f.write('\n')
    f.write('};\n')
    f.write('\n')
    f.write('static unsigned char entrylevels[] = {\n')
    i = -1
    for o in objects:
        if i > 0: 
            f.write(',')
        else:
            i = 0
        if i == 0:
            f.write('\n')
        f.write(o['name'])
        i = i + 1
        if i >= w:
            i = 0
            f.write('\n')
    f.write('};\n')
    f.write('\n')

def printentertbl(f, p):
    f.write('#if 0\n')
    f.write('static unsigned char entertbl[] = {')
    i = -1
    for j in range(len(p)):
        if i > -1: 
            f.write(',')
        else:
            i = 0
        if i == 0:
            f.write('\n')
        if i == 0:
            f.write('\n')
        i = i + 1
        if i >= 7:
            i = 0
        v = p[str(j)]
        d = v['enter']
        if d < 10:
            f.write(' '+str(d))
        else:
            f.write(str(d))
    f.write('};\n')
    f.write('#endif\n')
    f.write('\n')

def printguardpaths(f, objects, w):
    f.write("static unsigned char nrofguards = " + str(len(objects)) + ';\n')
    f.write('static unsigned char guardspawnx[] = {')
    i = -1
    for o in objects:
        if i > 0: 
            f.write(',')
        else:
            i = 0
        if i == 0:
            f.write('\n')
        f.write(str(int((o['x'])/16)))
        i = i + 1
        if i >= w:
            i = 0
    f.write('};\n')
    f.write('static unsigned char guardspawny[] = {')
    i = -1
    for o in objects:
        if i > 0: 
            f.write(',')
        else:
            i = 0
        if i == 0:
            f.write('\n')
        f.write(str(int((o['y']+8)/16)))
        i = i + 1
        if i >= w:
            i = 0
    f.write('};\n')
    f.write('static unsigned char guardpathlen[] = {')
    i = -1
    for o in objects:
        if 'polyline' in o:
            pl = o['polyline']
            if i > 0: 
                f.write(',')
            else:
                i = 0
            if i == 0:
                f.write('\n')
            f.write(str(len(pl)))
            i = i + 1
            if i >= w:
                i = 0
    f.write('};\n')
    f.write('static signed char guardpathx[] = {')
    i = -1
    for o in objects:
        if 'polyline' in o:
            pl = o['polyline']
            for item in pl:
                if i > -1: 
                    f.write(',')
                else:
                    i = 0
                if i == 0:
                    f.write('\n')
                if int(item['x']) < 0:
                    f.write(str(int((item['x']-8)/16)))
                else:
                    f.write(str(int((item['x']+8)/16)))
                i = i + 1
                if i >= w:
                    i = 0
    f.write('};\n')
    f.write('static signed char guardpathy[] = {')
    i = -1
    for o in objects:
        if 'polyline' in o:
            pl = o['polyline']
            for item in pl:
                if i > -1: 
                    f.write(',')
                else:
                    i = 0
                if i == 0:
                    f.write('\n')
                if int(item['y']) < 0:
                    f.write(str(int((item['y']-8)/16)))
                else:
                    f.write(str(int((item['y']+8)/16)))
                i = i + 1
                if i >= w:
                    i = 0
    f.write('};\n')
    f.write('\n')

def printbullettbl(f, p):
    f.write('#if 0\n')
    f.write('static unsigned char bullettbl[] = {')
    i = -1
    for j in range(len(p)):
        v = p[str(j)]
        if 'bullet' in v:
            if i > -1: 
                f.write(',')
            else:
                i = 0
            if i == 0:
                f.write('\n')
            i = i + 1
            if i >= 1:
                i = 0
            d = j
            if d < 10:
                f.write(' '+str(d)+',')
            else:
                f.write(str(d)+',')
            d = v['bullet']
            if d < 10:
                f.write(' '+str(d))
            else:
                f.write(str(d))
    f.write('};\n')
    f.write('#endif\n')
    f.write('\n')

def printkeytbl(f, p):
    f.write('#if 0\n')
    f.write('static unsigned char keytbl[] = {')
    i = -1
    for j in range(len(p)):
        v = p[str(j)]
        if 'key' in v:
            if i > -1: 
                f.write(',')
            else:
                i = 0
            if i == 0:
                f.write('\n')
            i = i + 1
            if i >= 1:
                i = 0
            d = j
            if d < 10:
                f.write(' '+str(d)+',')
            else:
                f.write(str(d)+',')
            d = v['key']
            if d < 10:
                f.write(' '+str(d))
            else:
                f.write(str(d))
    f.write('};\n')
    f.write('#endif\n')
    f.write('\n')

def printlocktbl(f, p):
    f.write('#if 0\n')
    f.write('static unsigned char locktbl[] = {')
    i = -1
    for j in range(len(p)):
        v = p[str(j)]
        if 'lock' in v:
            if i > -1: 
                f.write(',')
            else:
                i = 0
            if i == 0:
                f.write('\n')
            i = i + 1
            if i >= 1:
                i = 0
            d = j
            if d < 10:
                f.write(' '+str(d)+',')
            else:
                f.write(str(d)+',')
            d = v['lock']
            if d < 10:
                f.write(' '+str(d))
            else:
                f.write(str(d))
    f.write('};\n')
    f.write('#endif\n')
    f.write('\n')

def printdevicetbl(f, p):
    f.write('#if 0\n')
    f.write('static unsigned char devicetbl[] = {')
    i = -1
    for j in range(len(p)):
        v = p[str(j)]
        if 'device' in v:
            if i > -1: 
                f.write(',')
            else:
                i = 0
            if i == 0:
                f.write('\n')
            i = i + 1
            if i >= 1:
                i = 0
            d = j
            if d < 10:
                f.write(' '+str(d)+',')
            else:
                f.write(str(d)+',')
            d = v['device']
            if d < 10:
                f.write(' '+str(d))
            else:
                f.write(str(d))
    f.write('};\n')
    f.write('#endif\n')
    f.write('\n')

def printhostagetbl(f, p):
    f.write('#if 0\n')
    f.write('static unsigned char hostagetbl[] = {')
    i = -1
    for j in range(len(p)):
        v = p[str(j)]
        if 'hostage' in v:
            if i > -1: 
                f.write(',')
            else:
                i = 0
            if i == 0:
                f.write('\n')
            i = i + 1
            if i >= 1:
                i = 0
            d = j
            if d < 10:
                f.write(' '+str(d)+',')
            else:
                f.write(str(d)+',')
            d = v['hostage']
            if d < 10:
                f.write(' '+str(d))
            else:
                f.write(str(d))
    f.write('};\n')
    f.write('#endif\n')
    f.write('\n')

def printkittbl(f, p):
    f.write('#if 0\n')
    f.write('static unsigned char kittbl[] = {')
    i = -1
    for j in range(len(p)):
        v = p[str(j)]
        if 'kit' in v:
            if i > -1: 
                f.write(',')
            else:
                i = 0
            if i == 0:
                f.write('\n')
            i = i + 1
            if i >= 1:
                i = 0
            d = j
            if d < 10:
                f.write(' '+str(d)+',')
            else:
                f.write(str(d)+',')
            d = v['kit']
            if d < 10:
                f.write(' '+str(d))
            else:
                f.write(str(d))
    f.write('};\n')
    f.write('#endif\n')
    f.write('\n')

def printdamagetbl(f, p):
    f.write('#if 0\n')
    f.write('static unsigned char damagetbl[] = {')
    i = -1
    for j in range(len(p)):
        v = p[str(j)]
        if 'damage' in v:
            if i > -1: 
                f.write(',')
            else:
                i = 0
            if i == 0:
                f.write('\n')
            i = i + 1
            if i >= 1:
                i = 0
            d = j
            if d < 10:
                f.write(' '+str(d)+',')
            else:
                f.write(str(d)+',')
            d = v['damage']
            if d < 10:
                f.write(' '+str(d))
            else:
                f.write(str(d))
    f.write('};\n')
    f.write('#endif\n')
    f.write('\n')

fname= dir + '.c'
with open(fname, 'w') as f:
    layers = data['layers']
    for l in layers:
        if l['name'] == 'Tile Layer 1':
            plaindata(f, l['data'], dir)
    f.write('\n')
