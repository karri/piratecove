CC=/usr/bin/cc65
AS=/usr/bin/ca65
CL=/usr/bin/cl65
AR=/usr/bin/ar65
SP=/usr/bin/sp65
CP=cp
RM=rm -f
SED=sed
HMCC=hmcc
ECHO=echo
TOUCH=touch
PYTHON=python3
PYTHON2=python2
INDENT=indent -nbad -bap -nbc -bbo -hnl -br -brs -c33 -cd33 -ncdb -ce -ci4 -cli0 -d0 -di1 -nfc1 -i8 -ip0 -l80 -lp -npcs -nprs -npsl -sai -saf -saw -ncs -nsc -sob -nfca -cp33 -ss -ts8 -il1

CFLAGS=-I . --add-source -O -Or -Cl -Os

PREFIX=/usr/share
CC65_HOME=$(PREFIX)/cc65
CC65_INC=$(CC65_HOME)/include
CC65_ASMINC=$(CC65_HOME)/asminc
CC65_LIB=$(CC65_HOME)/lib

# Rule for making a *.o file out of a *.c file
%.s: %.c
	$(CC) -t atari7800 -I $(CC65_INC) $(CFLAGS) $(SEGMENTS) -o $(patsubst %c, %s, $(notdir $<)) $<

%.o: %.c
	$(CC) -t atari7800 -I $(CC65_INC) $(CFLAGS) $(SEGMENTS) -o $(patsubst %c, %s, $(notdir $<)) $<
	$(AS) -t atari7800 -I $(CC65_ASMINC) -o $@ $(AFLAGS) $(*).s
	$(RM) $*.s

# Rule for making a *.o file out of a *.s file
%.o: %.s
	$(AS) -t atari7800 -I $(CC65_ASMINC) -o $@ $(AFLAGS) $<

%.vgc: %.vgm
	$(PYTHON2) vgmpacker.py $<

%.bin: %.vgm
	$(PYTHON2) vgmconverter.py $< -n -r $@

%.hff: %.bin
	wine huffmunch.exe -B $< $@

%.c: %.json
	$(PYTHON) buildmap.py $<

