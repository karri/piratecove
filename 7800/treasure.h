/*
 *	SCOTT FREE
 *
 *	A free Scott Adams style adventure interpreter
 *
 *	Copyright:
 *	    This software is placed under the GNU license.
 *
 *	Statement:
 *	    Everything in this program has been deduced or obtained solely
 *	from published material. No game interpreter code has been
 *	disassembled, only published BASIC sources (PC-SIG, and Byte Dec
 *	1980) have been used.
 */

#ifdef MODE320
#define TOP_HEIGHT 10
#define STORY_LINES 14
#define SCROLL_DELAY 0
extern char screen[];
#define LINE_LEN 40
#else 
#define TOP_HEIGHT 2
#define STORY_LINES 24
#define SCROLL_DELAY 0
extern char screen[];
#define LINE_LEN 20
#endif

/*
 *	Controlling block
 */
  
#define LIGHT_SOURCE	9		/* Always 9 how odd */
#define CARRIED		255		/* Carried */
#define DESTROYED	0		/* Destroyed */
#define DARKBIT		15
#define LIGHTOUTBIT	16		/* Light gone out */

#define WordLen 3
#define MaxCarry 7
 
typedef struct
{
 	unsigned char NumItems;
 	unsigned char NumActions;
 	unsigned char NumWords;		/* Smaller of verb/noun is padded to same size */
 	unsigned char NumRooms;
 	unsigned char MaxCar;
 	unsigned char PlayerRoom;
 	unsigned char Treasures;
 	unsigned char WordLength;
 	unsigned char LightTime;
 	unsigned char NumMessages;
 	unsigned char TreasureRoom;
} Header;

typedef struct
{
	unsigned short Vocab;
	unsigned short Condition[5];
	unsigned short Action[2];
} Action;

typedef struct
{
	char Exits[6];
	char *Text;
} Room;

#define YOUARE		1	/* You are not I am */
#define SCOTTLIGHT	2	/* Authentic Scott Adams light messages */
#define DEBUGGING	4	/* Info from database load */
#define TRS80_STYLE	8	/* Display in style used on TRS-80 */
#define PREHISTORIC_LAMP 16	/* Destroy the lamp (very old databases) */


