RM=rm -f
SCOTTKIT=scottkit
PYTHON=python3

%.sco: %.sck
	$(SCOTTKIT) -c $< > $(patsubst %sck, %sco, $(notdir $<))

%.sck: %.dat
	$(SCOTTKIT) -b -d $< > $(patsubst %dat, %sck, $(notdir $<))

